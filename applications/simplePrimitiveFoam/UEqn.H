    // Momentum predictor

    tmp<fvVectorMatrix> UEqn
    (
        fvm::div(phi, U)
      - fvm::laplacian(nu,U)
      // == 0
    );

    UEqn().relax();

    solve(UEqn() == -fvc::grad(p));
