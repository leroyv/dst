#!/bin/zsh

find downscalingtools -name '*.pyc' -delete
cat files.txt | xargs rm -rf
python setup.py install --record files.txt
