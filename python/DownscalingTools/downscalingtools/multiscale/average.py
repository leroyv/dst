"""
Functions for average step.
"""

import configobj, os, shutil
import downscalingtools.tools.case as cs
import downscalingtools.tools.filesystem as fs
from downscalingtools.tools.mathematics import *
from PyFoam.Applications.FromTemplate import FromTemplate
from math import sqrt

def prepareMutables(caseDir, verbose = 0):
    """Process templates."""

    # Load CaseParameters.ini
    CaseParameters   = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    for dictName in CaseParameters['Dictionaries']:
        cs.primitiveFromTemplate(
            os.path.join(caseDir, dictName), 
            CaseParameters['Dictionaries'][dictName] 
            )

    # Build mesh
    argv = [ "blockMesh", "-case", caseDir ]
    cs.runSimpleUtility(argv, verbose = verbose)

def compute(caseDir, verbose = 0):
    """Run the averaging tool."""

    argvList = []

    # Run the average utility
    argvList.append([
        "average", 
        "-case", caseDir,
        os.path.join(caseDir,"input/reference")
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose = verbose)
