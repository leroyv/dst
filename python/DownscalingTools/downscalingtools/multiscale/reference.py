"""
Functions for reference step.
"""

import configobj, os, shutil
import downscalingtools.tools.case as cs
from PyFoam.Applications.FromTemplate import FromTemplate


def decompose(caseDir, verbose = 0):
    """Run decomposePar."""

    argv = [ "decomposePar", "-force", "-case", caseDir ]
    cs.runSimpleUtility(argv, verbose = verbose)

def reconstruct(caseDir, verbose = 0):
    """Run reconstructPar and perform other computational operations."""

    argvList = []

    # Run the reconstructPar utility
    argvList.append([ 
        "reconstructPar", 
        "-case", caseDir 
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose = verbose)

def flowPrepareMutables(caseDir, verbose = 0):
    """Prepare mutable files (BCs, etc.)."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    for dictName in CaseParameters['Dictionaries']:
        cs.primitiveFromTemplate(
            os.path.join(caseDir, dictName), 
            CaseParameters['Dictionaries'][dictName] 
            )

    # Move resulting initial conditions
    shutil.move(
        os.path.join(caseDir, '0/templates/U'),
        os.path.join(caseDir, '0/U')
        )

def flowCompute(caseDir, verbose = 0):
    """Run the solver."""

    appName = "simplePrimitiveFoam"
    cs.runSolver(appName, caseDir, verbose = verbose, clear = True)

def thermoPrepareMutables(caseDir, verbose = 0):
    """Prepare mutable files (BCs, etc.)."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    for dictName in CaseParameters['Dictionaries']:
        cs.primitiveFromTemplate(
            os.path.join(caseDir, dictName), 
            CaseParameters['Dictionaries'][dictName] 
            )

    # Move resulting initial conditions
    shutil.move(
        os.path.join(caseDir, '0/templates/T'),
        os.path.join(caseDir, '0/T')
        )

def thermoMapSources(caseDir, verbose = 0, consistent = False):
    """Map source data (e.g. initial conditions originating from other cases)."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))
    scaleFactor = CaseParameters['Scaling']['velocity']

    argvList = []

    # Map input velocity field
    flowDir = os.path.join(caseDir, "input/flow")
    argv = [ "mapFields", "-case", caseDir, '-mapMethod', 'cellPointInterpolate' ]
    if consistent: 
        argv.append("-consistent")
    argv.append(flowDir)
    argvList.append(argv)

    # Scale velocity field
    argv = [ "foamCalcEx", "multiplyDivide", "U", 
             "multiply", "-value", scaleFactor, 
             "-time", "0", 
             "-resultName", "U",
             "-case", caseDir
             ]
    argvList.append(argv)

    # Execute commands
    for argv in argvList:
        cs.runSimpleUtility(argv, verbose = verbose)

def thermoCompute(caseDir, verbose = 0):
    """Run the solver."""

    appName = "thermoPrimitiveFoam"
    cs.runSolver(appName, caseDir, verbose = verbose, clear = True)