"""
Functions for the definition of study parameters.
"""

import configobj
from math import pi

def process(StudyParameters):
    """
    Process the information that needs to be from the StudyParameters
    ConfigObj passed as an argument.
    """

    r0      = float(StudyParameters['Lengths']['r0'])
    rc      = r0 * float(StudyParameters['Lengths']['rcFactor'])
    l0      = r0**2 / (2 * pi * rc)
    mu      = float(StudyParameters['Fluid']['mu'])
    rho     = float(StudyParameters['Fluid']['rho'])
    nu      = mu / rho
    URef    = nu / l0
    k       = float(StudyParameters['Fluid']['k'])
    cp      = float(StudyParameters['Fluid']['cp'])
    alpha      = k/(rho*cp)
    epsilon = 1. - pi * (rc / r0)**2

    AdditionalParameters = {
        'rc' : rc,
        'l0' : l0,
        'k'  : k,
        'nu' : nu,
        'alpha' : alpha,
        'Pe' : nu/alpha,
        'URef' : URef
        }

    return AdditionalParameters