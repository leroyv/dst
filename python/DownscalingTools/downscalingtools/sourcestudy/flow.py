"""
Functions for flow cell step.
"""
import os, shutil, configobj, subprocess, glob
import downscalingtools.tools.case as cs
from PyFoam.Applications.FromTemplate import FromTemplate
from PyFoam.Execution.UtilityRunner import UtilityRunner

def prepareMutables(caseDir, verbose = 0):
    """Process mutable files."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    for dictName in CaseParameters['Dictionaries']:
        cs.primitiveFromTemplate(
            os.path.join(caseDir, dictName), 
            CaseParameters['Dictionaries'][dictName] 
            )

    # Move resulting initial conditions
    shutil.move(
        os.path.join(caseDir, '0/templates/UTildeStar'),
        os.path.join(caseDir, '0/UTildeStar')
        )

    # Create the createPatchDict
    subprocess.call([
        "python", 
        os.path.join(caseDir, "createPatchDict.py"), 
        "-s", os.path.join(caseDir, 'system'),
        "-c", os.path.join(caseDir, 'CaseParameters.ini')
        ])

    # Run the createPatch utility
    argv = [ "createPatch", "-case", caseDir, "-overwrite" ]
    cs.runSimpleUtility(argv, verbose=verbose)

    # Clear current directory obj files
    for fileName in glob.glob('*.obj'):
        try: 
            # Sometimes, when run in parallel, this one can crash 
            # (concurrent file removals)
            os.remove(fileName)
        except: 
            pass

def decompose(caseDir, verbose = 0):
    """Run decomposePar."""

    argv = [ "decomposePar", "-force", "-case", caseDir ]
    cs.runSimpleUtility(argv, verbose = verbose)

def compute(caseDir, verbose = 0):
    """Run the solver."""

    appName = "simpleDownscalingFoam"
    cs.runSolver(appName, caseDir, verbose = verbose, clear = True)

def reconstruct(caseDir, verbose = 0):
    """Run reconstructPar."""

    argv = [ "reconstructPar", "-case", caseDir ]
    cs.runSimpleUtility(argv, verbose = verbose)

# End of standard functions