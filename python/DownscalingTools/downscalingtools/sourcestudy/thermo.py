"""
Functions for thermo cell step.
"""
import os, shutil, configobj, subprocess
import downscalingtools.tools.case as cs
from PyFoam.Applications.FromTemplate import FromTemplate
from PyFoam.Execution.UtilityRunner import UtilityRunner
from downscalingtools.tools.case import PrimitiveRunner
from downscalingtools.tools.case import shellSafe

def prepareMutables(caseDir, verbose = 0):
    """Process mutable files."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    for dictName in CaseParameters['Dictionaries']:
        cs.primitiveFromTemplate(
            os.path.join(caseDir, dictName), 
            CaseParameters['Dictionaries'][dictName] 
            )

    # Move resulting initial conditions
    shutil.move(
        os.path.join(caseDir, '0/templates/TTildeStar'),
        os.path.join(caseDir, '0/TTildeStar')
        )

    # Create the createPatchDict
    subprocess.call([
        "python", 
        os.path.join(caseDir, "createPatchDict.py"), 
        "-s", os.path.join(caseDir, 'system'),
        "-c", os.path.join(caseDir, 'CaseParameters.ini')
        ])

    # Run the createPatch utility
    argv = [ "createPatch", "-case", caseDir, "-overwrite" ]
    cs.runSimpleUtility(argv, verbose=verbose)

def mapSources(caseDir, verbose = 0, consistent = False):
    """Map source data (e.g. initial conditions originating from other cases)."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    # Map input velocity field
    flowDir = os.path.join(CaseParameters["Directories"]['flowDir'])
    argv = [ "mapFields", 
             "-case", caseDir, 
             '-mapMethod', 'cellPointInterpolate', 
             '-sourceTime', 'latestTime' 
            ]
    if consistent: argv.append("-consistent")
    argv.append(flowDir)
    cs.runSimpleUtility(argv, verbose = verbose)

def decompose(caseDir, verbose = 0):
    """Run decomposePar."""

    argv = [ "decomposePar", "-force", "-case", caseDir ]
    cs.runSimpleUtility(argv, verbose = verbose)

def compute(caseDir, verbose = 0):
    """Run the solver."""

    appName = "thermoDownscalingFoam"
    cs.runSolver(appName, caseDir, verbose = verbose, clear = True)

def reconstruct(caseDir, verbose = 0):
    """Run reconstructPar."""

    argv = [ "reconstructPar", "-case", caseDir ]
    cs.runSimpleUtility(argv, verbose = verbose)

# End of standard functions
