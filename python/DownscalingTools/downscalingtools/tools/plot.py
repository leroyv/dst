"""
Tools for plotting.
"""

import math
import numpy as np
from downscalingtools.tools.mathematics import frexp10

def tickloc(vmin, vmax):
    """Builds a list of ticks given the parameters passed as arguments.
    Input:
    - vmin (float): minimum value
    - vmax (float): maximum value

    Output:
    - tickList (list(float)): list of tick locations
    """
    D = vmax - vmin # Ticks interval coverage
    N = 10 # Max number of divisions

    exp = frexp10(D)[1]

    multiplierList = [1, 2, 4, 5, 10]
    i = 0
    E = float(multiplierList[i]*10**exp)
    
    while ( i < len(multiplierList)-1 and (D/E >= N) ):
        i += 1
        E = float(multiplierList[i]*10**exp) # To be determined using N and D

    currentTick = vmin
    tickList = [ currentTick ]
    
    lowerBound = float(math.floor(vmin/E))*E
    upperBound = float(math.floor(vmax/E))*E
    
    # Define lower tick
    currentTick = lowerBound + E
    if ( abs(currentTick - vmin) < E/2 ):
        currentTick += E
    tickList.append(currentTick)

    while ( abs(currentTick - vmax) > E/2
        and currentTick < upperBound - E  ):
        currentTick += E
        tickList.append(currentTick)

    tickList.append(vmax)
    
    return tickList

def detect_coordinates(points, precision, cols=(0,1)):
    """Detects the coordinates in the array points. Column indices for the 
    x and y columns and precision for the inaccurate zeros elimination are
    passed as arguments."""

    xcol = cols[0]
    ycol = cols[1]

    ### Trim inaccurate zeros
    for i in range(points.shape[0]):
        if abs(points[i,xcol]) < precision:
            points[i,xcol] = 0
        if abs(points[i,ycol]) < precision:
            points[i,ycol] = 0
    
    ### Extract grid values (probably not the most efficient way to do this)
    xv = np.array(sorted(set(points[:,xcol])))
    yv = np.array(sorted(set(points[:,ycol])))

    ### Check if the number of values detected is coherent with the grid size
    if (len(xv)*len(yv) != points.shape[0]):
        print "Warning: Number of coordinate values detected doesn't match the grid size. Resulting plot might look bad."

    return xv, yv

if __name__ == '__main__':
    print 'Nothing to do'