"""
Various tools used to manipulate the filesystem.
"""

import os, shutil, errno

def mkdir_p(path):
    """
    Create path recursively; do nothing if directory already exists.
    """

    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def rm_rf(path):
    """
    Remove tree; do nothing if target does not exist.
    """

    try:
        shutil.rmtree(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.ENOENT:
            pass
        else: raise

def scandir(directory):
    """Scans a directory for files. 
    Returns the list of the files the directory dir and its subdirs contain 
    (relative path from dir).
    """

    # Get subdir list
    subdirList = [ os.path.relpath(x[0], directory) for x in os.walk(directory) ]
    
    # Get template files list
    fileList = []
    for currentdir, subdirs, files in os.walk(directory):
        for filename in files:
            fileList.append(
                os.path.relpath(
                    os.path.join(currentdir, filename),
                    directory
                    )
                )

    return fileList

def copyfilelist(srcDir, dstDir, copyList):
    """Copies files from source dir to destination dir.
    srcDir: path to the source dir (absolute or relative)
    dstDir: path to the destination dir (absolute or relative)
    copyList: list of 2-element lists containing relative source and destination 
        paths (resp. from the src and dst dirs) to the files to be copied

    The cool thing is that the copyList can be edited manually very easily (very 
    useful to copy only parts of an OpenFOAM case).
    """
    # Copy all files (overwrite if existing) after creating parent dir
    for pair in copyList:
        src = os.path.join(srcDir, pair[0])
        dst = os.path.join(dstDir, pair[1])
        mkdir_p(os.path.dirname(dst))
        shutil.copy(src, dst)

def copydir(srcDir, dstDir):
    """Copies all *files* in srcDir and subdirs to dstDir. Empty subdirs won't be
    copied."""

    copyList = [ [x,x] for x in scandir(srcDir) ]
    copyfilelist(srcDir, dstDir, copyList)