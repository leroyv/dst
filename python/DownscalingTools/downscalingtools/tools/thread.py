"""
Tools for threads.
"""

import threading, os, Queue
from pprint import pprint

# Element class for printer thread: the printer thread 
# outputs elements instantiated from this class
class PrintBufferElement:
    def __init__(self, message):
        self.threadName = threading.current_thread().getName()
        self.message = message

# Printer thread class
class Printer:
    """
    Printer class; not a singleton yet, this must be implemented 
    in the future.

    Printer is used to perform thread-safe prints. The Printer
    must be known passd to the worker functions so that they can
    pass messages to it. 
    """

    def __init__(self):
        self.queue = Queue.Queue()
        self.thread = threading.Thread(
            target = self.output,
            name="printer"
            )
        self.thread.daemon = True
        
    def start(self):
        self.thread.start()

    def put(self,message):
        self.queue.put(message)
        
    def output(self):
        while True:
            line = self.queue.get()
            self.queue.task_done()
            print('[{0}] {1}'.format(line.threadName, line.message))

# Thread monitor class
class Monitor:
    """
    Monitor class; not a singleton yet, this must be implemented 
    in the future.

    Monitor is used to keep track of tasks that have been executed.
    The register is a list (intrisically thread-safe in the common 
    implementations of Python) containing the IDs of finished tasks.
    The Monitor has to be instantiated and started by the master thread.
    Every worker thread needs to know the Monitor; they report to the 
    Monitor using its declare_finished() method.
    """

    def __init__(self):
        self.queue = Queue.Queue()
        self.register = []
        self.thread = threading.Thread(
            target = self.update,
            name="monitor"
            )
        self.thread.daemon = True

    def start(self):
        self.thread.start()

    def declare_finished(self, taskID):
        self.queue.put(taskID)
        
    def update(self):
        while True:
            taskID = self.queue.get()
            self.queue.task_done()
            self.register.append(taskID)

# Custom exception used to raise task manager exceptions
class TaskManagerException(Exception):
    pass

# Task Manager class, supersedes Monitor
class TaskManager:
    """
    TaskManager class. Thread safety is not implemented properly ATM; however,
    single operations on built-in Python structures are thread safe. The risk of
    thread conflict, although unassessed, is therefore low. However, neat data
    thread locking must be implemented in the future.

    TaskManager is used to keep track of tasks to be and already executed.
    It contains the following data structures:

    Data structures:
    - taskQueue (Queue.queue):
        List of tasks that are currently available for a worker thread to pick.
    - taskDict (dictionary):
        Dictionary containing the IDs of tasks that have been submitted (keys) to 
        the TaskManager and their completion state (values). If the value is 
        False, the task is unfinished; if the value is True, the task has 
        been completed.
    
    Public function members:
    - submit(Task):
        Submit a new task to the TaskManager.
    - pick():
        Pick a task from the queue. This method will only deliver a task
        that has its prerequisites met. If it does not find a task to deliver,
        it raises an exception.
    - declare_completed(Task):
        Declares that a task was completed and registers it as such.
    """

    def __init__(self):
        self.taskQueue = Queue.Queue()
        self.taskDict = {}

    def submit(self, task):
        """Adds task to the taskQueue."""
        self.taskQueue.put(task)
        self.taskDict[task.ID] = False

    def pick(self):
        """Delivers a task from taskQueue after checking if the task's
        prerequisites are met. Raises an exception if no task can be delivered."""

        prereqMet = False
        nPicked = 0

        while ( not(prereqMet) ):
            # Pick task
            task = self.taskQueue.get()
            self.taskQueue.task_done()
            nPicked += 1

            if not(task.prereq == ''):
                try:
                    evalString = task.prereq
                    for key in self.taskDict.keys():
                        evalString = evalString.replace(key, str(self.taskDict[key]))

                    prereqMet = eval(evalString)
                except:
                    print("Problem evaluating string: {0}".format(evalString))
                    raise 
            else:
                prereqMet = True
            # Use of eval is potentially dangerous, but I don't have the time to
            # write a pyparse parser.

            if (prereqMet):
                return task
            else:
                self.taskQueue.put(task)

            # If we cycled the entire queue and did not find a task to pick, raise
            # an exception
            if ( nPicked > self.taskQueue.qsize() ):
                raise TaskManagerException("no task has its prerequisites met")

    def declare_completed(self,task):
        """Register the task passed as an argument as completed."""

        self.taskDict[task.ID] = True

class Task:
    """
    Task class. Improvements in the flexibility of the prereq data structure and
    its analysis could be done.

    Task contains basic information about a task to be managed by a TaskManager.
    Data structures:
    - ID (string):
        String that identifies the task.
    - data (dictionary):
        Dictionary containing data to be used for the execution of the task.
    - prereq (string):
        String containing the logical expression for the prerequisites to be 
        met. Example: if tasks bearing the IDs 01, 02 and 04 have to be completed,
        the prereq string will be : "01 and 02 and 04". Any boolean expression
        can be 
    """

    def __init__(self, ID, data = {}, prereq = ''):
        self.ID = ID
        self.data = data
        self.prereq = prereq
