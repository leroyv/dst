"""
Tools for math.
"""

import numpy as np
from math import floor, log10
from scipy.linalg import norm
from scipy.spatial import KDTree

class Function2D_next:
    """An array representation of a 2D scalar function.

    This class encapsulates a 2D numpy.array containing data describing a
    function f(x,y) discretized on a Cartesian grid, as well as various data
    and methods that might prove useful for interpolation, differentiation,
    averaging... The mesh and function are described using the 'xy' (default)
    indexing convention. The function has to be loaded from a file  (see the
    self.load member function).

    Attributes:
        data (numpy.array): 2D array containing x, y and z values from the 
            file used as a reference.
        xi (numpy.array): 2D array containing the x values for the 
            Cartesian grid. Automatically generated from the xv and yv
            attributes using the numpy.meshgrid function.
        yi (numpy.array): 2D array containing the x values for the 
            Cartesian grid. Automatically generated from the xv and yv
            attributes using the numpy.meshgrid function.
        xv (numpy.array): 1D array containing the x values used for mesh
            generation.
        yv (numpy.array): 1D array containing the y values used for mesh
            generation.
        dx (float): Cartesian grid step (x).
        dy (float): Cartesian grid step (y).
        zi (numpy.array): 2D array containing the z values of f on the 
            Cartesian grid.

    """

    def __init__(self):
        """"Basic constructor. No value is assigned, do nothing."""

        self.data = None
        self.xv = None
        self.yv = None
        self.dx = None
        self.dy = None
        self.xi = None
        self.yi = None
        self.zi = None

    def load(self, filename, usecols=(0,1,2), skiprows=0, delimiter=','):
        """Load values from CSV file.

        Data contained in the file passed as an argument is read and stored in
        self.data member attribute. The source file must be ordered by column:
        abscissae must have the inner loop. Loading is then followed by a data
        refresh.

        Args:
            filename (string): Name of the file to be read.
            usecols (tuple(int)): indices of columns containing x, y and f values.
                Defaults to (0,1,2) (first three columns).
            skiprows (int): number of header lines to ignore. Defaults to 0.
            delimiter (string): delimiter to be used. Default to ',' (comma).

        """

        if len(usecols) > 3:
            raise ValueError("too many columns specified")
        
        # Load data
        self.data = np.loadtxt(
            filename, 
            skiprows=skiprows, 
            usecols=usecols,
            delimiter=delimiter
            )

        self.refresh_data()
    
    def refresh_data(self):
        """Data contained in self.data is used to generate square grid values.

        Note:
            This method also applies the self.refresh_cartesian_step member 
            function.

        """

        # Detect size
        nx, ny = 0, 0

        yTemp1 = self.data[0,1]
        yTemp2 = self.data[0,1]

        while (abs(yTemp1 - yTemp2) < 1e-10):
            nx += 1
            yTemp2 = self.data[nx,1]
        
        ny = self.data.shape[0] / nx

        # Check that the nx and ny values are coherent with the self.data
        if (nx*ny != self.data.shape[0]):
            raise ValueError('tentative mesh dimension inconsistent with input data size')

        # Generate mesh
        xmin = self.data[:,0].min()
        xmax = self.data[:,0].max()
        ymin = self.data[:,1].min()
        ymax = self.data[:,1].max()

        self.xv = np.linspace(xmin, xmax, nx)
        self.yv = np.linspace(ymin, ymax, ny)
        self.xi, self.yi = np.meshgrid(self.xv, self.yv, indexing='xy')
        # NOTE: the 'xy' indexing convention makes the indexing access the
        # reverse of that of the arguments in the mathematical notation: if
        # z=f(x,y), i is the index for looping on abscissae and j on
        # ordinates, then accessing the (i,j) element is done by calling
        # zi[j,i].

        # Store function values
        self.zi = np.copy(self.xi)
        k = 0
        for j in range(ny):
            for i in range(nx):
                self.zi[j,i] = self.data[k,2]
                k += 1

        # Refresh unset attributes
        self.refresh_cartesian_step()

    def refresh_cartesian_step(self):
        """Refreshes Cartesian grid steps.

        """

        self.dx = (self.xv.max() - self.xv.min()) / (len(self.xv) - 1)
        self.dy = (self.yv.max() - self.yv.min()) / (len(self.yv) - 1)

    def clone(self,f):
        """Copies the attributes of a Function2D_next object to self.

        This clones the function passed as an argument to the current
        instance.

        Args:
            f (Function2D): Function to be cloned to self.

        """

        self.data = np.copy(f.data)
        self.xv   = np.copy(f.xv)
        self.yv   = np.copy(f.yv)
        self.xi   = np.copy(f.xi)
        self.yi   = np.copy(f.yi)
        self.zi   = np.copy(f.zi)
        self.refresh_cartesian_step()

    def trim(self, xmin, xmax, ymin, ymax):
        """Trims self.data using coordinate values passed as arguments.

        This method trims the self.data array using the min and max coordinate
        values passed as arguments. The trim is followed by an attribute
        refresh.

        Args:
            xmin (float): Minimum abscissa.
            xmax (float): Maximum abscissa.
            ymin (float): Minimum ordinate.
            ymax (float): Maximum ordinate.

        """

        self.data
        pointList = []

        for k in range(self.data.shape[0]):
            if not(    self.data[k,0] < xmin
                    or self.data[k,0] > xmax
                    or self.data[k,1] < ymin
                    or self.data[k,1] > ymax ):
                pointList.append(self.data[k,:])

        self.data = np.array(pointList)
        self.refresh_data()

def m0_next(f, radius):
    """Applies a top hat filter to the field. 
    
    This is basically about computing the convolution product
    of a function f by the m0 weighting function.
    Works only on a regular grid.

    Args:
        f (Function2D_next): Function be averaged.
        radius (float): Radius of the averaging box (r0/2).
    
    Returns:
        g (Function2D_next): m0*f (where * is the convolution product).

    """

    g = Function2D_next()
    g.clone(f)

    filterCenter = np.array([0,0])
    currentPoint = np.array([0,0])

    for i in range(len(g.xv)):
        for j in range(len(g.yv)):
            # Search for zone indices
            kMin = len(f.xv)
            kMax = 0
            lMin = len(f.yv)
            lMax = 0

            for k in range(len(f.xv)):
                if abs(f.xv[k] - f.xv[i]) <= radius:
                    kMin = k
                    break
            for k in range(len(f.xv)-1, -1, -1):
                if abs(f.xv[k] - f.xv[i]) <= radius:
                    kMax = k
                    break

            for l in range(len(f.yv)):
                if abs(f.yv[l] - f.yv[j]) <= radius:
                    lMin = l
                    break
            for l in range(len(f.yv)-1, -1, -1):
                if abs(f.yv[l] - f.yv[j]) <= radius:
                    lMax = l
                    break

            value = 0
            for k in range(kMin, kMax+1):
                for l in range(lMin, lMax+1):
                    value += f.data[k,l]

            g.zi[j,i] = value / ((kMax+1-kMin)*(lMax+1-lMin))

    return g

def curvature(zi, dx=1, dy=1):
    """Compute the curvature of the surface passed as an argument.

    This function computes the Guassian, mean and principal
    curvatures of the function parametrized by 
        phi(u,v) = (u,v,f(u,v))
    The step of the grid used for the definition of f can be
    specified on function call.

    Args:
        zi (numpy.array): A 2nd order array (preferably using the 
            'xy' convention) which contains the values of the f 
            function on a regular Cartesian grid.
        dx (int, optional): Step of the Cartesian mesh (x axis). 
            Defaults to 1.
        dy (int, optional): Step of the Cartesian mesh (y axis). 
            Defaults to 1.

    Returns:
        K (numpy.array): A 2nd order array containing the values
            of the Gaussian curvature field on the mesh used for
            zi.
        H (numpy.array): A 2nd order array containing the values
            of the mean curvature field on the mesh used for zi.
        Pmin (numpy.array): A 2nd order array containing the values
            of the minimal principal curvature field on the mesh 
            used for zi.
        Pmax (numpy.array): A 2nd order array containing the values
            of the maximal principal curvature field on the mesh
            used for zi.

    """

    ziy, zix   = np.gradient(zi,  dx, dy, edge_order=2)
    zixy, zixx = np.gradient(zix, dx, dy, edge_order=2)
    ziyy, _    = np.gradient(ziy, dx, dy, edge_order=2)
    
    K = ( zixx*ziyy - zixy**2 ) / (1 + zix**2 + ziy **2)**2
    H = ( (1 + zix**2)*ziyy - 2*zix*ziy*zixy + (1 + ziy**2)*zixx ) \
        / (2*(1 + zix**2 + ziy**2)**(1.5))

    Pmin = H - np.sqrt(H**2 - K)
    Pmax = H + np.sqrt(H**2 - K)

    return K, H, Pmin, Pmax

def frexp10(x):
    """Returns the mantissa and exponent of x in scientific notation.
    Use of logarithm makes the method potentially inaccurate (round off 
    errors)."""

    exponent = floor(log10(abs(x))) if x != 0 else 0
    mantissa = x / 10**exponent

    return mantissa, exponent




# WHAT FOLLOWS IS OFFICIALLY DEPRECATED








class Function2D:
    """An array representation of a 2D scalar function."""

    def __init__(self):
        """Basic constructor: no value is assigned, do nothing.
        Members:
            meshX (numpy.array): list of grid abscissas (dim 1)
            meshY (numpy.array): list of grid ordinates (dim 1)
            mesh (numpy.array): coordinates grid, built from meshX and meshY (dim 3)
            size (tuple(float)): grid size, computed from meshX and meshY
            data (numpy.array): list of function values on mesh (dim 2)
        """
        self.meshX = np.empty(shape=(0))
        self.meshY = np.empty(shape=(0))
        self.size = (0,0)
        self.mesh = np.empty(shape=(0,0,2))
        self.data = np.empty(shape=(0,0))
        self.mesh_point_index = []
        self.kdtree = None
        
    def load(self, fileName, usecols=(0,1,2), skiprows=0, delimiter=','):
        """Load values from CSV file.
        Input:
            fileName (string): name of the file to be read.
            usecols (tuple(int)): indices of columns containing x, y and f values.
            skiprows (int): number of header lines to ignore.
            delimiter (string): delimiter to be used.
        """

        if len(usecols) > 3:
            raise ValueError("too many columns")
        
        # Load data
        data = np.genfromtxt(fileName, skip_header=skiprows, usecols=usecols, delimiter=delimiter)

        # TODO: Order data: per-column ordering 
        # (abscissas have the inner loop)

        # Detect size
        nX = 0
        nY = 0

        yTemp1 = data[0,1]
        yTemp2 = data[0,1]

        while (abs(yTemp1 - yTemp2) < 1e-10):
            nX += 1
            yTemp2 = data[nX,1]
        
        nY = data.shape[0] / nX

        self.size = (nX,nY)

        # Store mesh
        self.mesh.resize((nX, nY, 2))
        k = 0
        for j in range(nY):
            for i in range(nX):
                self.mesh[i,j,0] = data[k,0]
                self.mesh[i,j,1] = data[k,1]
                k += 1

        self.meshX.resize((nX))
        for i in range(nX):
            self.meshX[i] = self.mesh[i,0,0]

        self.meshY.resize((nY))
        for j in range(nY):
            self.meshY[j] = self.mesh[0,j,1]

        # Store function values
        self.data.resize(self.size)
        k = 0
        for j in range(nY):
            for i in range(nX):
                self.data[i,j] = data[k,2]
                k += 1

        # Refresh self.mesh_point_index
        self.refresh_mesh_point_index()       

    def clone(self,f):
        """Copies the attributes of Function2D object f to self."""

        self.mesh  = np.copy(f.mesh)
        self.meshX = np.copy(f.meshX)
        self.meshY = np.copy(f.meshY)
        self.data  = np.copy(f.data)
        self.size  = f.size[:]
        self.refresh_mesh_point_index()

    def refresh_mesh_point_index(self):
        """Refresh the mesh index. This is simply the data in self.mesh stored 
        in a linear form, with an index to link self.mesh_point_index and 
        self.mesh."""

        self.mesh_point_index = []
        
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                self.mesh_point_index.append(((i,j), self.mesh[i,j]))
        
    def refresh_kdtree(self):
        """Refresh the k-d tree associated with the mesh, used for nearest neighbor queries."""

        data = [ element[1] for element in self.mesh_point_index ]
        self.kdtree = KDTree(data)
    
    def interpolate(self, coords, scheme='bilinear', debug=False):
        """
        Interpolates the value of self at the coordinates coords.

        Input: 
            coords (tuple): coordinates of the interpolation point (size=2)
            scheme (string): interpolation scheme (available: nearest)
        Output:
            function interpolated value (float) if interpolation was successful
            None if interpolation failed
        """

        output = None

        if len(coords) > 2:
            raise ValueError('coords tuple size must be equal to 2')

        if scheme == 'nearest':
            
            i,j = self.nearest(coords)
            output = self.data[i,j]

        elif scheme == 'bilinear':
            # This implementation of bilinear interpolation is not fool-proof.
            # In particular, cell corner detection is terrible and might result in errors
            # on mesh boundaries.
            # See http://en.wikipedia.org/wiki/Bilinear_interpolation for notations.

            # Get the four corners of the square including the coordinates
            ### Get the nearest neighbour
            i,j = self.nearest(coords)

            ### Identify the configuration
            x,y   = coords
            xn,yn = self.mesh[i,j]
            config = -1
            
            ### Check if the coords tuple is off-bound
            if (   (x >= self.meshX.max()) \
                or (x <= self.meshX.min()) \
                or (y >= self.meshY.max()) \
                or (y <= self.meshY.min()) ):
                raise ValueError('interpolation point on mesh boundary or out of mesh')
            else:
                if (x-xn > 0):
                    if (y-yn > 0):
                        config = 1
                    else:
                        config = 2
                else:
                    if (y-yn > 0):
                        config = 4
                    else:
                        config = 3

            ### Build the Qnn point list
            Q11, Q12, Q21, Q22 = None, None, None, None

            if (config == 1):
                Q11 = (i,j)
                Q12 = (i,j+1)
                Q21 = (i+1,j)
                Q22 = (i+1,j+1)
            elif (config == 2):
                Q11 = (i,j-1)
                Q12 = (i,j)
                Q21 = (i+1,j-1)
                Q22 = (i+1,j)
            elif (config == 3):
                Q11 = (i-1,j-1)
                Q12 = (i-1,j)
                Q21 = (i,j-1)
                Q22 = (i,j)
            elif (config == 4):
                Q11 = (i-1,j)
                Q12 = (i-1,j+1)
                Q21 = (i,j)
                Q22 = (i,j+1)

            ### Compute the bilinear interpolate
            x1,y1 = self.mesh[Q11[0],Q11[1]]
            x2,y2 = self.mesh[Q22[0],Q22[1]]
            
            output = (  self.data[Q11[0],Q11[1]] * (x2-x)*(y2-y)
                      + self.data[Q21[0],Q21[1]] * (x-x1)*(y2-y)
                      + self.data[Q12[0],Q12[1]] * (x2-x)*(y-y1)
                      + self.data[Q22[0],Q22[1]] * (x-x1)*(y-y1) 
                      ) / ((x2-x1)*(y2-y1))

        else:
            raise ValueError('unrecognized scheme ' + scheme)

        return output

    def nearest(self, coords):
        """Returns indices of nearest neighbor to coords.
        Input:
            coords (tuple(float)): coordinates of the point to be searched for (size=2)
        Output:
            nearest (tuple(int)): indices of the grid point closest to coords (size=2)
        """

        # Detect nearest neighbor
        distance = 1e300
        cDistance = distance
        currentPoint = np.empty(2)
        nearest = [0,0]

        for i in range(len(self.meshX)):
            currentPoint[0] = self.meshX[i]

            for j in range(len(self.meshY)):
                currentPoint[1] = self.meshY[j]
                cDistance = norm(currentPoint - coords)

                if cDistance < distance:
                    distance = cDistance
                    nearest[0] = i
                    nearest[1] = j

        return tuple(nearest)

    def nearest_kdtree(self, coords):
        """Returns indices of nearest neighbor to coords.
        Input:
            coords (tuple(float)): coordinates of the point to be searched for (size=2)
        Output:
            nearest (int): index of the nearest neighbor in self.mesh_point_index 
        """

        distance, nearest_index = self.kdtree.query(coords)
        return nearest_index

    def toArray(self):
        """
        Returns a 3-column array containing the values of self as a function of coordinates.
        Useful for plotting with gnuplot. 
        NB: iteration on y is the inner loop (quickest cycling).
        """

        output = np.empty((self.size[0]*self.size[1], 3))

        k = 0
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                output[k,0] = self.mesh[i,j,0]
                output[k,1] = self.mesh[i,j,1]
                output[k,2] = self.data[i,j]
                k += 1

        return output

def diff(f, direction):
    """Returns the derivative of function f.
    Uses a second-order-accurate centered difference scheme.
    Edges ignored: the returned Function2D object has a smaller grid than the argument.
    Input:
        f (Function2D): function to be differentiated
        direction (int): direction to be considered (0=x, 1=y)"""

    g = Function2D()
    g.clone(f)

    # Compute the derivative
    if direction == 0:
        for i in range(1,f.size[0]-1):
            for j in range(1,f.size[1]-1):
                g.data[i,j] = (f.data[i+1,j]   - f.data[i-1,j]) \
                            / (f.mesh[i+1,j,0] - f.mesh[i-1,j,0])
    elif direction == 1:
        for i in range(1,f.size[0]-1):
            for j in range(1,f.size[1]-1):
                g.data[i,j] = (f.data[i,j+1]   - f.data[i,j-1]) \
                            / (f.mesh[i,j+1,1] - f.mesh[i,j-1,1])

    # Remove side rows and colums
    # TODO: add 2nd order forward and backward difference schemes
    # for edge points
    g.data = np.delete(g.data, (0), axis=0)
    g.data = np.delete(g.data, (-1), axis=0)
    g.data = np.delete(g.data, (0), axis=1)
    g.data = np.delete(g.data, (-1), axis=1)

    g.mesh = np.delete(g.mesh, (0), axis=0)
    g.mesh = np.delete(g.mesh, (-1), axis=0)
    g.mesh = np.delete(g.mesh, (0), axis=1)
    g.mesh = np.delete(g.mesh, (-1), axis=1)

    g.meshX = np.delete(g.meshX, (0), axis=0)
    g.meshX = np.delete(g.meshX, (-1), axis=0)

    g.meshY = np.delete(g.meshY, (0), axis=0)
    g.meshY = np.delete(g.meshY, (-1), axis=0)

    g.size = (len(g.meshX), len(g.meshY))

    return g

def diff2(f, direction):
    """Returns the second derivative of function f.
    Uses a second-order-accurate centered difference scheme.
    Edges ignored: the returned Function2D object has a smaller grid than the argument.
    Input:
        f (Function2D): function to be differentiated
        direction (int): direction to be considered (0=x, 1=y)
    Output:
        g (Function2D): derivative of f"""

    g = Function2D()
    g.clone(f)

    # Compute the second derivative (check formula, might lack robustness 
    # for non regular grids)
    if direction == 0:
        for i in range(1,f.size[0]-1):
            for j in range(1,f.size[1]-1):
                g.data[i,j] = (f.data[i+1,j] - 2*f.data[i,j] + f.data[i-1,j]) \
                            / (0.5 * (f.mesh[i+1,j,0] - f.mesh[i-1,j,0]))**2
    elif direction == 1:
        for i in range(1,f.size[0]-1):
            for j in range(1,f.size[1]-1):
                g.data[i,j] = (f.data[i,j+1] - 2*f.data[i,j] + f.data[i,j-1]) \
                            / (0.5 * (f.mesh[i,j+1,1] - f.mesh[i,j-1,1]))**2

    # Remove side rows and colums
    # TODO: add 2nd order forward and backward difference schemes
    # for edge points
    g.data = np.delete(g.data, (0), axis=0)
    g.data = np.delete(g.data, (-1), axis=0)
    g.data = np.delete(g.data, (0), axis=1)
    g.data = np.delete(g.data, (-1), axis=1)

    g.mesh = np.delete(g.mesh, (0), axis=0)
    g.mesh = np.delete(g.mesh, (-1), axis=0)
    g.mesh = np.delete(g.mesh, (0), axis=1)
    g.mesh = np.delete(g.mesh, (-1), axis=1)

    g.meshX = np.delete(g.meshX, (0), axis=0)
    g.meshX = np.delete(g.meshX, (-1), axis=0)

    g.meshY = np.delete(g.meshY, (0), axis=0)
    g.meshY = np.delete(g.meshY, (-1), axis=0)

    g.size = (len(g.meshX), len(g.meshY))

    return g

def m0(f, radius):
    """Applies a top hat filter to the field. 
    
    This is basically about computing the convolution product
    of a function f by the m0 weighting function.
    Works only on a regular grid.

    Args:
        f (Function2D): Function be averaged.
        radius (float): Radius of the averaging box (r0/2).
    
    Returns:
        g (Function2D): m0*f (where * is the convolution product).

    """

    g = Function2D()
    g.clone(f)

    filterCenter = np.array([0,0])
    currentPoint = np.array([0,0])

    for i in range(g.size[0]):
        for j in range(g.size[1]):
            # Search for zone indices
            kMin = f.size[0]
            kMax = 0
            lMin = f.size[1]
            lMax = 0

            for k in range(f.size[0]):
                if abs(f.meshX[k] - f.meshX[i]) <= radius:
                    kMin = k
                    break
            for k in range(f.size[0]-1, -1, -1):
                if abs(f.meshX[k] - f.meshX[i]) <= radius:
                    kMax = k
                    break

            for l in range(f.size[0]):
                if abs(f.meshY[l] - f.meshY[j]) <= radius:
                    lMin = l
                    break
            for l in range(f.size[1]-1, -1, -1):
                if abs(f.meshY[l] - f.meshY[j]) <= radius:
                    lMax = l
                    break

            value = 0
            for k in range(kMin, kMax+1):
                for l in range(lMin, lMax+1):
                    value += f.data[k,l]

            g.data[i,j] = value / ((kMax+1-kMin)*(lMax+1-lMin))

    return g

if __name__ == '__main__':
    print 'Nothing to do'