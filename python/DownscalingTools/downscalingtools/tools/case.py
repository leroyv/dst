"""
Tools used to manipulate case data.
"""

import os, re, sys, subprocess, shutil, configobj
from PyFoam.LogAnalysis.BoundingLogAnalyzer import BoundingLogAnalyzer
from PyFoam.LogAnalysis.LogLineAnalyzer import LogLineAnalyzer
from PyFoam.Applications.ClearCase import ClearCase
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.AnalyzedRunner import AnalyzedRunner
from filesystem import mkdir_p

def detectLatestTime(path):
    """
    Detect latest time directory at the path passed as an argument.
    """
    dirList = os.listdir(path)

    timeDirListString = []
    timeDirListFloat  = []

    # Detect time directories
    for i in range(len(dirList)):
        try:
            timeDirListFloat.append(float(dirList[i]))
            timeDirListString.append(dirList[i])
        except:
            pass

    # Check for the existence of time directories
    if len(timeDirListFloat) == 0:
        raise NameError('No time directory found at path "' + path + '"')

    # Find latest time
    latest = max(timeDirListFloat)

    return timeDirListString[timeDirListFloat.index(latest)]

class CompactLineAnalyzer(LogLineAnalyzer):
    """
    Compact output using PyFoam.
    """
    def __init__(self):
        LogLineAnalyzer.__init__(self)
 
        self.told=""
        self.exp=re.compile("^(.+):  Solving for (.+), Initial residual = (.+), Final residual = (.+), No Iterations (.+)$")
 
    def doAnalysis(self,line):
        m=self.exp.match(line)
        if m!=None:
            name=m.group(2)
            resid=m.group(3)
            time=self.getTime()
            if time!=self.told:
                self.told=time
                print "\n t = %6g : " % ( float(time) ),
            print " %5s: %6e " % (name,float(resid)),
            sys.stdout.flush()
 
class CompactAnalyzer(BoundingLogAnalyzer):
    """
    Compact output using PyFoam.
    """
    def __init__(self):
        BoundingLogAnalyzer.__init__(self)
        self.addAnalyzer("Compact",CompactLineAnalyzer())


# Primitive runner class: provides a very basic interface inspired by
# that of PyFoam's BasicRunner, without thread spawning. Well-suited
# for short-living tasks and for ones that produce big amounts
# of output.
# A PrimitiveRunner is always silent.
# Use of shell=False in subprocess.call() causes different behaviour 
# from that of the PyFoam classes (no pattern expansion or shell 
# commands can be issued, for instance).
class PrimitiveRunner:
    def __init__(
        self,
        argv,
        logname = None,
        ):

        self.argv = argv
        self.cmd = " ".join(self.argv)
        
        self.dir = os.path.curdir
        if "-case" in self.argv:
            self.dir = self.argv[self.argv.index("-case") + 1]

        if (logname == None):
            logname = "PyFoam." + os.path.basename(argv[0])
        self.logFile = os.path.join(self.dir, logname + ".logfile")
        self.errFile = os.path.join(self.dir, logname + ".err.logfile")

    def start(self):
        """Start running the task registered in the commands."""

        with open(self.logFile, "w") as logFile:
            with open(self.errFile, "w") as errFile:
                subprocess.call(self.argv, shell = False, stdout = logFile, stderr = errFile)
        
def shellSafe(argList):
    """"Makes the args list passed as an argument shell safe, i.e. inserts quotes
    around args with whitespace in them if necessary."""

    for i in range(len(argList)):
        arg = argList[i]
        if " " in arg:
            if (not(arg[0] == "\"")) and (not(arg[-1] == "\"")):
                argList[i] = "\"{0}\"".format(arg)

    return argList

def runSimpleUtility(argv, verbose = 0, logSuffix = ''):
    """Runs a utility (two verbosity levels). 
    Inputs:
    argv: list with the command and arguments to execute
    verbose: verbosity level; 0- means silent, 1+ means complete output; 
        whatever the value, the complete output is written to a log file; 
        defaults to silent
    logSuffix: suffix for the log file; defaults to the first element of argv,
        which is the name of the utility
    """

    if logSuffix == '': # No app name specified: we need to find one
        logSuffix = argv[0]

    logname="PyFoamUtility.{0}".format(logSuffix)

    if (verbose > 0):
        print("\nRunning {0}".format(logSuffix))
        BasicRunner( \
            logname=logname,
            silent=not(verbose > 1),
            argv=argv
            ).start()
    else:
        PrimitiveRunner( \
            logname=logname,
            argv=argv
            ).start()

def runSolver(appName, caseDir, nProc = 0, verbose = 0, clear = False):
    """Runs the case with the appropriate command (three verbosity levels). 
    Inputs:
    appName: string with the name of the solver to run
    caseDir: path to the directory where the case to be run is located
    nProc: number of processors to use; if not specified, the script 
        searches for its value in the CaseParameters.ini config file;
        if no value can be found (for various possible reasons), the script
        defaults to single core
    verbose: verbosity level; 0- means silent, 1 means some output, 2+ means 
        complete solver output; whatever the value, the complete output is 
        written to a log file; defaults to silent
    clear: if set to True, the pyFoamClearCase utility is executed before
        the solver is run
    """

    # Cleanup if necessary
    if clear: ClearCase(args=[caseDir])

    if nProc <= 0: # This means nProc wasn't specified on function call
        CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))
        nProc = int(CaseParameters['Dictionaries']['system/decomposeParDict']['numberOfSubdomains'])

    argv=[]
    
    if nProc > 1:
        argv=[
            "mpirun", "-n", str(nProc), 
            appName,
            "-parallel",
            "-case", caseDir
            ]
    elif nProc == 1:
        argv=[
            appName,
            "-case", caseDir
            ]
    else:
        raise ValueError("Incorrect number processes required: {0}".format(nProc))

    logname="PyFoamSolve.{0}".format(appName)

    if (verbose > 0): 
        print("\nRunning {0} ({1} processes)".format(appName, nProc))

        if (verbose > 1):
            BasicRunner(
                logname=logname,
                silent=False,
                argv=argv
                ).start()
        else:
            AnalyzedRunner(
                CompactAnalyzer(), 
                logname=logname,
                silent=True,
                argv=argv
                ).start()
    else:
        PrimitiveRunner(
            logname=logname,
            argv=argv
            ).start()

def primitiveFromTemplate(targetName, templateDict):
    """Basic string substitution function."""

    replacements = {}
    for key in templateDict.keys():
        replacements['${0}$'.format(key)] = templateDict[key]
    
    with open(targetName + ".template") as infile:
        with open(targetName, 'w') as outfile:
            for line in infile:
                for src, target in replacements.iteritems():
                    line = line.replace(src, target)
                outfile.write(line)

def touchFoam(caseDir):
    """Create an empty .foam file in the target directory."""
    fileName = os.path.join(caseDir, os.path.split(caseDir)[1] + '.foam')
    open(fileName, 'a').close()