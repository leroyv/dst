"""
Functions for incompressible.postprocess step step.
"""

import configobj, os, shutil
import downscalingtools.tools.case as cs
import downscalingtools.tools.filesystem as fs
import numpy as np
from PyFoam.Applications.FromTemplate import FromTemplate
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from math import pi, cos, sin

def prepareMutables_cell(caseDir, verbose = 0):
    """Prepare mutable files."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    offset = map(float, CaseParameters['Cell'].as_list('offset'))
    rc = float(CaseParameters['Lengths']['r0']) * float(CaseParameters['Lengths']['rcFactor'])
    
    # Prepare funkySetFieldsDict
    sourceTermDict = ParsedParameterFile(os.path.join(caseDir, 'constant/sourceTermDict'))

    templateDict = {
        'UBarX' : sourceTermDict['UBar'][2][0],
        'UBarY' : sourceTermDict['UBar'][2][1],
        'gradUBarXX' : sourceTermDict['gradUBar'][2][0],
        'gradUBarXY' : sourceTermDict['gradUBar'][2][1],
        'gradUBarYX' : sourceTermDict['gradUBar'][2][3],
        'gradUBarYY' : sourceTermDict['gradUBar'][2][4],
        'pBar' : sourceTermDict['pBar'][2],
        'gradpBarX' : sourceTermDict['gradpBar'][2][0],
        'gradpBarY' : sourceTermDict['gradpBar'][2][1],
        'cellCenterX' : offset[0],
        'cellCenterY' : offset[1]
        }

    FromTemplate(args=[os.path.join(caseDir, 'system/funkySetFieldsDict'), templateDict])

    # Prepare sampleDict
    angleList = np.linspace(0,2*pi,72) # One point every 5 deg

    pointList = [
        offset + np.array([rc*cos(theta), rc*sin(theta), 0]) 
        for theta in angleList
        ]

    strList = [
        '{0} {1} {2}'.format(pointList[i][0], pointList[i][1], pointList[i][2])
        for i in range(len(pointList))
        ]
    templateDict = { 'pointString' : '((' + ')('.join(strList) + '))' }

    ### Manual subsitution, because FromTemplate doesn't work
    cs.primitiveFromTemplate(os.path.join(caseDir, 'system/sampleDict'), templateDict)

def compute_reference(caseDir, verbose = 0):
    """Do stuff on a reference case."""

    argvList = []

    # Compute velocity magnitude
    argvList.append([
        "foamCalc", 
        'mag', 'U',
        "-case", os.path.join(caseDir)
        ])

    # Sample lines
    argvList.append([
        'sample',
        '-latestTime',
        '-case', os.path.join(caseDir)
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose=verbose)

def compute_average(caseDir, verbose = 0):
    pass

def compute_cell(caseDir, verbose = 0):
    "Do stuff on a cell case."

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))
    cellID = CaseParameters['Cell']['cellID']

    # Mapping of average data requires special treatment
    dst = os.path.join(caseDir, 'system/mapFieldsDict')
    src = os.path.join(caseDir, 'system/mapFieldsDict.average')
    shutil.copy(os.path.abspath(src), os.path.abspath(dst))

    # Map average fields
    argv = [
        "mapFields", 
        "-case", os.path.join(caseDir),
        "-mapMethod", "cellPointInterpolate",
        "-sourceTime", "latestTime",
        os.path.abspath(os.path.join(caseDir, '../../average'))
        ]
    cs.runSimpleUtility(argv, verbose=verbose)

    # Replace mapFieldsDict with the generic version
    src = os.path.join(caseDir, 'system/mapFieldsDict.generic')
    shutil.copy(os.path.abspath(src), os.path.abspath(dst))

    argvList = []

    # Map reference fields
    argvList.append([
        "mapFields", 
        "-case", os.path.join(caseDir),
        "-mapMethod", "cellPointInterpolate",
        "-sourceTime", "latestTime",
        os.path.abspath(os.path.join(caseDir, '../../reference'))
        ])

    # Map fields from the cell case
    argvList.append([
        "mapFields", 
        "-case", os.path.join(caseDir),
        "-mapMethod", "cellPointInterpolate",
        "-sourceTime", "latestTime",
        os.path.abspath(os.path.join(caseDir, '../../cell', cellID))
        ])

    # Reconstruct average fields
    argvList.append([
        "funkySetFields", 
        "-case", os.path.join(caseDir),
        "-time", "0",
        ])

    # Reconstruct velocity
    argvList.append([
        "foamCalc", 
        'addSubtract', 'UBarStar',
        'add', '-field', 'UTildeStar',
        '-resultName', 'UStar',
        "-case", os.path.join(caseDir)
        ])

    # Reconstruct pressure
    argvList.append([
        "foamCalc", 
        'addSubtract', 'pBarStar',
        'add', '-field', 'pTildeStar',
        '-resultName', 'pStar',
        "-case", os.path.join(caseDir)
        ])

    # Compute reconstructed velocity magnitude
    argvList.append([
        "foamCalc", 
        'mag', 'UStar',
        "-case", os.path.join(caseDir)
        ])

    # Compute velocity deviation field
    argvList.append([
        "foamCalc", 
        'addSubtract', 'U',
        'subtract', '-field', 'UBar',
        '-resultName', 'UTilde',
        "-case", os.path.join(caseDir)
        ])

    # Compute pressure deviation field
    argvList.append([
        "foamCalc", 
        'addSubtract', 'p',
        'subtract', '-field', 'pBar',
        '-resultName', 'pTilde',
        "-case", os.path.join(caseDir)
        ])

    # Sample lines
    argvList.append([ 
        'sample',
        '-latestTime',
        '-case', os.path.join(caseDir)
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose=verbose)

    cs.touchFoam(caseDir)
