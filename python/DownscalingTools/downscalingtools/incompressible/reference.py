"""
Functions for reference step.
"""

import configobj, os, shutil
import downscalingtools.tools.case as cs
from PyFoam.Applications.FromTemplate import FromTemplate

def prepareMutables(caseDir, verbose = 0):
    """Prepare mutable files (BCs, etc.)."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    for dictName in CaseParameters['Dictionaries']:
        cs.primitiveFromTemplate(
            os.path.join(caseDir, dictName), 
            CaseParameters['Dictionaries'][dictName] 
            )

    # Move resulting initial conditions
    shutil.move(
        os.path.join(caseDir, '0/templates/U'),
        os.path.join(caseDir, '0/U')
        )

def decompose(caseDir, verbose = 0):
    """Run decomposePar."""

    argv = [ "decomposePar", "-force", "-case", caseDir ]
    cs.runSimpleUtility(argv, verbose = verbose)

def compute(caseDir, verbose = 0):
    """Run the solver."""

    appName = "simplePrimitiveFoam"
    cs.runSolver(appName, caseDir, verbose = verbose, clear = True)

def reconstruct(caseDir, verbose = 0):
    """Run reconstructPar and perform other computational operations."""

    argvList = []

    # Run the reconstructPar utility
    argvList.append([ 
        "reconstructPar", 
        "-case", caseDir 
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose = verbose)