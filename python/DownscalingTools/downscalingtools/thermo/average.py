"""
Functions for thermo.average step.
"""

import configobj, os, shutil, copy
import downscalingtools.tools.case as cs
import downscalingtools.tools.filesystem as fs
from PyFoam.Applications.FromTemplate import FromTemplate
from downscalingtools.tools.mathematics import *
from math import sqrt

def prepareMutables(caseDir, verbose = 0):
    """Process templates."""

    # Load CaseParameters.ini
    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))
    
    for dictName in CaseParameters['Dictionaries']:
        cs.primitiveFromTemplate(
            os.path.join(caseDir, dictName), 
            CaseParameters['Dictionaries'][dictName] 
            )

    # Build mesh
    argv = [ "blockMesh", "-case", caseDir ]
    cs.runSimpleUtility(argv, verbose = verbose)

def compute(caseDir, verbose = 0):
    """Run the averaging tool."""

    argvList = []

    # Run the average utility
    argvList.append([
        "average", 
        "-case", caseDir,
        os.path.join(caseDir,"input/reference")
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose = verbose)

    compute_sourceTerms(caseDir, verbose = verbose)

def compute_applyAddOps(caseDir, order, verbose = 0):
    """Load the results and apply additional top hat averaging filters to
    increase the averaging filter order.

    Input:
    - caseDir (string): path to the case directory to process.
    - order (int): number of additional averaging operators to apply.
    """

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    # Load data from hard drive
    UBarX = Function2D()
    UBarX.load(os.path.join(caseDir, "postProcessing/m0_UBar.csv"), skiprows=1, usecols=(0,1,3))

    UBarY = Function2D()
    UBarY.load(os.path.join(caseDir, "postProcessing/m0_UBar.csv"), skiprows=1, usecols=(0,1,4))

    TBar  = Function2D()
    TBar.load(os.path.join(caseDir, "postProcessing/m0_TBar.csv"), skiprows=1, usecols=(0,1,3))

    # Apply the m0 weighting function to reach the desired order
    if (verbose > 0): print('Applying additional averaging operators...')

    radius = float(CaseParameters['Filter']['radius'])
    for i in range(order):
        UBarX = m0(UBarX, radius)
        UBarY = m0(UBarY, radius)
        TBar  = m0(TBar,  radius)

    # Write data to hard drive
    nx, ny = TBar.mesh.shape[0:2]
    data = np.zeros((nx*ny,4))
    k = 0
    for j in range(ny):
        for i in range(nx):
            data[k,:] = (TBar.mesh[i,j,0], TBar.mesh[i,j,1], 0, TBar.data[i,j])
            k += 1

    np.savetxt(os.path.join(caseDir, "postProcessing/m2_TBar.csv"), data, delimiter=',',
        header='"x","y","z","TBar"')

    nx, ny = UBarX.mesh.shape[0:2]
    data = np.zeros((nx*ny,7))
    k = 0
    for j in range(ny):
        for i in range(nx):
            UBarMag = sqrt(UBarX.data[i,j]**2 + UBarY.data[i,j]**2)
            data[k,:] = (
                UBarX.mesh[i,j,0], UBarX.mesh[i,j,1], 0, 
                UBarX.data[i,j], UBarY.data[i,j], 0, UBarMag
                )
            k += 1

    np.savetxt(os.path.join(caseDir, "postProcessing/m2_UBar.csv"), data, delimiter=',',
        header='"x","y","z","UBarX","UBarY","UBarZ","UBarMag"')

def compute_sourceTerms(caseDir, verbose = 0):
    """Compute source terms using the results of the averaging operation."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    # Load data from hard drive
    UBarX = Function2D()
    UBarX.load(os.path.join(caseDir, "postProcessing/m2_UBar.csv"), skiprows=1, usecols=(0,1,3))
    UBarY = Function2D()
    UBarY.load(os.path.join(caseDir, "postProcessing/m2_UBar.csv"), skiprows=1, usecols=(0,1,4))
    TBar = Function2D()
    TBar.load(os.path.join(caseDir, "postProcessing/m2_TBar.csv"), skiprows=1, usecols=(0,1,3))

    # Compute source terms
    if (verbose > 0): print('Computing source terms...')

    ### Compute divUBar
    divUBar = Function2D()
    
    divUBar = diff(UBarX, direction=0)
    divUBar.data += diff(UBarY, direction=1).data
    
    ### Compute gradTBar
    gradTBarX = Function2D()
    gradTBarY = Function2D()

    gradTBarX = diff(TBar, direction=0)
    gradTBarY = diff(TBar, direction=1)

    ### Compute gradgradTBar
    gradgradTBarXX = diff(diff(TBar, direction=0), direction=0)
    gradgradTBarXY = diff(diff(TBar, direction=1), direction=0)
    gradgradTBarYX = diff(diff(TBar, direction=0), direction=1)
    gradgradTBarYY = diff(diff(TBar, direction=1), direction=1)

    ### Compute gradUBar (required for post-processing)
    gradUBarXX = Function2D()
    gradUBarXY = Function2D()
    gradUBarYX = Function2D()
    gradUBarYY = Function2D()

    gradUBarXX = diff(UBarX, direction=0)
    gradUBarXY = diff(UBarX, direction=1)
    gradUBarYX = diff(UBarY, direction=0)
    gradUBarYY = diff(UBarY, direction=1)

    # Output source terms to the appropriate dictionary
    TBar.refresh_kdtree()
    gradTBarX.refresh_kdtree()
    gradgradTBarXX.refresh_kdtree()

    sourceTermDictDict = {}

    cellIDList = sorted(CaseParameters['GridCells'].keys())

    for cellID in cellIDList:
        cellCenter = map(float, CaseParameters['GridCells'].as_list(cellID)[0:2])
        fs.mkdir_p(os.path.join(caseDir, 'output', cellID))

        sourceTermDict = {}
        
        sourceTermDict['TBar'] = TBar.interpolate(cellCenter)
        sourceTermDict['UBarX'] = UBarX.interpolate(cellCenter)
        sourceTermDict['UBarY'] = UBarY.interpolate(cellCenter)
        sourceTermDict['gradTBarX'] = gradTBarX.interpolate(cellCenter)
        sourceTermDict['gradTBarY'] = gradTBarY.interpolate(cellCenter)
        sourceTermDict['divUBar']   = divUBar.interpolate(cellCenter)
        sourceTermDict['gradUBarXX'] = gradUBarXX.interpolate(cellCenter)
        sourceTermDict['gradUBarXY'] = gradUBarXY.interpolate(cellCenter)
        sourceTermDict['gradUBarYX'] = gradUBarYX.interpolate(cellCenter)
        sourceTermDict['gradUBarYY'] = gradUBarYY.interpolate(cellCenter)
        
        FromTemplate(args=[os.path.join(caseDir, 'output/sourceTermDict'), sourceTermDict])

        shutil.copy(os.path.join(caseDir, 'output/sourceTermDict'), os.path.join(caseDir, 'output', cellID))

        # Additional values for the CSV record
        sourceTermDict['gradgradTBarXX'] = gradgradTBarXX.interpolate(cellCenter)
        sourceTermDict['gradgradTBarXY'] = gradgradTBarXY.interpolate(cellCenter)
        sourceTermDict['gradgradTBarYX'] = gradgradTBarYX.interpolate(cellCenter)
        sourceTermDict['gradgradTBarYY'] = gradgradTBarYY.interpolate(cellCenter)

        # Copy the results to a general dictionary
        sourceTermDictDict[cellID] = copy.deepcopy(sourceTermDict)

    # Output source terms to a CSV file
    fieldIDList = sorted(sourceTermDictDict[sourceTermDictDict.keys()[0]].keys())
    tempArray = np.zeros((len(cellIDList), len(fieldIDList)))

    ### Gather all numerical data into an array
    i = 0
    for cellID in cellIDList:
        j = 0
        for fieldID in fieldIDList:
            tempArray[i,j] = sourceTermDictDict[cellID][fieldID]
            j += 1
        i += 1

    ### Stack numerical values and cellIDs into a string array
    tempArray = np.hstack((
        np.array(cellIDList, dtype="string_", ndmin=2).T,
        tempArray
        ))

    ### Output the resulting array to a CSV file (with a header)
    header = ",".join(["cellID"] + fieldIDList)
    np.savetxt(
        os.path.join(caseDir, "postProcessing/sources.csv"),
        tempArray,
        fmt="%s",
        header=header,
        delimiter=","
        )
