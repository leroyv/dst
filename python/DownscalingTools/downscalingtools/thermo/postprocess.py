"""
Functions for the thermo.postprocess step.
"""

import configobj, os, shutil
import downscalingtools.tools.case as cs
from downscalingtools.tools.mathematics import *
import numpy as np
from scipy.interpolate import LinearNDInterpolator
from PyFoam.Applications.FromTemplate import FromTemplate
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from math import pi, cos, sin, sqrt


def prepareMutables_cell(caseDir, verbose = 0):
    """Prepare mutable files (BCs, etc.)."""

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))

    offset = map(float, CaseParameters['Cell'].as_list('offset'))
    rc = float(CaseParameters['Lengths']['r0']) * float(CaseParameters['Lengths']['rcFactor'])
    
    # Prepare funkySetFieldsDict
    sourceTermDict = ParsedParameterFile(os.path.join(caseDir, 'constant/sourceTermDict'))

    templateDict = {
        'UBarX' : sourceTermDict['UBar'][2][0],
        'UBarY' : sourceTermDict['UBar'][2][1],
        'gradUBarXX' : sourceTermDict['gradUBar'][2][0],
        'gradUBarXY' : sourceTermDict['gradUBar'][2][1],
        'gradUBarYX' : sourceTermDict['gradUBar'][2][3],
        'gradUBarYY' : sourceTermDict['gradUBar'][2][4],
        'TBar' : sourceTermDict['TBar'][2],
        'gradTBarX' : sourceTermDict['gradTBar'][2][0],
        'gradTBarY' : sourceTermDict['gradTBar'][2][1],
        'cellCenterX' : offset[0],
        'cellCenterY' : offset[1]
        }

    FromTemplate(args=[os.path.join(caseDir, 'system/funkySetFieldsDict'), templateDict])

    # Prepare sampleDict
    angleList = np.linspace(0,2*pi,72) # One point every 5 deg

    pointList = [
        offset + np.array([rc*cos(theta), rc*sin(theta), 0]) 
        for theta in angleList
        ]

    strList = [
        '{0} {1} {2}'.format(pointList[i][0], pointList[i][1], pointList[i][2])
        for i in range(len(pointList))
        ]
    templateDict = { 'pointString' : '((' + ')('.join(strList) + '))' }

    ### Manual subsitution, because FromTemplate doesn't work
    cs.primitiveFromTemplate(os.path.join(caseDir, 'system/sampleDict'), templateDict)
    
def compute_reference(caseDir, verbose = 0):
    """Do stuff on a reference case."""

    argvList = []

    # Sample lines
    argvList.append([
        'sample',
        '-latestTime',
        '-case', os.path.join(caseDir)
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose=verbose)

def compute_average(caseDir, verbose = 0):
    """Do stuff on an average case."""

    argvList = []

    # Compute velocity deviation field
    argvList.append([
        "foamCalc", 
        'mag', 'UBar',
        "-case", os.path.join(caseDir)
        ])

    # Sample horizontal center line
    argvList.append([
        'sample',
        '-latestTime',
        '-case', os.path.join(caseDir)
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose = verbose)

def compute_average_curvature(caseDir, StudyParameters, cellList=None, verbose=0):
    """Compute curvature of dimensionless average temperature surface.

    For each cell specified in the StudyParameters.Cell.Grid section: The
    dimensionless temperature TBar+ = (TBar(r+)-TBar(x+))/TBar(x+) is
    computed. The space coordinates are adimensionized using the cell size as
    a reference. The curvature field of TBar+ if computed. The curvature field
    is then evaluated at the centroid x+ of the current cell. The resulting
    series of numbers is stored in a CSV table written to the postprocessing
    directory of the current study.

    Args:
        caseDir (os.path or string): Path to the average case to be processed.
        StudyParameters (ConfigObj or dictionary): StudyParameters configuration
            dictionary of the current study.
        cellList (dictionary, optional): Keys are the cell IDs, values are 3-element 
            tuples containing the coordinates of the cell centroid. If not present,
            the StudyParameters.Cell.Grid dictionary is used.
        verbose (int): Verbosity level. Defaults to 0.

    Returns:
        curvArray (list): A list containing list elements made of
            [ cellID, H, K, Pmin, Pmax ] 
            (very convenient for use with the tabulate library)

    """

    M  = int(StudyParameters['Reference']['Grid']['M'])
    N  = int(StudyParameters['Reference']['Grid']['N'])
    r0 = float(StudyParameters['Lengths']['r0'])

    if (cellList == None):
        cellList = StudyParameters['Cell']['Grid']

    # Load temperature m2 average
    TBar = Function2D_next()
    TBar.load(
        os.path.join(caseDir, "postProcessing/m2_TBar.csv"), 
        skiprows=1, 
        usecols=(0,1,3)
        )

    # Compute dimensionless coordinates
    refLength = r0
    TBarPlus = Function2D_next()

    # Create data structures to collect results
    curvatureData = []
    
    # Loop on cells
    for cellID in sorted(cellList.keys()):
        # Get cell center (trims the z coordinate if it has been issued)
        cellCenter = np.array(map(float, cellList[cellID]))[0:2]

        # Get average temperature at the center of the current cell
        interpolator = LinearNDInterpolator(TBar.data[:,0:2], TBar.data[:,2])
        TBarCenter = interpolator(cellCenter)[0]

        # Compute dimensionless temperature
        TBarPlus.clone(TBar)
        ### Trim field to the current cell (we just go a little further to
        ### make sure that the curvatures are correctly computed on edges)
        xmin = cellCenter[0] - 0.6*r0
        xmax = cellCenter[0] + 0.6*r0
        ymin = cellCenter[1] - 0.6*r0
        ymax = cellCenter[1] + 0.6*r0
        TBarPlus.trim(xmin, xmax, ymin, ymax)

        ### Adimensionize space coordinates and temperature
        TBarPlus.data[:,0:2] /= refLength
        TBarPlus.data[:,2] = (TBarPlus.data[:,2] - TBarCenter) / TBarCenter
        TBarPlus.refresh_data()

        # Compute the Gauss, mean and principal curvatures
        (Ki, Hi, Pmini, Pmaxi) = curvature(TBarPlus.zi, TBarPlus.dx, TBarPlus.dy)
        curvDict = { 'K' : Ki,
                     'H' : Hi,
                     'Pmin' : Pmini,
                     'Pmax' : Pmaxi
                    }

        # Evaluate curvature at cell centroid
        result = [cellID]
        for curvID in sorted(curvDict.keys()):
            temp = np.vstack((
                TBarPlus.xi.ravel(), 
                TBarPlus.yi.ravel(), 
                curvDict[curvID].ravel()
                )).T
            interpolator = LinearNDInterpolator(temp[:,0:2], temp[:,2])
            result.append(interpolator(cellCenter/refLength)[0])

        curvatureData.append(result)

    # Write results to CSV
    cellIDList = []
    for i in range(len(curvatureData)):
        cellIDList.append(curvatureData[i][0])
    tempData1 = np.array(cellIDList, dtype="string_", ndmin=2)

    tempData2 = np.zeros((len(curvatureData),4))
    for i in range(len(curvatureData)):
        tempData2[i,0] = curvatureData[i][1]
        tempData2[i,1] = curvatureData[i][2]
        tempData2[i,2] = curvatureData[i][3]
        tempData2[i,3] = curvatureData[i][4]

    csvData = np.hstack((tempData1.T, tempData2))
    header = "cellID,H,K,Pmin,Pmax"
    filename = os.path.join(caseDir, "postProcessing/curvature.csv")
    np.savetxt(
        filename,
        csvData,
        fmt="%s",
        header=header,
        delimiter=","
        )

    # We're done!
    return curvatureData


def compute_cell(caseDir, verbose = 0):
    "Do stuff on a cell case."

    CaseParameters = configobj.ConfigObj(os.path.join(caseDir, "CaseParameters.ini"))
    cellID = CaseParameters['Cell']['cellID']

    # Mapping of average data requires special treatment
    dst = os.path.join(caseDir, 'system/mapFieldsDict')
    src = os.path.join(caseDir, 'system/mapFieldsDict.average')
    shutil.copy(os.path.abspath(src), os.path.abspath(dst))
    
    # Map average fields
    argv = [
        "mapFields", 
        "-case", os.path.join(caseDir),
        "-mapMethod", "cellPointInterpolate",
        "-sourceTime", "latestTime",
        os.path.abspath(os.path.join(caseDir, '../../average'))
        ]
    cs.runSimpleUtility(argv, verbose=verbose)

    # Replace mapFieldsDict with the generic version
    src = os.path.join(caseDir, 'system/mapFieldsDict.generic')
    shutil.copy(os.path.abspath(src), os.path.abspath(dst))

    argvList = []

    # Map reference fields
    argvList.append([
        "mapFields", 
        "-case", os.path.join(caseDir),
        "-mapMethod", "cellPointInterpolate",
        "-sourceTime", "latestTime",
        os.path.abspath(os.path.join(caseDir, '../../reference'))
        ])

    # Map fields from the cell case
    argvList.append([
        "mapFields", 
        "-case", os.path.join(caseDir),
        "-mapMethod", "cellPointInterpolate",
        "-sourceTime", "latestTime",
        os.path.abspath(os.path.join(caseDir, '../../cell', cellID))
        ])

    # Reconstruct average fields
    argvList.append([
        "funkySetFields", 
        "-case", os.path.join(caseDir),
        "-time", "0",
        ])

    # Reconstruct velocity
    argvList.append([
        "foamCalc", 
        'addSubtract', 'UBarStar',
        'add', '-field', 'UTildeStar',
        '-resultName', 'UStar',
        "-case", os.path.join(caseDir)
        ])

    # Reconstruct temperature
    argvList.append([
        "foamCalc", 
        'addSubtract', 'TBarStar',
        'add', '-field', 'TTildeStar',
        '-resultName', 'TStar',
        "-case", os.path.join(caseDir)
        ])

    # Compute velocity deviation field
    argvList.append([
        "foamCalc", 
        'addSubtract', 'U',
        'subtract', '-field', 'UBar',
        '-resultName', 'UTilde',
        "-case", os.path.join(caseDir)
        ])

    # Compute temperature deviation field
    argvList.append([
        "foamCalc", 
        'addSubtract', 'T',
        'subtract', '-field', 'TBar',
        '-resultName', 'TTilde',
        "-case", os.path.join(caseDir)
        ])

    # Compute reconstructed wall heat flux
    # argvList.append([
    #     'wallHeatFluxLaminarDownscaling',
    #     '-latestTime',
    #     '-case', os.path.join(caseDir)
    #     ])

    # Compute precision stats
    argvList.append([
        'thermoDownscalingStats',
        '-case', os.path.join(caseDir)
        ])

    # Sample lines
    argvList.append([ 
        'sample',
        '-latestTime',
        '-case', os.path.join(caseDir)
        ])

    for argv in argvList:
        cs.runSimpleUtility(argv, verbose=verbose)

    cs.touchFoam(caseDir)