from distutils.core import setup

setup(
    name='DownscalingTools',
    version='dev',
    description='Tools for downscaling',
    author='Vincent Leroy',
    author_email='vincent.leroy@icmcb-bordeaux.cnrs.fr',
    packages=[
        'downscalingtools', 
        'downscalingtools.tools', 
        'downscalingtools.incompressible',
        'downscalingtools.thermo',
        'downscalingtools.sourcestudy',
        'downscalingtools.multiscale'
        ]
    )