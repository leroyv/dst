# DownscalingTools

A toolset to perform various tasks related with downscaling with OpenFOAM. This README is written using Markdown (python-markdown syntax, very close to the Github flavor's one). If you are looking for a no-frills markdown viewer, try the [Markdown Preview Plus](https://github.com/volca/markdown-preview) Chrome extension.

## Introduction

This toolset is designed to reduce the amount of code necessary to manage OpenFOAM downscaling cases. It supports several homebrew OpenFOAM solvers. It is based on version 2.2.2 of OpenFOAM and makes extensive use of the pyFoam library. All scripts are written in Python 2.7. See the list of dependencies.

## Install

It is recommended to install this script in a virtualenv. The install process is entirely managed by the `setup.py` script. Just execute the following command in a terminal:
    
    python setup.py install

## Use

Once the library is made available in the Python environment, it can be used to write run scripts for the specific case considered. The code is currently undocumented and will remain so until this projects exits the experimental research state.

## Dependencies

### Third-party applications and libraries

Software configuration (tested on Ubuntu 12.04 LTS and CentOS 6.5):

| Program          | Version        |
|:---------------- |:-------------- |
| OpenFOAM         | 2.2.2          |
| swak4foam        | 0.2.3--0.2.4   |
| Python           | 2.6.6--2.7.7   |
| pyFoam           | 0.6.2          |
| python-configobj | 5.0.4--5.0.5   |
| python-scipy     | 0.7.6--0.14.0  |
| python-numpy     | 1.4.1--1.6.1   |
| python-gnuplot   | 1.8            |
| gnuplot          | 4.4.3--4.6.5   |
| epstopdf         | 2.15--2.22     |

### Custom solvers and tools

| Program               | Version |
|:--------------------- |:------- |
| simplePrimitiveFoam   | 1.0.0   |
| simpleDownscalingFoam | 1.1.0   |
| average               | 2.0.0   |

## Release notes

### 1.0.0 (under development)

* First release
* Features
    * Supported downscaling problems: incompressible, thermo
    * Various common tools to ease programming and increase code modularity
