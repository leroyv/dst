# thermoDownscalingStats

A post-processing tool for thermal cell problems.

## Functions

Computes

* Volume dispersion indicators (S3, m3, M3)
* Surface dispersion indicators (S2, m2, M2)

Outputs

* TDif to the time directory from which input data is read

## Formulae

### Fields 

* Algebraic relative difference between the reference and reconstructed fields: `TDif1 = (TStar - T) / T`
* Algebraic relative difference between the reference and reconstructed fields (reference being the average at the center of the cell): `TDif2 = (TStar - T) / TBar(x)`
* Algebraic relative difference between the reference and reconstructed average fields: `TBarDif1 = (TBarStar - TBar)/TBar`
* Algebraic relative difference between the reference and reconstructed average fields (reference being the average at the center of the cell): `TBarDif2 = (TBarStar - TBar)/TBar(x)`
* Absolute relative difference between the reference and reconstructed deviation fields (reference being the absolute maximum reference deviation): `TTildeDif1 = abs(TTildeStar - TTilde)/max(abs(TTilde))`

### Indicators

#### Volume

Volume indicators are computed using the cell center values over the entire computational domain.

* Relative standard deviation (tells 'how wrong the reconstructed field is'): `S3 = sqrt(int_V(TDif1^2)/V)`
* Minimum absolute relative difference: `m3 = min_V(abs(TDif1))`
* Maximum absolute relative difference (tells 'how wrong is the wrongest value'): `M3 = max_V(abs(TDif1))`

#### Surface

Surface indicators are computed using the face center values over the `"cylinder.*"` patches.

* Relative standard deviation: `S2 = sqrt(int_A(TDif1^2)/A)`
* Minimum absolute relative difference: `m2 = min_A(abs(TDif1))`
* Maximum absolute relative difference: `M2 = max_A(abs(TDif1))`

## Upcoming features

* Output
    * Sampled volume data (cell x, y, z, volume, T, T*) for temperature fields for statistical computations
    * Sampled surface data (face x, y, z, surface, T, T*) for temperature fields for statistical computations

## Version history

### 1.2.1 (2014/12/04)

* Corrected the definition of S2 in the code, which yielded wrong results

### 1.2.0

* Changed the definition of the indicators

### 1.1.0

* Additional precision field indicator (see above)
* TDif renamed TDif2

### 1.0.0

* First version
* Implemented functionalities:
    * Computation of `TDif`
    * Volume spread indicators (S3, m3, M3)
    * Surface spread indicators (S2, m2, M2)