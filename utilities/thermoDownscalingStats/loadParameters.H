    // IOdictionary: system/sourceTermDict
    // Purpose: contains macroscopic source terms, including average reference temperature
    Info<< "Reading sourceTermDict:" << endl;
    IOdictionary sourceTermDict
    (
        IOobject
        (
            "sourceTermDict",
            runTime.constant(),
            mesh,
            IOobject::MUST_READ_IF_MODIFIED,
            IOobject::NO_WRITE
        )
    );

    dimensionedScalar TBarX(sourceTermDict.lookup("TBar"));
    Info<< "TBar(x) = " << TBarX.value() << endl;

    Info<< nl;