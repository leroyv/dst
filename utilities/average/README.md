# average

An averaging tool for 2D OpenFOAM cases. This tool applies the _m0_ averaging filter to various fields specified in the system/averageDict dictionary.

## Sample averageDict

    /*--------------------------------*- C++ -*----------------------------------*\
    | =========                 |                                                 |
    | \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
    |  \\    /   O peration     | Version:  2.2.2                                 |
    |   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
    |    \\/     M anipulation  |                                                 |
    \*---------------------------------------------------------------------------*/
    FoamFile
    {
        version     2.0;
        format      ascii;
        class       dictionary;
        location    "system";
        object      averageDict;
    }
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    fields
    (
        U
        T
    );

    filter
    {
        order  2;
        radius $radius$;
    }

    // ************************************************************************* //

Entry details:

* `fields`: list of words; names of the fields to be averaged
* `filter`: section which contains the settings of the filter and the Cartesian grid
    * `order`: int; order of the filter (only for the OF mesh output, the Cartesian grid output can only be applied a 0-th order filter)
    * `radius`: scalar; filter radius

## Upcoming features

* Choice of source time
* Write the results of every iteration
* Command-line order specification
* Multi-core computation (very work wow)

## Version history

### 2.0.1 (under development)

* Minor changes
    * All average orders are now output to CSV files

### 2.0.0 (2014/10/22)

* Major changes
    * Massive overhaul
    * Use of the average utility now requires the construction of a Cartesian mesh
    * Mesh size is set manually when constructing the grid: the grid parameters in the averageDict are now unused
    * First (_m0_) iteration is as slow as before
    * Subsequent (_m1_ and higher order) iterations duration depend on the target mesh resolution only (very fast if target mesh is coarse, potentially slower than the _m0_ iteration if target mesh is fine)
    * Results are still exported to CSV files for easy plotting with Matplotlib and such

### 1.1.0 (2014/10/17)

* New feature
    * Addition of filter order specification (only for OF mesh; Cartesian version will come later)

### 1.0.1 (2014/06/04)

* Minor changes
    * Enabled support for out-of-case call (using the -case optional argument)
* Bugfixes
    * Some unelegant console output stuff fixed

### 1.0.0 (2014/05/28)

* Features
    * Initial release
    * Single-core calculation only
    * Only in-case (without the -case optional argument) use possible
    * Available filter: _m0_ only
    * Average can be computed on both the OpenFOAM original mesh and a run-time defined Cartesian grid
