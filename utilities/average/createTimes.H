// Create times
Info<< "Creating source and target times" << nl << endl;

Time runTimeSource(Time::controlDictName, rootDirSource, caseDirSource);
Time runTimeTarget(Time::controlDictName, rootDirTarget, caseDirTarget);
