// Load source mesh
Info<< "Loading source mesh... ";
fvMesh meshSource
(
    IOobject
    (
        fvMesh::defaultRegion,
        runTimeSource.timeName(),
        runTimeSource
    )
);
Info<< "Source mesh size: " << meshSource.nCells() << endl;

// Load target mesh
Info<< "Loading target mesh... ";
fvMesh meshTarget
(
    IOobject
    (
        fvMesh::defaultRegion,
        runTimeTarget.timeName(),
        runTimeTarget
    )
);
Info<< "Target mesh size: " << meshTarget.nCells() << nl << endl;
