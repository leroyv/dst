# Generic study file

[Files]
    studyDirectory = 'study_nonuniform_01_nonlinearSource_hpc4/'
    templatesDirectory = 'templates_nonuniform_01/'

[Lengths]
    r0 = 0.1
    rcFactor = 0.4 # rc = r0*rcFactor

[Fluid]
    rho = 1.0    # Density
    mu  = 1e-5   # Dynamic viscosity
    cp  = 1000    # Heat capacity
    k   = 2e-2   # Conductivity

[Parametric] # List of configurations and where to find the velocity source files
    [[test01]]
        flowDirectory = /home/tomo/OpenFOAM/tomo-2.2.2/run/downscaling_incompressibleFlow/study_lowPor_hpc4/Re0.01
    [[test02]]
        flowDirectory = /home/tomo/OpenFOAM/tomo-2.2.2/run/downscaling_incompressibleFlow/study_lowPor_hpc4/Re0.1
    [[test03]]
        flowDirectory = /home/tomo/OpenFOAM/tomo-2.2.2/run/downscaling_incompressibleFlow/study_lowPor_hpc4/Re1
    [[test04]]
        flowDirectory = /home/tomo/OpenFOAM/tomo-2.2.2/run/downscaling_incompressibleFlow/study_lowPor_hpc4/Re10
    [[test05]]
        flowDirectory = /home/tomo/OpenFOAM/tomo-2.2.2/run/downscaling_incompressibleFlow/study_lowPor_hpc4/Re100

[Reference]
    [[Parallel]]
        nThread = 1
        nProc = 2
        decomposition = 1,2,1 # Domain decomposition in x,y,z directions
    [[Grid]]
        M = 10 # Number of columns
        N = 7  # Number of rows
    [[test01]]
        [[[TimeMarching]]]
            ddtScheme = backward
            deltaT = 100
            endTime = 20000
            writeInterval = 1000
        [[[BoundaryConditions]]]
            T0 = 300 # Inflow temperature
            S1 = (T<=3000) ? ((T>=0) ? (1.5+0.005)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T) : (0)) : (0) # Wall heat flux expression
            S2 = (T<=3000) ? ((T>=0) ? (1.5+0.005)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T): (0)) : (0) # Wall heat flux expression
    [[test02]]
        [[[TimeMarching]]]
            ddtScheme = backward
            deltaT = 100
            endTime = 20000
            writeInterval = 1000
        [[[BoundaryConditions]]]
            T0 = 300 # Inflow temperature
            S1 = (T<=3000) ? ((T>=0) ? (1.5+0.05)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T) : (0)) : (0) # Wall heat flux expression
            S2 = (T<=3000) ? ((T>=0) ? (1.5+0.05)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T): (0)) : (0) # Wall heat flux expression
    [[test03]]
        [[[TimeMarching]]]
            ddtScheme = backward
            deltaT = 100
            endTime = 20000
            writeInterval = 1000
        [[[BoundaryConditions]]]
            T0 = 300 # Inflow temperature
            S1 = (T<=3000) ? ((T>=0) ? (1.5+0.5)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T) : (0)) : (0) # Wall heat flux expression
            S2 = (T<=3000) ? ((T>=0) ? (1.5+0.5)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T): (0)) : (0) # Wall heat flux expression
    [[test04]]
        [[[TimeMarching]]]
            ddtScheme = backward
            deltaT = 100
            endTime = 10000
            writeInterval = 1000
        [[[BoundaryConditions]]]
            T0 = 300 # Inflow temperature
            S1 = (T<=3000) ? ((T>=0) ? (1.5+5)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T) : (0)) : (0) # Wall heat flux expression
            S2 = (T<=3000) ? ((T>=0) ? (1.5+5)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T): (0)) : (0) # Wall heat flux expression
    [[test05]]
        [[[TimeMarching]]]
            ddtScheme = backward
            deltaT = 10
            endTime = 2000
            writeInterval = 100
        [[[BoundaryConditions]]]
            T0 = 300 # Inflow temperature
            S1 = (T<=3000) ? ((T>=0) ? (1.5+50)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T) : (0)) : (0) # Wall heat flux expression
            S2 = (T<=3000) ? ((T>=0) ? (1.5+50)*10*1*(-4.44444444e-07*T*T + 1.33333333e-03*T): (0)) : (0) # Wall heat flux expression

[Average]
    [[Parallel]]
        nThread = 2
    [[Filter]]
        radius = 0.05001 # filter radius = r0/2
        order  = 2 # filter order
    [[Grid]]
        nX     = 100 # nX = (M-1) * 100 
        nY     = 70 # nY = (N-1) * 100

[Cell]
    [[Parallel]]
        nThread = 1
        nProc = 2 # Number of processors per thread
        decomposition = 1,2,1 # Domain decomposition in x,y,z directions
    [[Grid]] # List of offsets for the cells to operate on; keys are used as cell names
        # WARNING: cell names must be the exact same as in the flow problem 
        # (better copy-paste this section from the StudyParameters.ini in the downscaling_incompressibleFlow case)
        cell_02_04 = 0.1,0.3,0.0
        cell_02_05 = 0.1,0.4,0.0
        cell_02_06 = 0.1,0.5,0.0
        cell_06_04 = 0.5,0.3,0.0
        cell_06_05 = 0.5,0.4,0.0
        cell_06_06 = 0.5,0.5,0.0
        
[PostProcessing]
    [[Parallel]]
        nThread = 2
    [[LineList]]
        line2 = cell_02_04, cell_02_05, cell_02_06
        line6 = cell_06_04, cell_06_05, cell_06_06
    [[Plot]]
        # Number of mesh cells per element cell
        linesPlotDensity = 20
        cylinderPlotDensity = 50
