# Runs the averaging operation

import os, argparse, configobj, itertools, Queue, threading, time, datetime
import downscalingtools.thermo.average as average
import downscalingtools.thermo.configuration as configuration
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs
from downscalingtools.tools.thread import *
from downscalingtools.tools.case import detectLatestTime

def runner(param):
    """
    Worker thread function.
    """

    args = param['args']
    StudyParameters = param['StudyParameters']
    taskQueue = param['taskQueue']
    printer = param['printer']

    # Process arguments
    steps = {
            'prep' : True,
            'run' : True
        }
    if args.onlyprep:
        steps['run'] = False
    elif args.onlyrun:
        steps['prep'] = False

    while not(taskQueue.empty()):

        # Get task parameters
        task = taskQueue.get()
        taskQueue.task_done()
        taskID = task['ID']
        confID = task['confID']

        # Gather directories
        studyDir = StudyParameters['Files']['studyDirectory']
        templateDir = os.path.join(StudyParameters['Files']['templatesDirectory'], 'average')
        runDir = os.path.join(studyDir, taskID, 'average')
        referenceDir = os.path.join(studyDir, taskID, 'reference')

        message = "{0} AVERAGE CASE ({1} jobs left) {2}".format(
            datetime.datetime.now(), taskQueue.qsize(), runDir)
        printer.put(PrintBufferElement(message))

        if steps['prep']:
            # Compute diffusivity and so on
            AdditionalParameters = configuration.process(StudyParameters)

            # Copy template files
            fs.copydir(templateDir, runDir)

            # Copy reference files
            ### Generate list of files
            copyList = [
                [ 'system/controlDict'           , 'system/controlDict' ],
                [ 'system/decomposeParDict'      , 'system/decomposeParDict' ],
                [ 'constant/transportProperties' , 'constant/transportProperties' ],
                [ 'constant/polyMesh/boundary'   , 'constant/polyMesh/boundary' ],
                [ 'constant/polyMesh/faces'      , 'constant/polyMesh/faces' ],
                [ 'constant/polyMesh/neighbour'  , 'constant/polyMesh/neighbour' ],
                [ 'constant/polyMesh/owner'      , 'constant/polyMesh/owner' ],
                [ 'constant/polyMesh/points'     , 'constant/polyMesh/points' ]
                ]
            latestTime = detectLatestTime(referenceDir)
            copyList.append([ os.path.join(latestTime,'U'), '0/U' ])
            copyList.append([ os.path.join(latestTime,'T'), '0/T' ])
            ### Copy files
            fs.copyfilelist(referenceDir, os.path.join(runDir, 'input/reference'), copyList)

            # Prepare CaseParameters.ini
            CaseParameters = configobj.ConfigObj()
            
            CaseParameters['GridCells'] = StudyParameters['Cell']['Grid'].dict()

            CaseParameters['Dictionaries'] = {
                'system/averageDict' : { 
                    'order'  : StudyParameters['Average']['Filter']['order'], 
                    'radius' : StudyParameters['Average']['Filter']['radius'], 
                    },
                'constant/polyMesh/blockMeshDict' : { 
                    'nX'     : StudyParameters['Average']['Grid']['nX'],
                    'nY'     : StudyParameters['Average']['Grid']['nY']
                    },
                }

            CaseParameters.filename = os.path.join(runDir, 'CaseParameters.ini')
            CaseParameters.write()

        # Execute step
        startDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} START".format(startDate)))

        if steps['prep']:
            average.prepareMutables(runDir, verbose=args.verbose)
        if steps['run']:
            average.compute(runDir, verbose=args.verbose)

        cs.touchFoam(runDir)

        endDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} END (exec time = {1})".format(endDate, endDate-startDate)))

    # count workers still running
    activeThreadsList = threading.enumerate()
    nActive = 0
    for t in activeThreadsList:
        if "runner" in t.name: nActive += 1
    printer.put(PrintBufferElement("{0} Work finished ({1} worker threads still running)".format(
        datetime.datetime.now(), nActive-1)))

def main():
    # Handle keyboard interruption
    try:
        # Process arguments
        parser = argparse.ArgumentParser()
        parser.add_argument('studyfile', action="store", help="path to study file")
        parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
        # The following three are mutually exclusive
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--onlyprep', action="store_true", help="only prepare case")
        group.add_argument('--onlyrun', action="store_true", help="only run average computation")
        args = parser.parse_args()

        # Spawn printer
        printer = Printer()

        # Process configuration file
        configFilePath = args.studyfile
        StudyParameters = configobj.ConfigObj(configFilePath)
        
        # Create and fill task queue
        confIDList = StudyParameters['Parametric'].keys()
        taskQueue = Queue.Queue()

        for confID in confIDList:
            task = {}
            task['ID'] = confID
            task['confID'] = confID
            taskQueue.put(task)

        # Spawn runner threads
        param = {
            'args' : args,
            'StudyParameters' : StudyParameters,
            'taskQueue' : taskQueue,
            'printer' : printer
            }

        threadPool = []
        nThread = int(StudyParameters['Average']['Parallel']['nThread'])
        for i in range(nThread):
            t = threading.Thread(
                target=runner, 
                args=(param,), 
                name="runner{0}".format(i)
                )
            t.daemon = True
            threadPool.append(t)

        # Start threads
        printer.start()
        for t in threadPool: t.start()

        # Main thread only waits until the others have finished their jobs
        while len(threading.enumerate()) > 2:
            time.sleep(1) 

    # This handles keyboard interruption
    except KeyboardInterrupt:
        print '\n! Received keyboard interrupt, quitting threads.\n'

if __name__ == '__main__':
    main()
