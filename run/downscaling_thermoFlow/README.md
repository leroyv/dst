# downscaling_thermoFlow

An OpenFOAM case for the downscaling of incompressible, steady, anisothermal flows, with weak coupling between flow and energy (i.e. the flow is 'frozen'). This README is written using Markdown (python-markdown syntax, very close to the Github flavor's one). If you are looking for a no-frills markdown viewer, try the [Markdown Preview Plus](https://github.com/volca/markdown-preview) Chrome extension or the [Markdown Viewer](https://github.com/Thiht/markdown-viewer) Firefox extension.

## Introduction

The purpose of this OpenFOAM case is to allow parametric studies for the investigation of the downscaling procedure for anisothermal flows. This case is based on version 2.2.2 of OpenFOAM and make extensive use of the pyFoam library. All run scripts are written in Python 2.7. See the list of dependencies.

## Use

This case comes with a bunch of template files which are used to perform the runs. Template files can be modified and forked at will. The complete run sequence is the following:

* Run the reference case: runReference.py
* Run the averaging operation: runAverage.py
* Run the cell cases: runCell.py
* Post-process the results: runPostProcess.py
* Plot the results: runPlot.py

The core study parameters are defined in a _study file_, using the INI syntax described in the [configobj](http://www.voidspace.org.uk/python/configobj.html#the-config-file-format) Python module. A sample study file is available in the templates/ directory.

## Vocabulary

Here are a few vocabulary definitions:

* A study is a collection of configurations to be used as input for a run
* Configuration: set of parameters; each configuration is associated with a unique confID
* A run is divided in steps, divided in tasks
* Run: complete step sweep (reference, average, cell, postprocessing, plotting)
* Step: execution of the tasks (reference, average, cell and postprocessing are steps)
* Tasks: collection of commands and data manipulation; there are several types of tasks, depending on the step considered
* Case: OpenFOAM case; each step, except plotting, is associated to a corresponding case

Sample directories: 
* Study: study/
* Configuration: study/Re_100/
* Step: study/Re_100/reference/
* Template: templates/
* Case: every step directory containing OpenFOAM case files is a case directory

## About the sample template files

Templates are provided and allow to simulate the reference and associated cell problems for an anisothermal flow in a cylinder array. The upper and lower edges of the domain are forced at a 300 K, like in the test case by Angeli et al. (2013).

## Parameter sweeping

No parameter sweeping is possible at the moment.

## How to build the mesh

The meshes stored in the templates/ directory are built using the SALOME mesher and the [salomeToOpenFOAM](https://github.com/nicolasedh/salomeToOpenFOAM) script.

## Dependencies

### Third-party applications and libraries

| Program          | Version      |
|:---------------- |:------------ |
| OpenFOAM         | 2.2.2        |
| swak4foam        | 0.2.3        |
| Python           | 2.6.6--2.7.7 |
| pyFoam           | 0.6.2        |
| python-configobj | 5.0.4        |
| python-scipy     | 0.14.0       |
| python-numpy     | 1.8.1        |

### Custom solvers and tools

| Program               | Version |
|:--------------------- |:------- |
| thermoPrimitiveFoam   | 1.0.0   |
| thermoDownscalingFoam | 1.0.0   |
| average               | 1.1.0   |

## Upcoming features

* Parametric run as a function of the Damköhler number

## Current issues

Nothing yet.

## Version history

### 1.0.0 (under development)

* Features
    * Initial release
    * Supported study steps:
        * Run reference case
        * Run averaging procedure (_m0_ filter for basic output, _m2_ filter for source term computation)
        * Run cell case for user-defined cells
        * Run post-processing operations in an independent case for proper visualization
        * Run plot scripts
    * Case packaging to archive the results in a compact way
    * Use of a triangle mesh generated using the GMSH mesher
    * Basic graphical monitoring of residues
    * Update mode for the reference case for faster execution of cases with fine meshes (no remap of velocity when enabled)
* Limitations
    * Support for flows with an adherence boundary condition at the cylinder-fluid interface

## Note about version numbering

The code version numbering is done using a three-digit convention, plus an additional string for alpha and beta versions. Here are the cases to be considered:

* Production release: vX.Y.Z. X is the major release number and changes only when a major overhaul of the code is done. Y is the minor release number, and changes when major features or changes are implemented. Z is the bug fix number, and changes when important bug fixes need to be included before the next minor release. Examples: 1.0.0 for the the very first production release; 1.0.1 for the first bug fix release; 1.1.0 for the first minor release; 2.0.0 for the next big overhaul of the code; etc.
* Test release: vX.0yZ. X is the major release number. y is a letter, denoting the alpha (a), beta (b) or release candidate (rc) state of the code. Z is the release number. Examples: 1.0a1 for the very first alpha release; 1.0b4 for the fourth beta release; 1.0rc2 for the second release candidate.
