# Runs the plotting tools
# http://stackoverflow.com/questions/9008370/python-2d-contour-plot-from-3-lists-x-y-and-rho
# http://wiki.scipy.org/Cookbook/Histograms
# http://matplotlib.org/examples/images_contours_and_fields/pcolormesh_levels.html

import argparse, configobj, os, scipy.interpolate, math
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs
import numpy as np
from downscalingtools.tools.mathematics import frexp10
from downscalingtools.tools.plot import tickloc, detect_coordinates
from mpl_toolkits.axes_grid1 import make_axes_locatable
from math import pi
from tabulate import tabulate

# Configure matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker
### Force matplotlib to not use any Xwindows backend.
mpl.use('agg')
### Change math font to sans serif
mpl.rcParams['mathtext.fontset'] = 'stixsans'

def plot_average(confID, StudyParameters, outputExtension):
    """Plot the average fields in the confDir configuration directory."""

    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)
    outputDir = os.path.join(confDir, 'postProcessing/output')

    fs.mkdir_p(outputDir)
    
    for filterName in ['m0', 'm1', 'm2']:
        # Temperature
        ### Load data from the average step results
        fileName = os.path.join(confDir, 'average/postProcessing', filterName + '_TBar.csv')
        data = np.genfromtxt(fileName, skip_header=1, usecols=(0,1,3), delimiter=',')
        points = data[:,0:2]

        ### Extract grid values
        xv, yv = detect_coordinates(points, 1e-6)
        
        ### Plot
        plot_average_temperature(
            data, xv, yv, 
            os.path.join(outputDir, filterName + '_TBar.' + outputExtension)
            )

        # Velocity
        ### Load data from the average step results
        fileName = os.path.join(confDir, 'average/postProcessing', filterName + '_UBar.csv')
        data = np.genfromtxt(fileName, skip_header=1, usecols=(0,1,3,4,6), delimiter=',')
        points = data[:,0:2]

        ### Extract grid values
        xv, yv = detect_coordinates(points, 1e-6)

        ### Quiver plot settings
        quiverSettings = {
            'bounds' : (0, 0.9, 0, 0.6),
            'nPoints': (10,7)
        }

        ### Plot
        plot_average_velocity(data, xv, yv, quiverSettings, 
            os.path.join(outputDir, filterName + '_UBar.' + outputExtension)
            )

def plot_average_temperature(data, xv, yv , fileName):
    points = data[:,0:2]
    T      = data[:,2]

    # Set up a regular grid of interpolation points
    xi, yi = np.meshgrid(xv,yv)

    # Interpolate
    Ti = scipy.interpolate.griddata(points, T, (xi, yi))

    # Plot temperature color map
    plt.figure()
    ax = plt.gca()
    cmap = plt.get_cmap('coolwarm')
    im = ax.imshow(Ti, vmin=Ti.min(), vmax=Ti.max(), 
        extent=[xv[0], xv[-1], yv[0], yv[-1]], 
        origin='lower', interpolation='nearest',
        cmap=cmap
        )

    # Plot temperature contour
    ax.contour(xi,yi,Ti,colors='k')
    
    # Set plot details
    plt.title(r'Average temperature [K]')
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$y$ [m]')

    # Add colorbar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="3%", pad=0.1)
    bar = plt.colorbar(im, cax=cax)

    bar.locator = matplotlib.ticker.FixedLocator(tickloc(T.min(), T.max()))
    bar.formatter = matplotlib.ticker.ScalarFormatter(useMathText=True) 
    bar.update_ticks()

    # Finish
    plt.tight_layout()
    plt.savefig(fileName,dpi=150)
    plt.close()

def plot_average_velocity(data, xvUMag, yvUMag, quiverSettings, fileName):
    points = data[:,0:2]
    Ux     = data[:,2]
    Uy     = data[:,3]
    UMag   = data[:,4]

    # Intensity
    ### Interpolate
    xv, yv = xvUMag[:], yvUMag[:]
    xi, yi = np.meshgrid(xv,yv)
    UMagi = scipy.interpolate.griddata(points, UMag, (xi, yi))

    ### Plot
    cmap = plt.get_cmap('coolwarm')

    plt.imshow(UMagi, vmin=UMagi.min(), vmax=UMagi.max(), 
        extent=[xv[0], xv[-1], yv[0], yv[-1]], 
        origin='lower', interpolation='nearest',
        cmap=cmap
        )
    bar = plt.colorbar()

    bar.locator = matplotlib.ticker.FixedLocator(tickloc(UMag.min(), UMag.max()))
    bar.formatter = matplotlib.ticker.ScalarFormatter(useMathText=True) 
    bar.formatter.set_powerlimits((-1,1))
    bar.update_ticks()
    
    # Direction
    ### Interpolate
    bounds = quiverSettings['bounds']
    nPoints = quiverSettings['nPoints']
    xv = np.linspace(bounds[0], bounds[1], nPoints[0])
    yv = np.linspace(bounds[2], bounds[3], nPoints[1])

    xi, yi = np.meshgrid(xv,yv)
    Uxi = scipy.interpolate.griddata(points, Ux, (xi, yi))
    Uyi = scipy.interpolate.griddata(points, Uy, (xi, yi))
    UMagi = scipy.interpolate.griddata(points, UMag, (xi, yi)) 

    ### Plot
    plt.quiver(xv, yv, Uxi/UMagi, Uyi/UMagi, pivot='middle')

    # Set plot details
    plt.title(r'Average velocity [m/s]')
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$y$ [m]')

    # Finish
    plt.savefig(fileName,dpi=150)
    plt.close()

def plot_hlines(confID, StudyParameters, outputExtension):
    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)
    outputDir = os.path.join(confDir, 'postProcessing/output')

    fs.mkdir_p(outputDir)

    # Gather average data
    tempDir = os.path.join(confDir, 'average/postProcessing/sets')
    fileName = str(os.path.join(tempDir, cs.detectLatestTime(tempDir), \
        'line_TBar_magUBar.csv') )
        
    ### Load and sort data
    tempArray = np.loadtxt(fileName, delimiter=',', dtype=float, skiprows=1)
    tempArray = tempArray[tempArray[:,0].argsort()]
    results = {}
    results['mesh'] = tempArray[:,0]
    results['TBar'] = tempArray[:,1]
    results['magUBar'] = tempArray[:,2]

    # Draw TBar plot
    plt.figure(1)
    plt.plot(results['mesh'],  results['TBar'], 'k-', label=r'$T$')
    plt.title('TBar')
    plt.xlim([results['mesh'].min(), results['mesh'].max()])
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$\overline{T}$ [K]')

    plt.tight_layout()
    fileName = os.path.join(outputDir, 'TBar_hline.' + outputExtension)
    plt.savefig(fileName,dpi=150)
    plt.close()

    # Draw UBar plot
    plt.figure(1)
    plt.plot(results['mesh'],  results['magUBar'], 'k-', label=r'$T$')
    plt.title('magUBar')
    plt.xlim([results['mesh'].min(), results['mesh'].max()])
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$\left|\overline{U}\right|$ [K]')

    plt.tight_layout()
    fileName = os.path.join(outputDir, 'UBar_hline.' + outputExtension)
    plt.savefig(fileName,dpi=150)
    plt.close()

def plot_vlines(confID, StudyParameters, outputExtension):
    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)
    outputDir = os.path.join(confDir, 'postProcessing/output')

    fs.mkdir_p(outputDir)

    # Define lines
    lineIDList = StudyParameters['PostProcessing']['LineList'].keys()

    # TODO: add line consistency check (cells must be adjacent)
    # TODO: add automatic line building in reference sampleDict
    # TODO: if density = 0, then do not resample

    # Define output mesh
    r0 = float(StudyParameters['Lengths']['r0'])
    density = int(StudyParameters['PostProcessing']['Plot']['linesPlotDensity'])
    results = {}
    for lineID in lineIDList:
        cellIDList = StudyParameters['PostProcessing']['LineList'][lineID]
        meshMin = 1e300
        meshMax = -1e300
        for cellID in cellIDList:
            center = map(float, StudyParameters['Cell']['Grid'][cellID])
            if center[1] - r0*0.5 < meshMin:
                meshMin = center[1] - r0*0.5
            if center[1] + r0*0.5 > meshMax:
                meshMax = center[1] + r0*0.5

        results[lineID] = { 'meshInterp' : np.linspace(meshMin, meshMax, num = len(cellIDList) * density + 1) }

    # Gather reference data
    for lineID in lineIDList:
        meshInterp = results[lineID]['meshInterp']

        tempDir = os.path.join(confDir, 'reference/postProcessing/sets')
        fileName = str(os.path.join(tempDir, cs.detectLatestTime(tempDir), \
            lineID + '_T.csv') )
        
        ### Load and sort data
        tempArray = np.loadtxt(fileName, delimiter=',', dtype=float, skiprows=1)
        tempArray = tempArray[tempArray[:,0].argsort()]
        results[lineID]['meshRef'] = tempArray[:,0]
        results[lineID]['TRef'] = tempArray[:,1]
        results[lineID]['TInterp'] = np.interp(meshInterp, tempArray[:,0], tempArray[:,1])

    # Gather cell data
    for lineID in lineIDList:
        meshInterp = results[lineID]['meshInterp']

        dataList = []
        cellIDList = StudyParameters['PostProcessing']['LineList'][lineID]

        for cellID in cellIDList:

            tempDir = os.path.join(confDir, 'postProcessing', cellID, 'postProcessing/sets')
            fileName = str(os.path.join(tempDir, cs.detectLatestTime(tempDir), \
                'patchSeed_in_T_TStar.csv') )

            tempArray = np.genfromtxt(fileName, delimiter=',', dtype=float, skip_header=1, usecols=(0,2))
            dataList.append(tempArray)

            ### Aggregate data
            tempArray = np.empty((0,2))
            for i in range(len(dataList)):
                tempArray = np.concatenate((tempArray,dataList[i]))

            ### Sort data, interpolate and store
            tempArray = tempArray[tempArray[:,0].argsort()]
            results[lineID]['meshCell'] = tempArray[:,0]
            results[lineID]['TStarCell'] = tempArray[:,1]
            results[lineID]['TStarInterp'] = np.interp(meshInterp, tempArray[:,0], tempArray[:,1])
    
    # Output raw data to csv files
    for lineID in lineIDList:
        tempArray = np.vstack((results[lineID]['meshRef'], results[lineID]['TRef']))
        np.savetxt(os.path.join(confDir, 'postProcessing/output', 'T_' + lineID + '_raw.csv'), \
            tempArray.T, delimiter=',', header='y,T'
            )

        tempArray = np.vstack((results[lineID]['meshCell'], results[lineID]['TStarCell']))
        np.savetxt(os.path.join(confDir, 'postProcessing/output', 'TStar_' + lineID + '_raw.csv'), \
            tempArray.T, delimiter=',', header='y,TStar'
            )

    # Output interpolated results to csv files
    for lineID in lineIDList:
        meshInterp     = results[lineID]['meshInterp']
        TInterp        = results[lineID]['TInterp']
        TStarInterp    = results[lineID]['TStarInterp']
        tempArray = np.vstack((meshInterp, TInterp, TStarInterp))
        np.savetxt(os.path.join(confDir, 'postProcessing/output', 'T_' + lineID + '_interp.csv'), \
            tempArray.T, delimiter=',', header='y,T,TStar'
            )

    # Draw raw data plots
    nLines = len(lineIDList)
    fig, axs = plt.subplots(nLines, 1, figsize=(9,5*nLines))

    for i in range(nLines):
        lineID = lineIDList[i]

        if nLines > 1:
            ax = axs[i]
        else:
            ax = axs
        
        ax.plot(results[lineID]['meshRef'],  results[lineID]['TRef'],      'g-', label=r'$T$')
        ax.plot(results[lineID]['meshCell'], results[lineID]['TStarCell'], 'ro', label=r'$T^*$')

        ax.set_title(lineID)
        ax.set_xlim([results[lineID]['meshInterp'].min(), results[lineID]['meshInterp'].max()])
        ax.set_xlabel(r'$y$ [m]')
        ax.set_ylabel(r'[K]')
        ax.legend()

    plt.tight_layout()
    fileName = os.path.join(outputDir, 'T_lines_raw.' + outputExtension)
    plt.savefig(fileName,dpi=150)
    plt.close()

    # Draw interpolated plots
    nLines = len(lineIDList)
    fig, axs = plt.subplots(nLines, 1, figsize=(9,5*nLines))
    
    for i in range(nLines):
        lineID = lineIDList[i]
        fileName = os.path.join(outputDir, 'T_' + lineID+ '_interp.csv')
        data = np.loadtxt(fileName, delimiter=',', dtype=float)

        axs[i].plot(data[:,0], data[:,1], 'g-', label=r'$T$')
        axs[i].plot(data[:,0], data[:,2], 'ro', label=r'$T^*$')

        axs[i].set_title(lineID)
        axs[i].set_xlim([data[:,0].min(), data[:,0].max()])
        axs[i].set_xlabel(r'$y$ [m]')
        axs[i].set_ylabel(r'[K]')
        axs[i].legend()

    plt.tight_layout()
    fileName = os.path.join(outputDir, 'T_lines_interp.' + outputExtension)
    plt.savefig(fileName,dpi=150)
    plt.close()

def plot_cylinders(confID, StudyParameters, outputExtension, polar=False):
    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)
    outputDir = os.path.join(confDir, 'postProcessing/output')

    fs.mkdir_p(outputDir)

    cellIDList = StudyParameters['Cell']['Grid'].keys()
    results = {}
    density = int(StudyParameters['PostProcessing']['Plot']['cylinderPlotDensity'])

    # Gather and interpolate data
    for cellID in cellIDList:

        tempDir = os.path.join(confDir, 'postProcessing', cellID, 'postProcessing/sets')
        fileName = str(os.path.join(tempDir, cs.detectLatestTime(tempDir), \
            'patchSeed_cylinder_T_TStar.csv') )
        tempArray = np.genfromtxt(fileName, delimiter=',', dtype=float, skip_header=1, usecols=(0,1,2))

        ### Sort data, interpolate and store
        tempArray = tempArray[tempArray[:,0].argsort()]
        mesh = np.linspace(tempArray[0,0],tempArray[-1,0]+1,density)
        T = np.interp(mesh, tempArray[:,0], tempArray[:,1])
        TStar = np.interp(mesh, tempArray[:,0], tempArray[:,2])

        ### Replace mesh with a degree scale
        mesh = np.linspace(0,360,density)
        
        tempArray = np.vstack((mesh, T, TStar)).T
        results[cellID] = tempArray

    # Output results to csv files
    for cellID in cellIDList:
        mesh     = results[cellID][:,0]
        T        = results[cellID][:,1]
        TStar    = results[cellID][:,2]
        tempArray = np.vstack((mesh, T, TStar))
        np.savetxt(os.path.join(confDir, 'postProcessing/output', 'T_' + cellID + '.csv'), \
            tempArray.T, delimiter=',', header='phi,T,TStar'
            )
    
    # Draw plots
    nCells = len(cellIDList)
    n = int(math.ceil(math.sqrt(nCells)))
    m = int(math.ceil(float(nCells)/n))

    fig, axs = plt.subplots(n, m, figsize=(9*m,5*n)) if (polar==False) else \
               plt.subplots(n, m, figsize=(6*m,6*n), subplot_kw=dict(polar=True))
    axs = axs.ravel() # Flatten the axs array if it has monre than 1 index

    for i in range(nCells):
        cellID = cellIDList[i]
        data = results[cellID]

        if (polar==True):
            # Change abscissa from degree to radian
            data[:,0] *= np.pi/180.

        axs[i].plot(data[:,0], data[:,1], 'g-', label=r'$T$')
        axs[i].plot(data[:,0], data[:,2], 'ro', label=r'$T^*$')

        axs[i].set_title(cellID)
        axs[i].set_xlim([data[:,0].min(), data[:,0].max()])
        axs[i].set_xlabel(r'$\phi$ [$^\circ$]')
        axs[i].set_ylabel(r'[K]')
        axs[i].legend()

    plt.tight_layout()
    fileName = os.path.join(outputDir, 'T_cylinders.' + outputExtension)
    plt.savefig(fileName,dpi=150)
    plt.close()

def plot_curv(confID, StudyParameters, outputExtension, verbose=0):     
    """Plot precision as a function of curvature.

    This function plots the precision indicators M3 and S3 in all cells of the
    given configuration, as a function of the mean curvature of the
    dimensionless temperature field.

    Args:
        confID (string): ID of the configuration to process.
        StudyParameters (ConfigObj or dictionary): StudyParameters configuration
            dictionary of the current study.
        outputExtension (string): File extension of the output files.

    """

    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)
    outputDir = os.path.join(confDir, 'postProcessing/output')
    fs.mkdir_p(outputDir)

    # Gather data
    ### Get curvature data
    filename = os.path.join(confDir, "average/postProcessing/curvature.csv")
    curvData = np.loadtxt(filename, delimiter=',', usecols=(1,2,3,4))

    ### Cell ID list (a bit tricky, we want a 6x1 array to assemble with the
    ### rest afterwards)
    cellData = np.genfromtxt(filename, delimiter=',', usecols=(0), dtype='string_')
    cellIDList = [x for x in cellData]
    cellData = np.array(cellIDList, dtype='string_', ndmin=2).T
    
    ### Get precision data
    indicatorList = [ 'M3', 'S3', 'M2', 'S2' ]
    precData = []

    for cellID in cellIDList:
        filename = os.path.join(
            confDir, "postProcessing", cellID, 
            "PyFoamUtility.thermoDownscalingStats.logfile"
            )
        data = {}
        with open(filename, 'r') as infile:
            for line in infile:
                for indicator in indicatorList:
                    if ( (indicator + " = ") in line ):
                        data[indicator] = float(line.split(' ')[-1])
        precData.append([data[indicator] for indicator in indicatorList])

    precData = np.array(precData)

    ### Aggregate data
    data = np.hstack((curvData, precData))
    headerList = ['cellID', 'H', 'K', 'Pmin', 'Pmax', 'M3', 'S3', 'M2', 'S2' ]

    # Write data to CSV
    if verbose >= 1:
        print(tabulate(
            np.hstack((cellData, data)),
            headers=headerList
            ))

    filename = os.path.join(outputDir, 'precision.csv')
    np.savetxt(
        filename, 
        np.hstack((cellData, data)),
        fmt="%s",
        delimiter=',',
        header=(','.join(headerList))
        )

    # Plot that shite
    fig, axs = plt.subplots(2,2, figsize=(20,15))
    axs = axs.ravel()

    for i in range(len(axs)):
        ax = axs[i]
        xindex = 0
        yindex = 4+i
        xi = data[:,xindex]
        yi = data[:,yindex]

        xmin = xi.min()
        xmax = xi.max()
        deltax = (xmax-xmin)/20

        ymin = yi.min()
        ymax = yi.max()
        deltay = (ymax-ymin)/20        

        ax.scatter(xi, yi)
        ax.set_xlabel(headerList[1+xindex])
        ax.set_ylabel(headerList[1+yindex])
        ax.set_xlim([xmin-deltax, xmax+deltax])
        ax.set_ylim([ymin-deltay, ymax+deltay])

        for label, x, y in zip(cellIDList, xi, yi):
            ax.annotate(
                label, 
                xy = (x, y), xytext = (-20, 20),
                textcoords = 'offset points', ha = 'right', va = 'bottom',
                bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
                arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    
    plt.tight_layout()    
    filename = os.path.join(outputDir, 'precision.' + outputExtension)
    plt.savefig(filename,dpi=150)
    plt.close()

def plot_precision(confID, StudyParameters, outputExtension, verbose=0):
    """Plot precision as a function of cell centroid coordinates (map).

    Precision indicators are plotted on a 2D color map. The data used comes
    from the precision.csv file generated during the execution of the
    plot_curv() function.

    Args:
        confID (string): ID of the configuration to process.
        StudyParameters (ConfigObj or dictionary): StudyParameters configuration
            dictionary of the current study.
        outputExtension (string): File extension of the output files.

    """

    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)
    outputDir = os.path.join(confDir, 'postProcessing/output')
    fs.mkdir_p(outputDir)

    # Load data
    filename = os.path.join(outputDir, 'precision.csv')
    dataCellID = np.genfromtxt(filename, delimiter=',', dtype=None, usecols=(0))
    dataPrecision = np.genfromtxt(
        filename, 
        delimiter=',', 
        dtype=float, 
        usecols=(5,6,7,8)
        )

    # Build data set suitable for plotting
    tempList = []
    for cellID in dataCellID:
        tempList.append(map(float,StudyParameters['Cell']['Grid'][cellID][0:2]))
    xy = np.array(tempList)
    data = np.hstack((xy, dataPrecision))

    # Draw scatter plot
    fig, axs = plt.subplots(2,2, figsize=(20,15))
    axs = axs.ravel()

    headerList = [ r'$M_3$', r'$S_3$', r'$M_2$', r'$S_2$' ]

    for i in range(len(axs)):
        ax = axs[i]
        zindex = 2+i
        xv = data[:,0]
        yv = data[:,1]
        zv = data[:,zindex]

        xmin = -0.05
        xmax = 0.95
        deltax = 0.

        ymin = -0.05
        ymax = 0.65
        deltay = 0.

        im = ax.scatter(xv, yv, c=zv, cmap=plt.get_cmap('coolwarm'), marker='s', s=3000, vmin=0)
        ax.set_aspect('equal')
        ax.set_xlabel(r'$x$ [m]')
        ax.set_ylabel(r'$y$ [m]')
        ax.set_xlim([xmin-deltax, xmax+deltax])
        ax.set_ylim([ymin-deltay, ymax+deltay])
        ax.set_title(headerList[i])

        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="3%", pad=0.1)
        plt.colorbar(im, cax=cax)
    
    plt.tight_layout()    
    filename = os.path.join(outputDir, 'precision_map.' + outputExtension)
    plt.savefig(filename,dpi=150)
    plt.close()
    

def main():
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('studyfile', action="store", help="path to study file")
    parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
    parser.add_argument('--format', action="store", default="png", choices=['png','pdf'], help="select output format (defaults to png)")
    args = parser.parse_args()

    format = args.format

    StudyParameters = configobj.ConfigObj(args.studyfile)

    # Plot average fields
    for confID in StudyParameters['Parametric'].keys():
        plot_average(confID, StudyParameters, format)

    # Plot lines
    lineIDList = StudyParameters['PostProcessing']['LineList'].keys()

    for confID in StudyParameters['Parametric'].keys():
        plot_hlines(confID, StudyParameters, format)
        plot_vlines(confID, StudyParameters, format)
        plot_cylinders(confID, StudyParameters, format, polar=True)

    # Draw precision plots
    for confID in StudyParameters['Parametric'].keys():
        plot_curv(confID, StudyParameters, format, verbose=args.verbose)
        plot_precision(confID, StudyParameters, format, verbose=args.verbose)

if __name__ == '__main__':
    main()