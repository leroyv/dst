# Runs the post-processing tools

import argparse, os, configobj, Queue, threading, time, datetime
from pprint import pprint
from downscalingtools.tools.thread import *
from downscalingtools.tools.case import detectLatestTime
import downscalingtools.thermo.postprocess as postprocess
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs

def runner(param):
    """
    Worker thread function.
    """

    # Process parameters for more convenience
    printer = param["printer"]
    taskManager = param["taskManager"]
    args = param["args"]

    while not(taskManager.taskQueue.empty()):

        task = None

        try:
            task = taskManager.pick()
        except TaskManagerException: 
            # No task has its prerequisite met: wait for 1s then retry
            printer.put(PrintBufferElement("No task ready to be executed, waiting for other threads to finish"))
            time.sleep(1)

        if ( not(task == None) ): # This means a task was successfully picked
            taskID = task.ID
            taskType = task.data["type"]
            printer.put(PrintBufferElement("Picked task {0} of type {1}".format(taskID, taskType)))

            StudyParameters = task.data["StudyParameters"]
            studyDir = StudyParameters['Files']['studyDirectory']
            templatesDir = StudyParameters['Files']['templatesDirectory']

            confID = task.data['confID']

            try:
                if taskType == "run_cell":
                    prepare_cell(task, verbose=args.verbose)

                    cellID = task.data['cellID']
                    runDir = os.path.join(studyDir, confID, 'postProcessing', cellID)

                    postprocess.prepareMutables_cell(runDir, verbose=args.verbose)
                    postprocess.compute_cell(runDir, verbose=args.verbose)

                    cs.touchFoam(runDir)
                
                elif taskType == "run_reference":
                    runDir = os.path.join(studyDir, confID, 'reference')
                    postprocess.compute_reference(runDir, verbose=args.verbose)

                elif taskType == "run_average":
                    runDir = os.path.join(studyDir, confID, 'average')
                    postprocess.compute_average(runDir, verbose=args.verbose)
                    postprocess.compute_average_curvature(runDir, StudyParameters, verbose=args.verbose)
                
                elif taskType == "process_lines":
                    process_lines(task, verbose=args.verbose)

                else:
                    printer.put(PrintBufferElement("Unrecognized task type, throwing task away"))

                taskManager.declare_completed(task)

            except:
                printer.put(PrintBufferElement("Error while executing task, putting it back in the queue"))
                taskManager.submit(task)
                time.sleep(1)
                raise

    # count workers still running
    activeThreadsList = threading.enumerate()
    nActive = 0
    for t in activeThreadsList: 
        if "runner" in t.name: nActive += 1
    printer.put(PrintBufferElement("{0} Work finished ({1} worker thread(s) still running)".format(
        datetime.datetime.now(), nActive-1)))

def process_lines(task, verbose = 0):
    pass

def prepare_cell(task, verbose = 0):
    """Cell preparation subroutine."""

    cellID = task.data["cellID"]
    confID = task.data['confID']
    StudyParameters = task.data["StudyParameters"]

    studyDir = StudyParameters['Files']['studyDirectory']
    confDir  = os.path.join(studyDir, confID)
    cellDir  = os.path.join(confDir, 'cell', cellID)
    runDir   = os.path.join(confDir, 'postProcessing', cellID)
    templateDir = os.path.join(
        StudyParameters['Files']['templatesDirectory'], 
        'postProcessing/cell'
        )

    # Copying results from reference, average or cell cases is unnecessary: the
    # mapFields utility can be used with the -sourceTime option to target the
    # desired time directory
    
    # Copy template files
    fs.copydir(templateDir, runDir)

    # Copy case files inherited from the cell case
    # This includes mesh points and faces, so that the target and source
    # meshes are strictly identical
    # Mesh processing could be done using the changeDictionary tool

    ### Generate list of files
    copyList = [
        [ 'constant/sourceTermDict'      , 'constant/sourceTermDict' ],
        [ 'constant/transportProperties' , 'constant/transportProperties' ],
        [ 'constant/polyMesh/faces'      , 'constant/polyMesh/faces' ],
        [ 'constant/polyMesh/neighbour'  , 'constant/polyMesh/neighbour' ],
        [ 'constant/polyMesh/owner'      , 'constant/polyMesh/owner' ],
        [ 'constant/polyMesh/points'     , 'constant/polyMesh/points' ]
        ]
    ### Copy files
    fs.copyfilelist(cellDir, runDir, copyList)

    # Prepare CaseParameters.ini
    CaseParameters = configobj.ConfigObj()
    CaseParameters.filename = os.path.join(runDir, 'CaseParameters.ini')

    CaseParameters['Lengths'] = StudyParameters['Lengths']
    CaseParameters['Cell'] = { 
        'cellID': cellID, 
        'offset' : task.data['StudyParameters']['Cell']['Grid'].as_list(cellID) 
        }

    CaseParameters.write()

def main():
    # Handle keyboard interruption
    try:
        # Process arguments
        parser = argparse.ArgumentParser()
        parser.add_argument('studyfile', action="store", help="path to study file")
        parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
        args = parser.parse_args()

        # Spawn printer
        printer = Printer()
        
        # Process configuration file
        configFilePath = args.studyfile
        StudyParameters = configobj.ConfigObj(configFilePath)

        # Spawn task manager
        taskManager = TaskManager()

        # Cycle on configuration IDs
        for confID in StudyParameters['Parametric'].keys():
            # StudyParameters is included in task data to reduce the amount 
            # of HD access
            
            # Submit run_reference task (no prerequisite)
            referenceTaskID = '{0}_run_reference'.format(confID)
            taskData = {
                'type' : 'run_reference',
                'confID' : confID,
                'StudyParameters' : StudyParameters,
                }
            taskManager.submit(Task(referenceTaskID, taskData, ''))

            # Submit run_average task (no prerequisite)
            averageTaskID = '{0}_run_average'.format(confID)
            taskData = {
                'type' : 'run_average',
                'confID' : confID,
                'StudyParameters' : StudyParameters,
                }
            taskManager.submit(Task(averageTaskID, taskData, ''))

            # Submit run_cell tasks (prerequisite: run_reference, run_average)
            cellTaskIDList = []
            for cellID in StudyParameters['Cell']['Grid'].keys():
                taskID = '{0}_run_cell_{1}'.format(confID, cellID)
                cellTaskIDList.append(taskID)
                taskData = {
                    'type' : 'run_cell',
                    'confID' : confID,
                    'cellID' : cellID,
                    'StudyParameters' : StudyParameters,
                    }
                taskManager.submit(Task(
                    taskID, 
                    taskData, 
                    referenceTaskID + ' and ' + averageTaskID
                    ))

            # Submit process_lines task (prerequisites: all)
            taskID = '{0}_process_lines'.format(confID)
            taskData = {
                'type' : 'process_lines',
                'confID' : confID,
                'StudyParameters' : StudyParameters,
                }
            prereq = ' and '.join([ referenceTaskID ] + cellTaskIDList)
            taskManager.submit(Task(taskID, taskData, prereq))
                
        # Spawn runner threads
        param = {
            'args' : args,
            'taskManager' : taskManager,
            'printer' : printer,
            }
        
        nThread = int(StudyParameters['PostProcessing']['Parallel']['nThread'])
        threadPool = []
        for i in range(nThread):
            t = threading.Thread(
                target=runner, 
                args=(param,), 
                name="runner{0}".format(i)
                )
            t.daemon = True
            threadPool.append(t)

        # Start threads
        printer.start()
        for t in threadPool: 
            t.start()

        # Main thread only waits until runners have finished their jobs
        while threading.active_count() > 2:
            time.sleep(1)

    # This handles keyboard interruption
    except KeyboardInterrupt:
        print '\n! Received keyboard interrupt, quitting threads.\n'

if __name__ == '__main__':
    main()