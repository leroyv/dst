import numpy as np
from math import pi
from interval import Interval, IntervalSet
import configobj, argparse, os, re

# A little snippet so that I don't forget how to search for a
# number with a regex
# re.search('Re\d*\.*\d*[eE]*\-*\d*', 'Re1.0E-2L0q2').group(0) 

# Configure matplotlib
import matplotlib as mp
### Force matplotlib to not use any Xwindows backend.
mp.use('agg')
import matplotlib.pyplot as plt

def confData(confID):
    """Returns a dictionary containing the data associated with the confID
    passed as an argument. Data output format:
    { 
        "fluxPolynom" : polynom array (see the numpy.polyval documentation 
                        for more information)
        "fluxInterval": Interval (see the Interval documentation for more
                        information)
    }"""

    points = np.array([
        [0,0],
        [1500,50],
        [3000,0]
        ])

    q = np.polyfit(points[:,0], points[:,1], 2)

    locationDict = {
        'L0' : Interval(0,360),
        'L1' : IntervalSet([Interval(0,45), Interval(135,225), Interval(315,360)]),
        'L2' : IntervalSet([Interval(45,135), Interval(225,315)])
        }

    locID = re.search('L\d*', confID).group(0)

    return { "fluxPolynom" : q, "fluxInterval" : locationDict[locID] }

def compute_flux(confID, StudyParameters):
    """Computes the reconstructed cylinder wall flux in the cells in the
    given configuration. The flux is given as a ."""
    print(confID)
    
    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)

    cellIDList = sorted(StudyParameters['Cell']['Grid'].keys())
    r0 = float(StudyParameters['Lengths']['r0'])
    rc = r0 * float(StudyParameters['Lengths']['rcFactor'])

    q = confData(confID)['fluxPolynom']
    locInterval = confData(confID)['fluxInterval']

    csvData = np.zeros((len(cellIDList), 3))
    j = 0

    for cellID in cellIDList:
        print("\t{0}".format(cellID))
        # Load data
        fileName = os.path.join(confDir, 'postProcessing/output', 'T_' + cellID + '.csv')
        data = np.genfromtxt(fileName, delimiter=',', dtype=float, usecols=(0,1,2))

        # Compute source around the cylinder
        ### Preliminary calculation
        sourceRef = np.polyval(q, data[:,1])
        sourceStar = np.polyval(q, data[:,2])

        ### Remove points on which the source term is supposed to be 0
        for i in range(len(data[:,0])):
            if data[i,0] not in locInterval:
                sourceRef[i]  = 0
                sourceStar[i] = 0

        ### Recompute abscissa so that it is the curvilinear abscissa on the
        ### circle arc
        abscissa = data[:,0] * 2*pi/360 * rc

        ### Integrate the resulting source (don't forget to multiply by the 
        ### cylinder height to match the results yielded by the wallHeatFlux
        ### utility)
        sourceRef = np.trapz(sourceRef, x=abscissa)*r0
        sourceStar = np.trapz(sourceStar, x=abscissa)*r0
        deviation = abs((sourceStar-sourceRef)/sourceRef)
        print("\t\tsourceRef  = {0}".format(sourceRef))
        print("\t\tsourceStar = {0}".format(sourceStar))
        print("\t\tDeviation  = {0} %".format(deviation*100.))

        ### Store data in the np.array used for file output
        csvData[j,0] = sourceRef
        csvData[j,1] = sourceStar
        csvData[j,2] = deviation
        ### Increment array line counter
        j += 1

    # Output results to CSV
    ### Stack numerical values and cellIDs into a string array
    csvData = np.hstack((
        np.array(cellIDList, dtype="string_", ndmin=2).T,
        csvData
        ))

    ### Output
    header = "cellID,sourceRef,sourceStar,deviation"
    np.savetxt(
        os.path.join(confDir, "postProcessing/output/heatSource.csv"),
        csvData,
        fmt="%s",
        header=header,
        delimiter=","
        )

if __name__ == '__main__':

    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('studyfile', action="store", help="use a non default study file")
    parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
    args = parser.parse_args()

    StudyParameters = configobj.ConfigObj(args.studyfile)

    q2 = confData('L0q2')['fluxPolynom']

    x = np.linspace(0,3000,50)

    plt.plot(x,np.polyval(q2, x))
    plt.xlabel('$T$')
    plt.ylabel('$q_2$')
    plt.gcf().set_size_inches(4,3)
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.savefig('q2.png',dpi=150)
    plt.close()

    for confID in StudyParameters['Parametric'].keys():
        compute_flux(confID, StudyParameters)
