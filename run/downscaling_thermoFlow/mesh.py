import argparse
import numpy as np
from math import pi, sin, cos
from pprint import *

def rotateVector(v, theta):
    """Rotates vector v by an angle theta in the plane (xOy)."""
    v0 = v[0]
    v1 = v[1]
    v[0] = v0 * cos(theta) - v1 * sin(theta)
    v[1] = v0 * sin(theta) + v1 * cos(theta)
    
class GmshGeo:
    """A class which creates a Gmsh geometry file. Notice that we
    are very careful and try to number entities counter clockwise."""

    # Basic functions
    def __init__(self):
        self.reset()

    def reset(self):
        self.idOffset = 0
        self.commands = []

    def output(self, filename):
        with open(filename,'w') as f:
            for line in self.commands:
                f.write(line)

    # Elementary entities (i.e. correspond to a single Gmsh command)
    def point(self, c):
        """Creates a point at coordinates c."""

        pointID = self.idOffset
        self.idOffset += 1

        self.commands.append("Point({0}) = {{ {1}, {2}, {3}, 1.0 }};\n".format(
            pointID, c[0], c[1], c[2]))

        return pointID

    def lineStraight(self, startID, endID):
        """Creates a straight line using the IDs passed as arguments.
        Returns the ID of the created straight line."""

        lineID = self.idOffset
        self.idOffset += 1

        self.commands.append(
            "Line({0}) = {{ {1}, {2} }};\n".format(
                lineID, startID, endID))

        return lineID

    def lineCircle(self, startID, centerID, endID):
        """Creates a circle line using the IDs passed as arguments.
        Returns the ID of the created circle line."""

        circleID = self.idOffset
        self.idOffset += 1

        self.commands.append("Circle({0}) = {{ {1}, {2}, {3} }};\n".format(
                circleID, startID, centerID, endID))

        return circleID

    def lineLoop(self, idList):
        """Creates a line loop with the given line IDs."""

        lineLoopID = self.idOffset
        self.idOffset += 1

        self.commands.append(
            "Line Loop({0}) = {{ {1} }};\n".format(
                lineLoopID, ",".join(map(str,idList))
                ))

        return lineLoopID

    def surfacePlane(self, idList):
        """Creates a plane surface using the boundary line loop
        IDs passed as an argument."""

        planeSurfaceID = self.idOffset
        self.idOffset += 1

        self.commands.append(
            "Plane Surface({0}) = {{ {1} }};\n".format(
                planeSurfaceID, ",".join(map(str,idList))
                ))

        return planeSurfaceID

    # Compound entities
    def parallelogram(self, a, b, c):
        """Creates a parallelogram generated from lines [ab] and [bc].
        Points must not priorily exist. Returns the IDs of the created
        line and line loop entities.
        Line index in list (given that d = a + (b-c)):
            ab -> 0
            bc -> 1
            cd -> 2
            da -> 3"""

        vertexIDList = []
        vertexIDList.append(self.point(a))
        vertexIDList.append(self.point(b))
        vertexIDList.append(self.point(c))
        vertexIDList.append(self.point(a + (c-b)))

        lineIDList = []
        for i in vertexIDList:
            lineIDList.append(
                self.lineStraight(
                    vertexIDList[i%4], 
                    vertexIDList[(i+1)%4]
                    ))

        lineLoopID = self.lineLoop(lineIDList)

        return { "lineIDList" : lineIDList, "lineLoopID" : lineLoopID }

    def circle(self, c, r, n = 4, theta0 = 0):
        """Creates a circle of center c or radius r, split into 
        n lines. The angle offset theta0 determines the position 
        of the first point. Miminum split number is 3. The function
        returns the IDs of the created circle line entities. Points 
        must not priorily exist."""

        if n < 3:
            raise ValueError("Can't subdivide a circle into less than 3 parts")

        # Define base vector
        e = r * np.array([1,0,0])
        
        # Apply offset
        rotateVector(e, theta0)

        # Build point set
        dtheta = 2*pi / n
        circlePointIDList = []
        centerPointID = self.point(c)

        for i in range(n):
            circlePointIDList.append(self.point(c + e))
            rotateVector(e, dtheta)

        lineIDList = []

        # Build circle lines
        for i in range(n):
            lineIDList.append(self.lineCircle(circlePointIDList[i%n], centerPointID, circlePointIDList[(i+1)%n]))

        lineLoopID = self.lineLoop(lineIDList)

        return { "lineIDList" : lineIDList, "lineLoopID" : lineLoopID }

    def openFoamExtrude(self, v, surfaceID):
        """Creates an extrude operation suited for an OpenFOAM 2D
        simulation. The extrusion vector v and the base surface ID 
        are passed as arguments."""

        self.commands.append(
            "surfaceVector[] = Extrude {{ {0}, {1}, {2} }} {{ Surface{{ {3} }}; Layers{{ 1 }}; Recombine; }};\n".format(
                v[0], v[1], v[2], surfaceID))

    def openFoamPhysical(self, backSurfaceID, nCylinders, nCircleDivisions = 4):
        """This is the funny part: where we build the physical 
        topology for OpenFOAM to be provided with boundary and 
        volume names. The very uncool thing is that entity IDing
        is done automatically and silently by Gmsh. The cool thing, 
        however, is that numbering is predictable (at least for the 
        case we are dealing with).

        The surfaceVector array created by the openFoamExtrude
        method contains the following indices:
        [0] the "front" boundary (the translated base surface)
        [1] the resulting volume
        [2] the "left" boundary
        [3] the "bottom" boundary
        [4] the "right" boundary
        [5] the "top" boundary
        [6 to 6+nCylinders*nCircleDivisions-1]
            the cylinder surfaces

        The cylinder boundaries are numbered clockwise thanks to
        the way the circle() method is implemented."""

        self.commands.append("Physical Volume(\"internal\") = surfaceVector[1];\n")
        self.commands.append("Physical Surface(\"front\") = surfaceVector[0];\n")
        self.commands.append("Physical Surface(\"back\") = {};\n".format(backSurfaceID))
        self.commands.append("Physical Surface(\"inlet\") = surfaceVector[2];\n")
        self.commands.append("Physical Surface(\"lowerWall\") = surfaceVector[3];\n")
        self.commands.append("Physical Surface(\"outlet\") = surfaceVector[4];\n")
        self.commands.append("Physical Surface(\"upperWall\") = surfaceVector[5];\n")

        cylinderWalls1String = []
        cylinderWalls2String = []
        offset = 6
        k = 0

        for i in range(nCylinders):
            for j in range(nCircleDivisions):
                tmpStr = "surfaceVector[{}]".format(offset+k)
                if k%2 == 0:
                    cylinderWalls1String.append(tmpStr)
                else:
                    cylinderWalls2String.append(tmpStr)
                k += 1

        self.commands.append("Physical Surface(\"cylinderWalls1\") = {{ {} }};\n".format(",".join(cylinderWalls1String)))
        self.commands.append("Physical Surface(\"cylinderWalls2\") = {{ {} }};\n".format(",".join(cylinderWalls2String)))

def main():

    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--outputfile', action="store", help="output file name (can be a path)")
    parser.add_argument('-c', '--configfile', action="store", help="path to a config file (INI syntax, if not used, default values are used); the following values must be specified: r0, rcFactor, M, N, nCircleDivisions, theta0")
    parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
    args = parser.parse_args()

    # Default parameter values
    r0 = 0.1
    rc = 0.444*r0
    M = 20
    N = 1
    nCircleDivisions = 4
    theta0 = pi/4
    outputfile = "mesh.geo"

    # If config file is specified, overwrite parameter values
    if args.configfile:
        MeshParameters = configobj.ConfigObj(args.configfile)
        r0 = float(MeshParameters['r0'])
        rc = float(MeshParameters['rcFactor'])*r0
        M = int(MeshParameters['M'])
        N = int(MeshParameters['N'])
        nCircleDivisions = int(MeshParameters['nCircleDivisions'])
        theta0 = int(MeshParameters['theta0'])
    
    geo = GmshGeo()
    
    paraID = geo.parallelogram(
        np.array([-r0/2, -r0/2+N*r0, -r0/2]),
        np.array([-r0/2, -r0/2, -r0/2]),
        np.array([-r0/2+M*r0, -r0/2, -r0/2])
        )
    
    circIDList = []
    for i in range(M):
        for j in range(N):
            c = np.array([r0*i, r0*j, -r0/2])
            circIDList.append(geo.circle(c, rc, nCircleDivisions, theta0))

    lineLoopIDList = [ paraID["lineLoopID"] ]
    for circID in circIDList:
        lineLoopIDList.append(circID["lineLoopID"])
    
    surfID = geo.surfacePlane(lineLoopIDList)

    geo.openFoamExtrude(
        np.array([0.0, 0.0, 0.1]),
        surfID
        )

    geo.openFoamPhysical(surfID, M*N, nCircleDivisions)

    if args.verbose:
        pprint(geo.commands)

    # If output file is specified, overwrite file name
    if args.outputfile:
        outputfile = args.outputfile

    # Output the resulting geometry to a 
    geo.output(outputfile)

if __name__ == '__main__':
    main()