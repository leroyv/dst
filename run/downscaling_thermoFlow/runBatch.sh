function run_batch {
	python runReference.py $1
	python runAverage.py $1
	python runCell.py $1
	python runPostprocess.py $1
	python runPlot.py $1
}
