#!/usr/bin/python

import os, argparse, Gnuplot
import numpy as np
import configobj

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('runDirectory', help="directory in which the downscaling cases are run")
    parser.add_argument('-r', '--reference', action="store_true", help="include plot for reference problem")
    parser.add_argument('-c', '--cell', action="store_true", help="include plots for cell problems")
    args = parser.parse_args()

    # Define file list
    StudyParameters = configobj.ConfigObj('templates/StudyParameters.ini')
    runPath = args.runDirectory

    caseFields = configobj.ConfigObj()
    
    if args.reference:
        caseFields['reference'] = { 'path' : os.path.join(runPath, 'reference'),
                                    'appname' : 'thermoPrimitiveFoam',
                                    'fields' : ['T'] }
    if args.cell:
        for cellID in StudyParameters['Cell']['Grid'].keys():
            caseFields[cellID] = { 'path' : os.path.join(runPath, 'cell', cellID),
                                   'appname' : 'thermoDownscalingFoam',
                                   'fields' : ['TTilde'] }

    if not(args.reference) and not(args.cell):
        print('No plot to print')

    # Plot data
    g = Gnuplot.Gnuplot()

    for caseName in caseFields.keys():
        data = np.empty([0,2])
        tempArray = np.empty([1,2])
        i = 0

        logFileName = os.path.join(caseFields[caseName]['path'],
            'PyFoamSolve.' + caseFields[caseName]['appname'] + '.logfile')

        # Gather data (to be improved to support multiple fields)
        try: 
            with open(logFileName) as logFile:
                for line in logFile:
                    if "Solving for " + caseFields[caseName]['fields'][0] in line:
                        tempArray[0,0] = i
                        tempArray[0,1] = float(line.split()[7].strip(','))
                        data = np.concatenate((data, tempArray))
                        i += 1

            outputFileName = 'monitor/' + caseName + '.png'
            g.reset()
            g('set terminal png size 1600,1200 enhanced font "Helvetica" 12')
            g('set output "{}"'.format(outputFileName))
            g('set logscale y')
            g('set style data lines')
            g.title("Residual plot -- {}".format(caseFields[caseName]['appname']))
            g.xlabel('Iteration')
            g.ylabel('Residual')
            g.plot(data)
            print("Plotted residuals to file {}".format(outputFileName))
        except: print("Could not file {}".format(logFileName))

if __name__ == '__main__':
    main()
