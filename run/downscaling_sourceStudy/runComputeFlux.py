from downscalingtools.tools.filesystem import *
import configobj, argparse, os
import numpy as np
from scipy.optimize import curve_fit
import sympy as sy
from math import pi
from interval import Interval, IntervalSet
from sympy.parsing.sympy_parser import parse_expr
from tabulate import tabulate

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
mpl.use('agg')
mpl.rcParams['mathtext.fontset'] = 'stixsans'

def confData(StudyParameters, confID):
    """Returns a dictionary containing the data associated with the confID
    passed as an argument. Data output format:
    { 
        "fluxPolynom" : polynom array (see the numpy.polyval documentation 
                        for more information)
        "fluxInterval": Interval (see the Interval documentation for more
                        information)
    }"""

    # Define the L1 and L2 zones
    locationDict = {
        'L0' : Interval(0,360),
        'L1' : IntervalSet([Interval(0,45), Interval(135,225), Interval(315,360)]),
        'L2' : IntervalSet([Interval(45,135), Interval(225,315)])
        }

    # Load fluxes for the L1 and L2 zones from study file
    T = sy.symbols('T')
    S1Exp = parse_expr(
        StudyParameters['Thermo'][confID]['BoundaryConditions']['S1'].split("?")[-1].split(":")[0]
        )
    S2Exp = parse_expr(
        StudyParameters['Thermo'][confID]['BoundaryConditions']['S2'].split("?")[-1].split(":")[0]
        )

    # Use flux expressions to determine the configuration
    locID = ''
    if (S1Exp == 0 and S2Exp != 0):
        locID = 'L2'
    elif (S1Exp != 0 and S2Exp == 0):
        locID = 'L1'
    else:
        locID = 'L0'

    return { 
        "S1Exp" : S1Exp, 
        "S2Exp" : S2Exp,
        "locID" : locID,
        "locationDict" : locationDict
        }

def compute_flux(StudyParameters, confID, method="trapz", verbose=False):
    """Computes the reconstructed cylinder wall flux in the cells in the
    given configuration. This function only works as intended is S1 = S2 or
    if one of the two is null.

    Args: 
        StudyParameters (configobj.ConfigObj): ConfigObj object containing 
            the study file parameters.
        confID (str): Configuration identifier (to be extracted from the 
            appropriate StudyParameters file section).
        method (str, optional): Integration method to be used, to be chosen
            from: ["rect" [default], "trapz"].
        verbose (str, optional): verbosity switch.

    Returns:
        tuple: Contains the following data:
            TBar (double): Average temperature in the cell (read from
                StudyParameters). 
            gradTBarX (double): Average temperature gradient (x component) in 
                the cell (read from StudyParameters).
            gradTBarY (double): Average temperature gradient (y component) in 
                the cell (read from StudyParameters).
            source (double): Macroscopic heat source (crude model result, S(TBar)).
                Only accurate if S1 = S2 or if one of the two is zero. Otherwise, 
                yields S2(TBar).
            sourceStar (double): Average heat flux computed from the reconstructed
                cylinder wall temperature.
            Ar (double): Specific reactive area.
    """
    
    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, 'postProcessing', confID)

    # Load geometrical data
    r0 = float(StudyParameters['Lengths']['r0'])
    rc = r0 * float(StudyParameters['Lengths']['rcFactor'])

    # Load macroscopic source term data
    TBar = float(StudyParameters['Thermo'][confID]['MacroSourceTerms']['TBar'])
    gradTBarX = float(StudyParameters['Thermo'][confID]['MacroSourceTerms']['gradTBarX'])
    gradTBarY = float(StudyParameters['Thermo'][confID]['MacroSourceTerms']['gradTBarY'])

    # Load heat source data
    tempDict = confData(StudyParameters, confID)
    S1Exp = tempDict['S1Exp']
    S2Exp = tempDict['S2Exp']
    T = sy.symbols('T')
    L1Interval = tempDict['locationDict']['L1']
    L2Interval = tempDict['locationDict']['L2']

    # Evaluate crude source term S(TBar)
    S1TBar = S1Exp.subs(T, TBar).evalf()
    S2TBar = S2Exp.subs(T, TBar).evalf()
    source = S1TBar if (S1TBar != 0.) else S2TBar

    # Compute specific reactive area
    area = 2*pi*rc*r0 if tempDict['locID'] == 'L0' else pi*rc*r0
    volume = r0**3
    Ar = area/volume

    # Load data
    fileName = os.path.join(studyDir, 'postProcessing/output', 'TStar_' + confID + '.csv')
    data = np.genfromtxt(fileName, delimiter=',', dtype=float, usecols=(0,1), skip_header=1)
    flux = np.zeros((data.shape[0]))

    # Compute flux around the cylinder
    for i in range(len(data[:,0])):
        if data[i,0] in L1Interval:
            flux[i] = S1Exp.subs(T, data[i,1]).evalf()
        elif data[i,0] in L2Interval:
            flux[i] = S2Exp.subs(T, data[i,1]).evalf()

    # Recompute abscissa so that it is the curvilinear abscissa on the
    # circle arc
    abscissa = data[:,0] * 2*pi/360 * rc
    sourceStar = 0

    if (method == "trapz"):
        # Integrate the resulting source (don't forget to multiply by the 
        # cylinder height to match the results yielded by the wallHeatFlux
        # utility)
        sourceStar = np.trapz(flux, x=abscissa)*r0

    elif (method == "rect"):
        # Integrate the source using rectangular interpolation
        N = len(abscissa)
        h = np.sum(abscissa[1:]-abscissa[:N-1])/N
        sourceStar = np.sum(flux)*h*r0
    else:
        raise ValueError('unrecognized integration method {0}'.format(method))

    # Divide by the reactive area
    sourceStar /= area
    
    if verbose:
        print(confID)
        print("\tAr         = {0}".format(Ar))
        print("\tsourceStar = {0}".format(sourceStar))

    return (TBar, gradTBarX, gradTBarY, source, sourceStar, Ar)

def quadric(X, a, b, c, d, e, f):
    """Returns z for the values x (2D vector) for the equation
        z = a + b*x + c*y + d*x^2 + e*y^2 + f*x*y

    Args:
        X (np.array): 2D array containing the x and y coordinates.
        a (float): a parameter.
        b (float): b parameter.
        c (float): c parameter.
        d (float): d parameter.
        e (float): e parameter.
        f (float): f parameter.

    Returns:
        z (float): value of z."""

    x, y = X[:,0], X[:,1]
    return a + b*x + c*y + d*x**2 + e*y**2 + f*x*y

if __name__ == '__main__':

    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('studyfile', action="store", help="use a non default study file")
    parser.add_argument('-w', '--write', action="store", help="write table to <studyDir>/postProcessing/output/heatSource_<WRITE>.csv")
    parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
    parser.add_argument('-m', '--method', action="store", default="rect", help="specify integration method (rect [default], trapz)")
    args = parser.parse_args()

    if args.verbose:
        print("Computing flux with {0} method...\n".format(args.method))

    StudyParameters = configobj.ConfigObj(args.studyfile)
    studyDir = os.path.join(StudyParameters["Files"]["studyDirectory"])
    
    data = []

    for confID in StudyParameters['Parametric']['Thermo'].keys():
        TBar, gradTBarX, gradTBarY, source, sourceStar, A = compute_flux(StudyParameters, confID, method=args.method, verbose=args.verbose)
        data.append([confID, TBar, gradTBarX, gradTBarY, source, sourceStar, A])

    headers = ["confID", "TBar [K]", "gradTBarX [K/m]", "gradTBarY [K/m]", "source [W/m^2]", "sourceStar [W/m^2]", "Ar [m^-1]"]

    # Print data to terminal
    print(tabulate(
        data, 
        headers=headers
        ))

    # Write data to hard drive
    if args.write:
        outputDir = os.path.join(studyDir, 'postProcessing/output')
        mkdir_p(outputDir)
        suffix = args.write
        outputfile = os.path.join(outputDir, 'heatSource_' + suffix + '.csv')
        np.savetxt(outputfile, data, fmt="%s, %f, %f, %f, %f, %f, %f", header=','.join(headers))

    # Compute the equation of a quadric fit for the data points
    arraydata = np.asarray(data) # Convert list to array
    xydata = arraydata[:,1:3].astype('float64')
    zdata = arraydata[:,5].astype('float64')
    optimalparams, covmatrix = curve_fit(quadric, xydata, zdata)
    print "optimal parameters: "
    print optimalparams
    print "covariance matrix: "
    print covmatrix

    # Compute relative error
    op = optimalparams
    zfit = quadric(xydata, op[0], op[1], op[2], op[3], op[4], op[5])
    error = np.abs((zfit-zdata)/zdata)
    cerror = np.sum(error)
    print 'cumulated relative error:', cerror
    merror = error.max()
    print 'maximum relative error:', merror

    # Plot results
    if args.write:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        
        ### Plot surface
        n = 51
        xv = np.linspace(xydata[:,0].min(), xydata[:,0].max(),n).reshape((n,1))
        yv = np.linspace(xydata[:,1].min(), xydata[:,1].max(),n).reshape((n,1))
        xi, yi = np.meshgrid(xv, yv)
        zi = np.array([quadric(np.array([x,y]).reshape(1,2), op[0], op[1], op[2], op[3], op[4], op[5])
                       for x,y in zip(np.ravel(xi), np.ravel(yi))
                       ]).reshape(xi.shape)

        ax.plot_surface(xi, yi, zi, cmap='coolwarm')

        ### Plot points
        ax.scatter(xydata[:,0], xydata[:,1], zdata, color='w', edgecolors='k')

        ### Set plot details
        ax.set_xlabel('T')
        ax.set_ylabel('gradT')
        ax.set_zlabel('S*')
        
        ### Save plot
        outputDir = os.path.join(studyDir, 'postProcessing/output')
        mkdir_p(outputDir)
        for viewangle in ['elegant', 'vsT', 'vsgradT']:
            suffix = viewangle if (args.write == None) else '{0}_{1}'.format(args.write,viewangle)
            if viewangle == 'elegant':
                ax.view_init(elev=45,azim=-135) # Elegant
            elif viewangle == 'vsT':
                ax.view_init(elev=0,azim=-90) # Best to see variations vs T
            elif viewangle == 'vsgradT':
                ax.view_init(elev=0,azim=0) # Best to see variations vs gradT
        
            outputfile = os.path.join(outputDir, 'heatSource_{0}.png'.format(suffix))
            plt.savefig(outputfile,dpi=150)

        plt.close()
