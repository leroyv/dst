#!/bin/bash

# Job pool library import
source job_pool.sh

# Packing functions
function pack_cell {
    cd $1
    pyFoamPackCase.py --last --pyfoam \
        --add=\*.foam --add=postProcessing --add=output \
        $2
}

function pack_postprocessing {
    cd $1
    tar -zcf postprocessing.tgz postProcessing/
}

# Default vairable values
PATH_ROOT=`pwd`
PATH_RUN=$PATH_ROOT/run
PATH_ARCH=$PATH_ROOT/archive
OPT_P=0
OPT_A=0
OPT_H=0
OPT_N=2

# Option processing
while getopts ":pahR:A:N:" opt; do

    case $opt in
        p)
            OPT_P=1
        ;;

        a)
            OPT_A=1
        ;;

        h)
            OPT_H=1
        ;;

        R)
            if [[ $OPTARG == /* ]]; then
                PATH_RUN=$OPTARG
            else
                PATH_RUN=$PATH_ROOT/$OPTARG
            fi
            # Remove trailing slash
            PATH_RUN=${PATH_RUN%/}
        ;;

        A)
            if [[ $OPTARG == /* ]]; then
                PATH_ARCH=$OPTARG
            else
                PATH_ARCH=$PATH_ROOT/$OPTARG
            fi
        ;;

        N)
            NPROC=$OPTARG
        ;;

        \?)
            echo "Invalid option -$OPTARG" >&2
            exit 1
        ;;

    esac

done

# Display help message
if [[ $OPT_H == 1 ]]; then
    echo 'Available options:'
    echo '-h        Action: display help'
    echo '-R <path> Path to run directory (where the results are); defaults to run/'
    echo '-N <nPrc> Maximum number of simultaneous processes allowed (defaults to 2)'
    exit 0
fi

# Pack results

# Initialize job pool with proper number of processes 
# (2nd argument == 0 means no command echo, 1 means command echo)
job_pool_init $NPROC 1

echo "Packing results from $PATH_RUN to $PATH_ARCH ..."

for CONFID in `ls $PATH_RUN`; do

    cd $PATH_RUN/flow

    for CONFID in `ls -d -- */`; do
        job_pool_run \
            pack_cell $PATH_RUN/flow $CONFID
    done

    cd $PATH_RUN/thermo

    for CONFID in `ls -d -- */`; do
        job_pool_run \
            pack_cell $PATH_RUN/thermo $CONFID
    done

    job_pool_run \
        pack_postprocessing $PATH_RUN

done

# Blocking point: we wait for all jobs to finish
job_pool_wait
job_pool_shutdown
echo "job_pool_nerrors: ${job_pool_nerrors}"

# Move files to the archive directory
cd $PATH_ROOT

mkdir -p $PATH_ARCH/flow/
mkdir -p $PATH_ARCH/thermo/
    
mv $PATH_RUN/flow/*.tgz $PATH_ARCH/flow/
mv $PATH_RUN/thermo/*.tgz $PATH_ARCH/thermo/
mv $PATH_RUN/postprocessing.tgz $PATH_ARCH/
