function run_batch {
	python runFlow.py $1
	python runThermo.py $1
	python runPostprocess.py $1
	python runPlot.py $1
}
