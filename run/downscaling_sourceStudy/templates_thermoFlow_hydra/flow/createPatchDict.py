# Script to build system/createPatchDict
import numpy as np
import configobj, argparse, os

def createPatchDict(systemDirectory, CaseParameters):
    
    ### Common data
    r0 = float(CaseParameters['Lengths']['r0']) # Cell size
    M  = int(CaseParameters['GridSize']['M']) # Number of columns (index name = I)
    N  = int(CaseParameters['GridSize']['N']) # Number of rows (index name = J)

    ### Patch pair list
    patchPairList = {   'in'  : { 'patches' : ['inlet','outlet'] ,
                                'separationVector' : np.array([r0*M, 0.0, 0.0]) # The vector from face 0 to face 1
                                },
                        'up'  : { 'patches' : ['lowerWall','upperWall'] ,
                                'separationVector' : np.array([0.0, r0*N, 0.0]) # The vector from face 0 to face 1
                                }
                    }

    ### Output createPatchDict
    with open(os.path.join(systemDirectory, 'createPatchDict'),'w') as createPatchDict:
        # Copy header
        with open(os.path.join(systemDirectory, 'createPatchDict.header'), 'r') as header:
            for line in header:
                createPatchDict.write(line)
        
        createPatchDict.write("patches (\n")

        for patchPairName in sorted(patchPairList.keys()):
            patchPair = patchPairList[patchPairName]
            separationVector = patchPair['separationVector']

            createPatchDict.write(" "*4  + "{\n")
            createPatchDict.write(" "*8  +     "name " + patchPairName + "_half0;\n")
            createPatchDict.write(" "*8  +     "patchInfo {\n")
            createPatchDict.write(" "*12 +         "type cyclic;\n")
            createPatchDict.write(" "*12 +         "neighbourPatch " + patchPairName + "_half1;\n")
            createPatchDict.write(" "*12 +         "transform translational;\n")
            createPatchDict.write(" "*12 +         "separationVector ({sepX} {sepY} {sepZ});\n".format(sepX=separationVector[0],
                                                                                                       sepY=separationVector[1],
                                                                                                       sepZ=separationVector[2]))
            createPatchDict.write(" "*8  +     "}\n")
            createPatchDict.write(" "*8  +     "constructFrom patches;\n")
            createPatchDict.write(" "*8  +     "patches (" + patchPair["patches"][0] + ");\n")
            createPatchDict.write(" "*4  + "}\n\n")

            createPatchDict.write(" "*4  + "{\n")
            createPatchDict.write(" "*8  +     "name " + patchPairName + "_half1;\n")
            createPatchDict.write(" "*8  +     "patchInfo {\n")
            createPatchDict.write(" "*12 +         "type cyclic;\n")
            createPatchDict.write(" "*12 +         "neighbourPatch " + patchPairName + "_half0;\n")
            createPatchDict.write(" "*12 +         "transform translational;\n")
            createPatchDict.write(" "*12 +         "separationVector ({sepX} {sepY} {sepZ});\n".format(sepX=-separationVector[0],
                                                                                                       sepY=-separationVector[1],
                                                                                                       sepZ=-separationVector[2]))
            createPatchDict.write(" "*8  +     "}\n")
            createPatchDict.write(" "*8  +     "constructFrom patches;\n")
            createPatchDict.write(" "*8  +     "patches (" + patchPair["patches"][1] + ");\n")
            createPatchDict.write(" "*4  + "}\n\n")

        createPatchDict.write(");\n")

def main():
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sysdir', action="store", help="path to the system directory where the createPatchDict.header file is located; defaults to system/")
    parser.add_argument('-c', '--casepar', action="store", help="path to the CaseParameters.ini file; defaults to .")
    args = parser.parse_args()

    systemDirectory = 'system/'
    if args.sysdir: systemDirectory = args.sysdir

    pathToCaseParameters = 'CaseParameters.ini'
    if args.casepar:
            pathToCaseParameters = args.casepar

    # Read CaseParameters
    CaseParameters = configobj.ConfigObj(pathToCaseParameters)
    
    # Create the dict file
    createPatchDict(systemDirectory, CaseParameters)

if __name__ == "__main__":
    main()
