# Runs the thermo cell case

import os, argparse, configobj, glob, shutil, itertools, Queue, threading, time, datetime
import downscalingtools.sourcestudy.thermo as thermo
import downscalingtools.thermo.configuration as configuration
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs
from downscalingtools.tools.thread import *

def runner(param):
    """
    Worker thread function.
    """

    args = param['args']
    StudyParameters = param['StudyParameters']
    taskQueue = param['taskQueue']
    printer = param['printer']

    # Process arguments
    steps = {
            'prep' : True,
            'map' : True,
            'run' : True
        }
    if args.onlyprep:
        steps['map'], steps['run'] = False, False
    elif args.onlymap:
        steps['prep'], steps['run'] = False, False
    elif args.onlyrun:
        steps['prep'], steps['map'] = False, False

    while not(taskQueue.empty()):

        # Get task parameters
        task = taskQueue.get()
        taskQueue.task_done()
        taskID = task['ID']
        confID = task['confID']
        
        # Gather directories
        studyDir = StudyParameters['Files']['studyDirectory']
        templateDir = os.path.join(StudyParameters['Files']['templatesDirectory'], 'thermo')
        runDir = os.path.join(studyDir, 'thermo', confID)
        flowConfID = StudyParameters['Thermo'][confID]['Flow']['confID']
        flowDir = os.path.join(studyDir, 'flow', flowConfID)
        
        message = "{0} CELL CASE ({1} jobs left) {2}".format(
            datetime.datetime.now(), taskQueue.qsize(), runDir)
        printer.put(PrintBufferElement(message))

        if steps['prep']:
            # Compute diffusivity and so on
            AdditionalParameters = configuration.process(StudyParameters)

            # Gather multiprocessing parameters
            nProc = int(StudyParameters['Thermo']['Parallel']['nProc'])
            domDecomposition = map(int, StudyParameters['Thermo']['Parallel'].as_list('decomposition'))

            # Copy template files
            fs.copydir(templateDir, runDir)

            # Prepare CaseParameters.ini
            CaseParameters = configobj.ConfigObj()
            
            CaseParameters['Lengths']  = StudyParameters['Lengths'].dict()
            CaseParameters['GridSize'] = {'M' : 1, 'N' : 1}
            CaseParameters['Directories'] = { 'flowDir' : flowDir }

            ### Read BCs
            bcDict = {}
            for bcName in StudyParameters['Thermo'][confID]['BoundaryConditions'].keys():
                bcDict[bcName] = StudyParameters['Thermo'][confID]['BoundaryConditions'][bcName]

            temperatureDict = bcDict
            temperatureDict['k'] = AdditionalParameters['k']

            timeDict = StudyParameters['Thermo'][confID]['TimeMarching']

            ### Build the actual CaseParameters.ini
            CaseParameters['Dictionaries'] = {
                '0/templates/TTildeStar' : temperatureDict,
                'constant/transportProperties' : { 
                    'alpha' : AdditionalParameters['alpha'],
                    'k'     : AdditionalParameters['k'] 
                    },
                'constant/sourceTermDict' : {
                    'TBar'       : float(StudyParameters['Thermo'][confID]['MacroSourceTerms']['TBar']),
                    'divUBar'    : float(StudyParameters['Flow'][flowConfID]['MacroSourceTerms']['divUBar']),
                    'UBarX'      : float(StudyParameters['Flow'][flowConfID]['MacroSourceTerms']['UBarX']),
                    'UBarY'      : float(StudyParameters['Flow'][flowConfID]['MacroSourceTerms']['UBarY']),
                    'gradTBarX'  : float(StudyParameters['Thermo'][confID]['MacroSourceTerms']['gradTBarX']),
                    'gradTBarY'  : float(StudyParameters['Thermo'][confID]['MacroSourceTerms']['gradTBarY']),
                    'gradUBarXX' : float(StudyParameters['Flow'][flowConfID]['MacroSourceTerms']['gradUBarXX']),
                    'gradUBarXY' : float(StudyParameters['Flow'][flowConfID]['MacroSourceTerms']['gradUBarXY']),
                    'gradUBarYX' : float(StudyParameters['Flow'][flowConfID]['MacroSourceTerms']['gradUBarYX']),
                    'gradUBarYY' : float(StudyParameters['Flow'][flowConfID]['MacroSourceTerms']['gradUBarYY'])
                    },
                'system/decomposeParDict' : { \
                    'numberOfSubdomains' : nProc, 
                    'nX' : domDecomposition[0],
                    'nY' : domDecomposition[1],
                    'nZ' : domDecomposition[2]
                    },
                'system/fvSolution' : { 
                    'epsTTildeStar' : StudyParameters['Thermo'][confID]['Penalization']['epsTTildeStar'] 
                    },
                'system/controlDict' : {
                        'endTime' : timeDict['endTime'],
                        'deltaT' : timeDict['deltaT'],
                        'writeInterval' : timeDict['writeInterval']
                    }
                }

            CaseParameters.filename = os.path.join(runDir, 'CaseParameters.ini')
            CaseParameters.write()

        # Execute step
        startDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} START".format(startDate)))

        if steps['prep']:
            thermo.prepareMutables(runDir, verbose=args.verbose)
        if steps['map']:
            thermo.mapSources(runDir, consistent=True, verbose=args.verbose)
        if steps['run']:
            thermo.decompose(runDir, verbose=args.verbose)
            thermo.compute(runDir, verbose=args.verbose)
            thermo.reconstruct(runDir, verbose=args.verbose)

        cs.touchFoam(runDir)

        endDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} END (exec time = {1})".format(endDate, endDate-startDate)))

    # Count workers still running
    activeThreadsList = threading.enumerate()
    nActive = 0
    for t in activeThreadsList:
        if "runner" in t.name: nActive += 1
    printer.put(PrintBufferElement("{0} Work finished ({1} worker threads still running)".format(
        datetime.datetime.now(), nActive-1)))

def main():
    # Handle keyboard interruption
    try:
        # Process arguments
        parser = argparse.ArgumentParser()
        parser.add_argument('studyfile', action="store", help="path to study file")
        parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
        # The following three are mutually exclusive
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--onlyprep', action="store_true", help="only prepare case")
        group.add_argument('--onlymap', action="store_true", help="only map velocity case")
        group.add_argument('--onlyrun', action="store_true", help="only run OpenFOAM decomposition, solver and reconstruction")
        args = parser.parse_args()

        # Spawn printer
        printer = Printer()

        # Process configuration file
        configFilePath = args.studyfile
        StudyParameters = configobj.ConfigObj(configFilePath)

        # Create and fill task queue (cartesian product of taskID and cellID lists)
        confIDList = StudyParameters['Parametric']['Thermo'].keys()
        taskQueue = Queue.Queue()

        for confID in confIDList:
            task = {}
            task['ID'] = "{0}".format(confID) # Unique ID
            task['confID'] = confID
            taskQueue.put(task)

        # Spawn runner threads
        param = {
            'args' : args,
            'StudyParameters' : StudyParameters,
            'taskQueue' : taskQueue,
            'printer' : printer
            }

        threadPool = []
        nThread = int(StudyParameters['Thermo']['Parallel']['nThread'])
        for i in range(nThread):
            t = threading.Thread(
                target=runner, 
                args=(param,), 
                name="runner{0}".format(i)
                )
            t.daemon = True
            threadPool.append(t)

        # Start threads
        printer.start()
        for t in threadPool: t.start()

        # Main thread only waits until the others have finished their jobs
        while threading.active_count() > 2:
            time.sleep(1)

        # Remove all .obj files
        for f in glob.glob("*.obj"):
            try:
                os.remove(f)
            except:
                pass

    # This handles keyboard interruption
    except KeyboardInterrupt:
    #except (KeyboardInterrupt, SystemExit):
        print '\n! Received keyboard interrupt, quitting threads.\n'
        
if __name__ == '__main__':
    main()