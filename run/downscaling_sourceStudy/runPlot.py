# Runs the plotting tools
# http://stackoverflow.com/questions/9008370/python-2d-contour-plot-from-3-lists-x-y-and-rho
# http://wiki.scipy.org/Cookbook/Histograms
# http://matplotlib.org/examples/images_contours_and_fields/pcolormesh_levels.html

import argparse, configobj, os, scipy.interpolate, math
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs
import numpy as np
from downscalingtools.tools.mathematics import frexp10
from downscalingtools.tools.plot import tickloc, detect_coordinates
from math import pi

# Configure matplotlib
import matplotlib as mp
import matplotlib.pyplot as plt
import matplotlib.ticker
### Force matplotlib to not use any Xwindows backend.
mp.use('agg')
### Change math font to sans serif
mp.rcParams['mathtext.fontset'] = 'stixsans'

def plot_cylinders(confID, StudyParameters, outputExtension):
    studyDir = StudyParameters['Files']['studyDirectory']
    outputDir = os.path.join(studyDir, 'postProcessing/output')

    fs.mkdir_p(outputDir)

    results = {}
    density = int(StudyParameters['PostProcessing']['Plot']['cylinderPlotDensity'])

    # Gather and interpolate data
    # Some of that stuff should be moved to runPostprocess.py
    tempDir = os.path.join(studyDir, 'postProcessing', confID, 'postProcessing/sets')
    fileName = str(os.path.join(tempDir, cs.detectLatestTime(tempDir), \
        'patchSeed_cylinder_TStar.csv') )
    tempArray = np.genfromtxt(fileName, delimiter=',', dtype=float, skip_header=1, usecols=(0,1))

    ### Sort data, interpolate and store
    tempArray = tempArray[tempArray[:,0].argsort()]
    mesh = np.linspace(tempArray[0,0],tempArray[-1,0]+1,density)
    TStar = np.interp(mesh, tempArray[:,0], tempArray[:,1])

    ### Replace mesh with a degree scale
    mesh = np.linspace(0,360,density)
    
    data = np.vstack((mesh, TStar)).T
    
    # Output results to csv file
    np.savetxt(os.path.join(outputDir, 'TStar_' + confID + '.csv'), \
        data, delimiter=',', header='phi,TStar'
        )

    # Draw plot
    plt.figure(1)

    plt.plot(data[:,0], data[:,1], 'g-', label=r'$T^*$')
    
    plt.title(confID)
    plt.xlim([0, 360])
    plt.xticks(np.arange(0,361,45))
    plt.xlabel(r'$\phi$ [$^\circ$]')
    plt.ylabel(r'$T^*$ [K]')
    #plt.legend()

    plt.tight_layout()
    fileName = os.path.join(outputDir, 'TStar_cylinder_' + confID + '.' + outputExtension)
    plt.savefig(fileName,dpi=150)
    plt.close()

def main():
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('studyfile', action="store", help="path to study file")
    parser.add_argument('-n', '--no-plot', action="store", help="only output CSV, do not draw plots")
    parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
    args = parser.parse_args()

    StudyParameters = configobj.ConfigObj(args.studyfile)

    for confID in StudyParameters['Parametric']['Thermo'].keys():
        if args.verbose: print("Processing",confID)
        plot_cylinders(confID, StudyParameters, 'png')

if __name__ == '__main__':
    main()