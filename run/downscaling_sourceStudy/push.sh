#!/bin/bash

# This script propagates the source code to target tomo@hpc2.
# Exclusion rules are in the .push_exclude file.
# Excluded directories and files are protected from deletion.
# Files non existent in the source tree and present in the 
# destination directory are deleted.

rsync -e ssh -avhz \
    --exclude-from=.push_exclude \
    --delete-after \
    . \
    tomo@hpc2:"~/OpenFOAM/tomo-2.2.2/run/downscaling_sourceStudy"
