# Creates a study file

import configobj, argparse
import numpy as np

def main():
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('templatefile', action="store", help="path to template study file")
    parser.add_argument('outputfile',   action="store", help="path to output study file")
    parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
    args = parser.parse_args()

    # Load template study file
    template = configobj.ConfigObj(args.templatefile)

    # Input parameters
    print("Please enter parameters")

    TBarMin = 0
    TBarMax = 0
    while (TBarMax <= TBarMin):
        TBarMin = float(input("TBarMin [K] = "))
        TBarMax = float(input("TBarMax [K] = "))
        if TBarMax <= TBarMin:
            print("TBarMax <= TBarMin, please enter new values")

    nTBar = 0
    while (nTBar <= 0):
        nTBar   = int(input("nTBar = "))
        if nTBar <= 0:
            print("nTBar <= 0, please enter new value")

    gradTBarMin = 0
    gradTBarMax = 0
    while (gradTBarMax <= gradTBarMin):
        gradTBarMin = float(input("gradTBarMin [K] = "))
        gradTBarMax = float(input("gradTBarMax [K] = "))
        if (gradTBarMax <= gradTBarMin):
            print("gradTBarMax <= gradTBarMin, please enter new values")

    nGradTBar = 0
    while (nGradTBar <= 0):
        nGradTBar   = int(input("nGradTBar = "))
        if (nGradTBar <= 0):
            print("nGradTBar <= 0, please enter new value")

    namePrefix = str(input("Section name prefix: "))

    # Check parameters (redundant but useful to implement a config file-based mode)
    if TBarMax <= TBarMin:
        raise ValueError("TBarMax <= TBarMin")
    elif gradTBarMax <= gradTBarMin:
        raise ValueError("gradTBarMax <= gradTBarMin")
    elif nTBar <= 0:
        raise ValueError("nTBar <= 0")
    elif nGradTBar <= 0:
        raise ValueError("nGradTBar <= 0")

    # Create additional sections
    TBarList = np.linspace(TBarMin, TBarMax, nTBar)
    gradTBarList = np.linspace(gradTBarMin, gradTBarMax, nGradTBar)

    if (args.verbose > 0): 
        print "TBar list:", TBarList
        print "gradTBar list:", gradTBarList

    templateSection = template['Files']['Template']
    n = 0

    for TBar in TBarList:
        for gradTBar in gradTBarList:
            sectionName = namePrefix + "P" + "{:05d}".format(n)
            section = templateSection.dict()
            section['MacroSourceTerms']['TBar'] = TBar
            section['MacroSourceTerms']['gradTBarX'] = gradTBar
            template['Thermo'][sectionName] = section
            template['Parametric']['Thermo'][sectionName] = {}
            n += 1

    template.filename = args.outputfile
    template.write()

if __name__ == '__main__':
    main()