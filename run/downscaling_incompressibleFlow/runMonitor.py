#!/usr/bin/python

import os, argparse, subprocess, configobj
import numpy as np
import Gnuplot, Gnuplot.funcutils

# TODO: switch to pyFoamPlotWatcher for data analysis (more reliable and flexible)
# Sample command: pyFoamPlotWatcher.py 
#                   --write-files 
#                   --silent 
#                   --implementation=dummy 
#                   --solver-not-running-anymore 
#                   <path to log file>

def grep(filename, arg):
    process = subprocess.Popen(['grep', '-n', arg, filename], stdout=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return stdout, stderr

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('runDirectory', help="directory in which the downscaling cases are run")
    parser.add_argument('-r', '--reference', action="store_true", help="include plot for reference problem")
    parser.add_argument('-c', '--cell', action="store_true", help="include plots for cell problems")
    parser.add_argument('-s', '--studyfile', action="store", help="use a non default study file")
    args = parser.parse_args()

    # Process configuration file
    configFilePath = 'templates/StudyParameters.ini'
    if args.studyfile:
        configFilePath = args.studyfile
    StudyParameters = configobj.ConfigObj(configFilePath)

    # Define file list
    runPath = args.runDirectory
    caseFields = configobj.ConfigObj()
    
    if args.reference:
        caseFields['reference'] = { 'path' : os.path.join(runPath, 'reference'),
                                    'appname' : 'simplePrimitiveFoam',
                                    'fields' : ['Ux', 'Uy', 'p'] }
    if args.cell:
        for cellID in StudyParameters['Cell']['Grid'].keys():
            caseFields[cellID] = { 'path' : os.path.join(runPath, 'cell', cellID),
                                   'appname' : 'simpleDownscalingFoam',
                                   'fields' : ['UTildeStarx', 'UTildeStary', 'pTildeStar'] }

    if not(args.reference) and not(args.cell):
        print('No plot to print')

    # Plot data
    g = Gnuplot.Gnuplot()

    for caseName in caseFields.keys():
        fieldList = caseFields[caseName]['fields']
        data = np.empty([0,len(fieldList)+1])

        logFileName = os.path.join(caseFields[caseName]['path'],
            'PyFoamSolve.' + caseFields[caseName]['appname'] + '.logfile')

        # Gather data
        try: 
            print("Gathering data")
            with open(logFileName) as logFile:
                # Load file into memory
                lineList = logFile.readlines()

                # First sweep to determine the number of timesteps
                nTimes = 0
                for line in lineList:
                    if 'ExecutionTime' in line:
                        nTimes += 1

                data.resize((nTimes, len(fieldList)+1)) 

                
                # Second sweep to retrieve data
                for j in range(len(fieldList)):
                    i = 0
                    for line in lineList:
                        if "Solving for " + caseFields[caseName]['fields'][j] in line:
                            data[i,0] = i
                            data[i,j+1] = float(line.split()[7].strip(','))
                            i += 1
                        if i >= nTimes: # Skip lines associated with the current (unfinished) timestep
                            break

            outputFileName = 'monitor/' + caseName + '.png'
            g.reset()
            g('set terminal png size 1600,1200 enhanced font "Helvetica" 12')
            g('set output "{}"'.format(outputFileName))
            g('set logscale y')
            g('set style data lines')
            g.title("Residual plot -- {}".format(caseFields[caseName]['appname']))
            g.xlabel('Iteration')
            g.ylabel('Residual')

            dataSetList = []
            for j in range(len(fieldList)):
                x = data[:,0]
                y = data[:,j+1]
                dataSetList.append(Gnuplot.Data(x, y, title=fieldList[j]))

            g._add_to_queue(dataSetList)
            g.replot()
            print("Plotted residuals to file {}".format(outputFileName))

        except: 
            print("Problem while processing file {}".format(logFileName))
            raise

if __name__ == '__main__':
    main()
