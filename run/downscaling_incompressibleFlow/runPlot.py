# Runs the plotting tools
# http://stackoverflow.com/questions/9008370/python-2d-contour-plot-from-3-lists-x-y-and-rho
# http://wiki.scipy.org/Cookbook/Histograms
# http://matplotlib.org/examples/images_contours_and_fields/pcolormesh_levels.html

import argparse, configobj, os, scipy.interpolate, math
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs
import numpy as np
from downscalingtools.tools.mathematics import frexp10
from downscalingtools.tools.plot import tickloc, detect_coordinates
from math import pi

# Configure matplotlib
import matplotlib as mp
import matplotlib.pyplot as plt
import matplotlib.ticker
### Force matplotlib to not use any Xwindows backend.
mp.use('agg')
### Change math font to sans serif
mp.rcParams['mathtext.fontset'] = 'stixsans'

def plot_average(confID, StudyParameters, outputExtension):
    """Plot the average fields in the confDir configuration directory."""

    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)
    outputDir = os.path.join(confDir, 'postProcessing/output')

    fs.mkdir_p(outputDir)
    
    for filterName in ['m0', 'm1', 'm2']:
        # Pressure
        ### Load data from the average step results
        fileName = os.path.join(confDir, 'average/postProcessing', filterName + '_pBar.csv')
        data = np.genfromtxt(fileName, skip_header=1, usecols=(0,1,3), delimiter=',')
        points = data[:,0:2]
        
        ### Extract grid values (probably not the most efficient way to do this)
        xv, yv = detect_coordinates(points, 1e-6)

        ### Plot
        plot_average_pressure(
            data, xv, yv, 
            os.path.join(outputDir, filterName + '_pBar.' + outputExtension)
            )
        
        # Velocity
        ### Load data from the average step results
        fileName = os.path.join(confDir, 'average/postProcessing', filterName + '_UBar.csv')
        data = np.genfromtxt(fileName, skip_header=1, usecols=(0,1,3,4,6), delimiter=',')
        points = data[:,0:2]

        ### Extract grid values
        xv, yv = detect_coordinates(points, 1e-6)

        ### Quiver plot settings
        quiverSettings = {
            'bounds' : (0, 0.9, 0, 0.6),
            'nPoints': (10,7)
        }

        ### Plot
        plot_average_velocity(data, xv, yv, quiverSettings, 
            os.path.join(outputDir, filterName + '_UBar.' + outputExtension)
            )

def plot_average_pressure(data, xv, yv , fileName):
    points = data[:,0:2]
    p      = data[:,2]

    # Set up a regular grid of interpolation points
    xi, yi = np.meshgrid(xv,yv)

    # Interpolate
    p_i = scipy.interpolate.griddata(points, p, (xi, yi))

    cmap = plt.get_cmap('coolwarm')
    plt.imshow(p_i, vmin=p_i.min(), vmax=p_i.max(), 
        extent=[xv[0], xv[-1], yv[0], yv[-1]], 
        origin='lower', interpolation='nearest',
        cmap=cmap
        )
    bar = plt.colorbar()

    bar.locator = matplotlib.ticker.FixedLocator(tickloc(p.min(), p.max()))
    bar.formatter = matplotlib.ticker.ScalarFormatter(useMathText=True) 
    bar.update_ticks()
    
    # Set plot details
    plt.title(r'Average pressure [Pa]')
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$y$ [m]')

    # Finish
    plt.savefig(fileName,dpi=150)
    plt.close()

def plot_average_velocity(data, xvUMag, yvUMag, quiverSettings, fileName):
    points = data[:,0:2]
    Ux     = data[:,2]
    Uy     = data[:,3]
    UMag   = data[:,4]

    # Intensity
    ### Interpolate
    xv, yv = xvUMag[:], yvUMag[:]
    xi, yi = np.meshgrid(xv,yv)
    UMagi = scipy.interpolate.griddata(points, UMag, (xi, yi))

    ### Plot
    cmap = plt.get_cmap('coolwarm')

    plt.imshow(UMagi, vmin=UMagi.min(), vmax=UMagi.max(), 
        extent=[xv[0], xv[-1], yv[0], yv[-1]], 
        origin='lower', interpolation='nearest',
        cmap=cmap
        )
    bar = plt.colorbar()

    bar.locator = matplotlib.ticker.FixedLocator(tickloc(UMag.min(), UMag.max()))
    bar.formatter = matplotlib.ticker.ScalarFormatter(useMathText=True) 
    bar.formatter.set_powerlimits((-1,1))
    bar.update_ticks()
    
    # Direction
    ### Interpolate
    bounds = quiverSettings['bounds']
    nPoints = quiverSettings['nPoints']
    xv = np.linspace(bounds[0], bounds[1], nPoints[0])
    yv = np.linspace(bounds[2], bounds[3], nPoints[1])

    xi, yi = np.meshgrid(xv,yv)
    Uxi = scipy.interpolate.griddata(points, Ux, (xi, yi))
    Uyi = scipy.interpolate.griddata(points, Uy, (xi, yi))
    UMagi = scipy.interpolate.griddata(points, UMag, (xi, yi)) 

    ### Plot
    plt.quiver(xv, yv, Uxi/UMagi, Uyi/UMagi, pivot='middle')

    # Set plot details
    plt.title(r'Average velocity [m/s]')
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$y$ [m]')

    # Finish
    plt.savefig(fileName,dpi=150)
    plt.close()

def plot_lines(confID, StudyParameters, outputExtension):
    studyDir = StudyParameters['Files']['studyDirectory']
    confDir = os.path.join(studyDir, confID)
    outputDir = os.path.join(confDir, 'postProcessing/output')

    fs.mkdir_p(outputDir)

    # Define lines
    lineIDList = StudyParameters['PostProcessing']['LineList'].keys()

    # TODO: add line consistency check (cells must be adjacent)
    # TODO: add automatic line building in reference sampleDict
    # TODO: if density = 0, then do not resample

    # Define output mesh
    r0 = float(StudyParameters['Lengths']['r0'])
    density = int(StudyParameters['PostProcessing']['Plot']['linesPlotDensity'])
    results = {}
    for lineID in lineIDList:
        cellIDList = StudyParameters['PostProcessing']['LineList'][lineID]
        meshMin = 1e300
        meshMax = -1e300
        for cellID in cellIDList:
            center = map(float, StudyParameters['Cell']['Grid'][cellID])
            if center[1] - r0*0.5 < meshMin:
                meshMin = center[1] - r0*0.5
            if center[1] + r0*0.5 > meshMax:
                meshMax = center[1] + r0*0.5

        results[lineID] = { 'meshInterp' : np.linspace(meshMin, meshMax, num = len(cellIDList) * density + 1) }

    # Gather reference data
    for lineID in lineIDList:
        meshInterp = results[lineID]['meshInterp']

        tempDir = os.path.join(confDir, 'reference/postProcessing/sets')
        fileName = str(os.path.join(tempDir, cs.detectLatestTime(tempDir), \
            lineID + '_magU.csv') )
        
        ### Load and sort data
        tempArray = np.loadtxt(fileName, delimiter=',', dtype=float, skiprows=1)
        tempArray = tempArray[tempArray[:,0].argsort()]
        results[lineID]['meshRef'] = tempArray[:,0]
        results[lineID]['magURef'] = tempArray[:,1]
        results[lineID]['magUInterp'] = np.interp(meshInterp, tempArray[:,0], tempArray[:,1])

    # Gather cell data
    for lineID in lineIDList:
        mesh = results[lineID]['meshInterp']

        dataList = []
        cellIDList = StudyParameters['PostProcessing']['LineList'][lineID]

        for cellID in cellIDList:

            tempDir = os.path.join(confDir, 'postProcessing', cellID, 'postProcessing/sets')
            fileName = str(os.path.join(tempDir, cs.detectLatestTime(tempDir), \
                'patchSeed_in_magUStar.csv') )

            tempArray = np.genfromtxt(fileName, delimiter=',', dtype=float, skip_header=1, usecols=(0,1))
            dataList.append(tempArray)

            ### Aggregate data
            tempArray = np.empty((0,2))
            for i in range(len(dataList)):
                tempArray = np.concatenate((tempArray,dataList[i]))

            ### Sort data, interpolate and store
            tempArray = tempArray[tempArray[:,0].argsort()]
            results[lineID]['meshCell'] = tempArray[:,0]
            results[lineID]['magUStarCell'] = tempArray[:,1]
            results[lineID]['magUStarInterp'] = np.interp(meshInterp, tempArray[:,0], tempArray[:,1])
    
    # Output raw results to csv files
    for lineID in lineIDList:
        tempArray = np.vstack((results[lineID]['meshRef'], results[lineID]['magURef']))
        np.savetxt(os.path.join(confDir, 'postProcessing/output', 'magU_' + lineID + '_raw.csv'), \
            tempArray.T, delimiter=',', header='y,magU'
            )

        tempArray = np.vstack((results[lineID]['meshCell'], results[lineID]['magUStarCell']))
        np.savetxt(os.path.join(confDir, 'postProcessing/output', 'magUStar_' + lineID + '_raw.csv'), \
            tempArray.T, delimiter=',', header='y,magUStar'
            )

    # Output interpolated results to csv files
    for lineID in lineIDList:
        meshInterp     = results[lineID]['meshInterp']
        magUInterp     = results[lineID]['magUInterp']
        magUStarInterp = results[lineID]['magUStarInterp']
        tempArray = np.vstack((meshInterp, magUInterp, magUStarInterp))
        np.savetxt(os.path.join(confDir, 'postProcessing/output', 'magU_' + lineID + '_interp.csv'), \
            tempArray.T, delimiter=',', header='y,magU,magUStar'
            )

    # Draw raw data plots
    nLines = len(lineIDList)
    fig, axs = plt.subplots(nLines, 1, figsize=(9,5*nLines))
    
    for i in range(nLines):
        lineID = lineIDList[i]
        ax = None

        if nLines > 1:
            ax = axs[i]
        else:
            ax = axs

        ax.plot(results[lineID]['meshRef'],  results[lineID]['magURef'],  'g-', label=r'$U$')
        ax.plot(results[lineID]['meshCell'], results[lineID]['magUStarCell'], 'ro', label=r'$U^*$')

        ax.set_title(lineID)
        ax.set_xlim([results[lineID]['meshInterp'].min(), results[lineID]['meshInterp'].max()])
        ax.set_xlabel(r'$y$ [m]')
        ax.yaxis.get_major_formatter().set_powerlimits((-3,4))
        ax.set_ylabel(r'[m/s]')
        ax.legend()    

    plt.tight_layout()
    fileName = os.path.join(outputDir, 'magU_lines_raw.' + outputExtension)    
    plt.savefig(fileName,dpi=150)
    plt.close()

    # Draw interpolated data plots
    nLines = len(lineIDList)
    fig, axs = plt.subplots(nLines, 1, figsize=(9,5*nLines))
    
    for i in range(nLines):
        lineID = lineIDList[i]
        fileName = os.path.join(outputDir, 'magU_' + lineID + '_interp.csv')
        data = np.loadtxt(fileName, delimiter=',', dtype=float)

        if nLines > 1:
            axs[i].plot(data[:,0], data[:,1], 'g-', label=r'$U$')
            axs[i].plot(data[:,0], data[:,2], 'ro', label=r'$U^*$')

            axs[i].set_title(lineID)
            axs[i].set_xlim([data[:,0].min(), data[:,0].max()])
            axs[i].set_xlabel(r'$y$ [m]')
            axs[i].yaxis.get_major_formatter().set_powerlimits((-3,4))
            axs[i].set_ylabel(r'[m/s]')
            axs[i].legend()

        else:
            axs.plot(data[:,0], data[:,1], 'g-', label=r'$U$')
            axs.plot(data[:,0], data[:,2], 'ro', label=r'$U^*$')

            axs.set_title(lineID)
            axs.set_xlim([data[:,0].min(), data[:,0].max()])
            axs.set_xlabel(r'$y$ [m]')
            axs.yaxis.get_major_formatter().set_powerlimits((-3,4))
            axs.set_ylabel(r'[m/s]')
            axs.legend()

    plt.tight_layout()

    fileName = os.path.join(outputDir, 'magU_lines_interp.' + outputExtension)    
    plt.savefig(fileName,dpi=150)
    plt.close()

def main():
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('studyfile', action="store", help="path to study file")
    parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
    args = parser.parse_args()

    StudyParameters = configobj.ConfigObj(args.studyfile)

    # Plot average fields
    for confID in StudyParameters['Parametric'].keys():
        plot_average(confID, StudyParameters, 'png')

    # Plot lines
    lineIDList = StudyParameters['PostProcessing']['LineList'].keys()

    for confID in StudyParameters['Parametric'].keys():
        plot_lines(confID, StudyParameters, 'png')

if __name__ == '__main__':
    main()