# Runs the cell case

import os, argparse, configobj, glob, shutil, itertools, Queue, threading, time, datetime
import downscalingtools.incompressible.cell as cell
import downscalingtools.incompressible.configuration as configuration
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs
from downscalingtools.tools.thread import *

def runner(param):
    """
    Worker thread function.
    """

    args = param['args']
    StudyParameters = param['StudyParameters']
    taskQueue = param['taskQueue']
    printer = param['printer']

    # Process arguments
    steps = {
            'prep' : True,
            'run' : True
        }
    if args.onlyprep:
        steps['run'] = False
    elif args.onlyrun:
        steps['prep'] = False

    while not(taskQueue.empty()):

        # Get task parameters
        task = taskQueue.get()
        taskQueue.task_done()
        taskID = task['ID']
        confID = task['confID']
        cellID = task['cellID']

        # Gather directories
        studyDir = StudyParameters['Files']['studyDirectory']
        templateDir = os.path.join(StudyParameters['Files']['templatesDirectory'], 'cell')
        runDir = os.path.join(studyDir, confID, 'cell', cellID)
        averageDir = os.path.join(studyDir, confID, 'average')

        message = "{0} CELL CASE ({1} job(s) left) {2}".format(
            datetime.datetime.now(), taskQueue.qsize(), runDir)
        printer.put(PrintBufferElement(message))

        if steps['prep']:
            # Compute diffusivity and so on
            AdditionalParameters = configuration.process(StudyParameters)

            # Gather multiprocessing parameters
            nProc = int(StudyParameters['Cell']['Parallel']['nProc'])
            domDecomposition = map(int, StudyParameters['Cell']['Parallel'].as_list('decomposition'))

            # Copy template files
            fs.copydir(templateDir, runDir)

            # Fetch sourceTermDict
            shutil.copy(
                os.path.join(averageDir, 'output', cellID, 'sourceTermDict'),
                os.path.join(runDir, 'constant')
                )

            # Prepare CaseParameters.ini
            CaseParameters = configobj.ConfigObj()
            
            CaseParameters['Lengths']  = StudyParameters['Lengths'].dict()
            CaseParameters['GridSize'] = {'M' : 1, 'N' : 1}
            CaseParameters['Cell']     = {'cellID': cellID, 'offset' : StudyParameters['Cell']['Grid'].as_list(cellID)}

            Uw = float(StudyParameters['Reference'][confID]['BoundaryConditions']['UwPlus']) * AdditionalParameters['URef']
            cellCenter = map(float, StudyParameters['Cell']['Grid'][cellID])

            CaseParameters['Dictionaries'] = {
                '0/templates/UTildeStar' : { 'Uw' : Uw },
                'constant/transportProperties' : { 'nu' : AdditionalParameters['nu'] },
                'constant/cellProperties' : { \
                    'centerX' : cellCenter[0],
                    'centerY' : cellCenter[1],
                    'centerZ' : cellCenter[2]
                    },
                'system/decomposeParDict' : { \
                    'numberOfSubdomains' : nProc, 
                    'nX' : domDecomposition[0],
                    'nY' : domDecomposition[1],
                    'nZ' : domDecomposition[2]
                    },
                'system/fvSolution' : {
                    'relaxpTildeStar' : float(StudyParameters['Cell'][confID]['Relaxation']['pTildeStar']),
                    'relaxUTildeStar' : float(StudyParameters['Cell'][confID]['Relaxation']['UTildeStar']),
                    'epsUTildeStar'   : float(StudyParameters['Cell'][confID]['Penalization']['epsUTildeStar'])
                    },
                'system/controlDict' : StudyParameters['Cell'][confID]['TimeMarching'].dict()
                }

            CaseParameters.filename = os.path.join(runDir, 'CaseParameters.ini')
            CaseParameters.write()

        # Execute step
        startDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} START".format(startDate)))

        if steps['prep']:
            cell.prepareMutables(runDir, verbose=args.verbose)
        if steps['run']:
            cell.decompose(runDir, verbose=args.verbose)
            cell.compute(runDir, verbose=args.verbose)
            cell.reconstruct(runDir, verbose=args.verbose)

        cs.touchFoam(runDir)

        endDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} END (exec time = {1})".format(endDate, endDate-startDate)))

    # Count workers still running
    activeThreadsList = threading.enumerate()
    nActive = 0
    for t in activeThreadsList:
        if "runner" in t.name: nActive += 1
    printer.put(PrintBufferElement("{0} Work finished ({1} worker threads still running)".format(
        datetime.datetime.now(), nActive-1)))

def main():
    # Handle keyboard interruption
    try:
        # Process arguments
        parser = argparse.ArgumentParser()
        parser.add_argument('studyfile', action="store", help="path to study file")
        parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
        # The following two are mutually exclusive
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--onlyprep', action="store_true", help="only prepare case")
        group.add_argument('--onlyrun', action="store_true", help="only run OpenFOAM decomposition, solver and reconstruction")
        args = parser.parse_args()

        # Spawn printer
        printer = Printer()

        # Process configuration file
        configFilePath = args.studyfile
        StudyParameters = configobj.ConfigObj(configFilePath)

        # Create and fill task queue (cartesian product of taskID and cellID lists)
        confIDList = StudyParameters['Parametric'].keys()
        cellIDList = StudyParameters['Cell']['Grid'].keys()
        taskQueue = Queue.Queue()

        for it in itertools.product(confIDList, cellIDList):
            task = {}
            task['ID'] = "{0}/{1}".format(it[0],it[1]) # Unique ID
            task['confID'] = it[0]
            task['cellID'] = it[1]
            taskQueue.put(task)

        # Spawn runner threads
        param = {
            'args' : args,
            'StudyParameters' : StudyParameters,
            'taskQueue' : taskQueue,
            'printer' : printer
            }

        threadPool = []
        nThread = int(StudyParameters['Cell']['Parallel']['nThread'])
        for i in range(nThread):
            t = threading.Thread(
                target=runner, 
                args=(param,), 
                name="runner{0}".format(i)
                )
            t.daemon = True
            threadPool.append(t)

        # Start threads
        printer.start()
        for t in threadPool: t.start()

        # Main thread only waits until the others have finished their jobs
        while threading.active_count() > 2:
            time.sleep(1)

        # Remove all .obj files
        for f in glob.glob("*.obj"):
            try:
                os.remove(f)
            except:
                pass

    # This handles keyboard interruption
    except KeyboardInterrupt:
    #except (KeyboardInterrupt, SystemExit):
        print '\n! Received keyboard interrupt, quitting threads.\n'
        
if __name__ == '__main__':
    main()