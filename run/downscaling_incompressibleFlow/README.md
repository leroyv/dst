# downscaling_incompressibleFlow

An OpenFOAM case for the downscaling of incompressible, steady flows. This README is written using Markdown (python-markdown syntax, very close to the Github flavor's one). If you are looking for a no-frills markdown viewer, try the [Markdown Preview Plus](https://github.com/volca/markdown-preview) Chrome extension or the [Markdown Viewer](https://github.com/Thiht/markdown-viewer) Firefox extension or the [Markdown Viewer](https://github.com/Thiht/markdown-viewer) Firefox extension.

## Introduction

The purpose of this OpenFOAM case is to allow parametric studies for the investigation of the downscaling procedure for incompressible flows. This case is based on version 2.2.2 of OpenFOAM and makes extensive use of the pyFoam library. All run scripts are written in Python 2.7. See the list of dependencies.

## Use

This case comes with a bunch of template files which are used to perform the runs. Template files can be modified and forked at will. The complete run sequence is the following:

* Run the reference case: runReference.py
* Run the averaging operation: runAverage.py
* Run the cell cases: runCell.py
* Post-process the results: runPostProcess.py
* Plot the results: runPlot.py

The core study parameters are defined in a _study file_, using the INI syntax described in the [configobj](http://www.voidspace.org.uk/python/configobj.html#the-config-file-format) Python module. A sample study file is available in the templates/ directory.

## Vocabulary

Here are a few vocabulary definitions:

* A study is a collection of configurations to be used as input for a run
* Configuration: set of parameters; each configuration is associated with a unique confID
* A run is divided in steps, divided in tasks
* Run: complete step sweep (reference, average, cell, postprocessing, plotting)
* Step: execution of the tasks (reference, average, cell and postprocessing are steps)
* Tasks: collection of commands and data manipulation; there are several types of tasks, depending on the step considered
* Case: OpenFOAM case; each step, except plotting, is associated to a corresponding case

Sample directories: 
* Study: study/
* Configuration: study/Re_100/
* Step: study/Re_100/reference/
* Template: templates/
* Case: every step directory containing OpenFOAM case files is a case directory

## About the sample study file

The sample study files is designed to simulate the flow of a fluid with properties close to that of a gas at ambiant temperature and pressure. The sweep study file performs a parametric sweep of the inlet Reynolds number, ranging from a fully diffusive regime (Re_in = 0.01) to a strongly inertial one (Re_in = 100) (see Dybbs, A. & Edwards, R. (1984) for more information about flow regimes in porous media).

## Dependencies

### Third-party applications and libraries

Software configuration (tested on Ubuntu 12.04 LTS and CentOS 6.5):

| Program          | Version        |
|:---------------- |:-------------- |
| OpenFOAM         | 2.2.2          |
| swak4foam        | 0.2.3--0.2.4   |
| Python           | 2.6.6--2.7.7   |
| pyFoam           | 0.6.2          |
| python-configobj | 5.0.4          |
| python-scipy     | 0.14.0         |
| python-numpy     | 1.8.1          |

**Note:** Clusters generally run outdated software and updating them is often impossible because your system admin has other things to do. You might have to make adjustments to run the code correctly (see below the trouble through which I went).

**Note:** I had to compile GCC to install OpenFOAM 2.2.2. It's rather well explained [on the wiki](http://openfoamwiki.net/index.php/Installation/Linux/OpenFOAM-2.2.2/CentOS_SL_RHEL); I had only a little bit of fine-tuning to do.

**Note:** Upgrading numpy and scipy is doable using [pip and virtualenvs](http://www.dabapps.com/blog/introduction-to-pip-and-virtualenv-python/). You should actually use this all the time. Your distro will most probably come packing antique software and you will probably need to upgrade anyway. Warning: compiling scipy can be a nasty task. The simplest way to do it is the following (feel free to add the require version flags to the pip instructions):

* Install numpy by running `pip install numpy`
* Install BLAS and LAPACK using the distro package manager; compiling those is a little bit of a nightmare by itself
* Install scipy by running `pip install scipy`

**Note:** You can run a more recent version of Python in your virtualenv using [this method](http://stackoverflow.com/questions/5506110/is-it-possible-to-install-another-version-of-python-to-virtualenv). This is very handy to install the iPython shell when you only have Python 2.6 shipped with your distro.

### Custom solvers and tools

| Program               | Version |
|:--------------------- |:------- |
| simplePrimitiveFoam   | 1.0.0   |
| simpleDownscalingFoam | 1.1.0   |
| average               | 1.1.0   |

## Upcoming features & things to do (the higher, the sooner)

* Error check (by looking into the .err.logfile log files)
* Switch to FromTemplate instead of createPatchDict.py
* Proc number consistency check or building of the decomposeParDict using FromTemplate (automatically compute the number of necessary procs given the decomposition)
* Line consistency check for the line plots (i.e. do the defined lines contain adjacent cells? are the cells ordered correctly?)
* Relative error plot on lines
* Switch to an unsteady solver, better suited for the use of penalty methods
* De-daemonize the threads and use signalling mechanisms (see Event) to terminate them on KeyboardInterrupt
* Real-time plotting of residuals

## Known issues

* Cell problems tend to diverge badly at high Reynolds numbers if relaxation factors are left to "usual" values.
* Mapping operations are very slow. Use of the more advanced mapping options in OpenFOAM 2.3 would probably improve this.
* Massive amout of console output (redirected to the log file) generated by the cell problem solver might increase computation time; could be addressed by using a tmpfs (faster log writing) and switching to a more compact text output format (faster console output).
* The ParsedParameterFile class throws errors when runPostprocessing.py is executed with many threads, probably due to way this PyFoam class implements dictionary reading. Therefore, running the script with a small number of threads (e.g. 4) is recommended at the moment.

## Release notes

### 3.0.0 (2014/10/07)

* New features
    * Additional template files for the Angeli et al. case
    * Additional template files for high resolution reference cases
* Major changes
    * Massive code overhaul: switched to an external, more modular structure, for the DownscalingTools library
    * Unified features with the thermo case: configurations now have a unique confID; no more automated parametric sweep on U0 and Uw, each config parameters most be specified explicitly
    * Switched to Matplotlib for output
* Minor changes
    * All run scripts now have restricted use options, in case only a certain part of operations would be needed (i.e. only prepare, only map, only run, etc.)
    * README updated with important information
* Bugfixes
    * Problems on post-processing due to slow writing to disk

### 2.0.0 (2014/07/09)

* New features
    * Threading for all scripts
    * Push script to propagate the code to the production location
    * Verbosity control: no argument = silent mode (no output from running tasks); -v argument = compact output; -vv argument = complete output; using non-null verbosity with more than 1 thread will result in hardly readable output (verbosity > 0 is mostly useful for debugging cases)
* Major changes
    * Massive code overhaul: switched to a modular architecture for DownscalingTools.py; eliminated global variables
    * Better definition of vocabulary for more clarity
    * Average: significantly improved the source term computation thanks to the use of the new tools.math k-d tree nearest neighbor search
    * tools.math library: implemented a k-d tree nearest neighbor search
    * Moved the study file selection to mandatory parameter; no more default value for study file path
* Minor changes
    * Run scripts are no longer executable, and must be called using a command of the form `python <mystuff>.py` (this improves compatibility with virtualenv)
    * Cell: modified the createPatchDict.py script to avoid the use of the thread-unsafe os.chdir command
    * Cell: added sleep commands after the execution of every OF solver or utility to prevent some bugs from happening (happens only with verbosity level > 0)
    * Templates: renamed and slightly modified study files

### 1.2.0 (2014/06/24)

* New features
    * All steps: compact output; the amount of terminal output is significantly reduced; old, complete output can still be viewed by calling the run scripts with the --verbose option
    * Reference, cell: number of cores used for parallel execution and domain decomposition now defined in the study file
    * Templates: additional template files for simulations on a finer mesh
* Major changes
    * All steps: clearer progress messages, especially for post-processing scripts
    * Cell: switched to simpleDownscalingFoam v1.1.0, which uses a more rigorous derivation of the cell problem. This does not change anything for the user, though
    * Post-processing: separated data formatting and plotting to only use the gnuplot-py module as a data handler
    * Plotting: additional study step introduced, separated from post-processing; Gnuplot is now called using Gnuplot scripts stored in the templates/ directory
* Minor changes
    * Modified Gnuplot scripts for better compatibility and output beauty

### 1.1.1 (2014/06/11)

* Minor changes
    * Sample study file slightly adjusted
* Bugfixes
    * Fixed problems with the definition of lines in templates/reference/system/sampleDict
    * Fixed a critical bug in the averaging which preventing the reference fields to be mapped to the case as expected
    * Corrected a critical mistake in the computation of the reference Reynolds number

### 1.1.0 (2014/06/04)

* New features
    * Runtime-specified study file (defaults to templates/StudyParameters.ini)
    * Runtime-specified run and templates directories (in the study file)
    * Mass boundary source term using swak4foam
    * Parametric sweep on the wall velocity value
* Major changes
    * Dimensioning: changed the reference velocity. Consequence: varying parameters are now dimensionless velocities for input and interface source terms. _This allows to set the input velocity to zero_
    * Moved run sequences to the associated function in the DownscalingTools.py library. Consequence: Allrun.py scripts no longer needed; switched to average v1.0.1 (not backward-compatible)
* Minor changes
    * Case packing: smarter script, implementation of runtime selection of archive and run directories
* Bugfixes
    * Fixed some messy stuff in the templates/reference/system/sampleDict
    * Line graph gnuplot script now skips missing data files instead of crashing
    * Removed the execution of blockMesh in the reference case run script

### 1.0.0 (2014/05/28)

* Features
    * Initial release
    * Supported study steps:
        * Run reference case
        * Run averaging procedure (_m0_ filter for basic output, _m2_ filter for source term computation)
        * Run cell case for user-defined cells
        * Run post-processing operations in an independent case for proper visualization
    * Parametric run as a function of the Reynolds number
    * Case packaging to archive the results in a compact way
    * Use of a hybrid triangle-quadrangle mesh generated using the SALOME mesher
    * Basic graphical monitoring of residues

## Note about version numbering

The code version numbering is done using a three-digit convention, plus an additional string for alpha and beta versions. Here are the cases to be considered:

* Production release: vX.Y.Z. X is the major release number and changes only when a major overhaul of the code is done. Y is the minor release number, and changes when major features or changes are implemented. Z is the bugfix number, and changes when important bugfixes need to be included before the next minor release. Examples: 1.0.0 for the the very first production release; 1.0.1 for the first bugfix release; 1.1.0 for the first minor release; 2.0.0 for the next big overhaul of the code; etc.
* Test release: vX.0yZ. X is the major release number. y is a letter, denoting the alpha (a), beta (b) or release candidate (rc) state of the code. Z is the release number. Examples: 1.0a1 for the very first alpha release; 1.0b4 for the fourth beta release; 1.0rc2 for the second release candidate.
