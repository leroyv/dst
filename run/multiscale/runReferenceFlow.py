# Runs the reference case

import os, argparse, configobj, itertools, Queue, threading, time, datetime
import downscalingtools.multiscale.reference as reference
import downscalingtools.multiscale.configuration as configuration
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs
from downscalingtools.tools.thread import *

def runner(param):
    """
    Worker thread function.
    """

    args = param['args']
    StudyParameters = param['StudyParameters']
    taskQueue = param['taskQueue']
    printer = param['printer']

    # Process arguments
    steps = {
            'prep' : True,
            'run' : True
        }
    if args.onlyprep:
        steps['run'] = False
    elif args.onlyrun:
        steps['prep'] = False

    while not(taskQueue.empty()):

        # Get task parameters
        task = taskQueue.get()
        taskQueue.task_done()
        taskID = task['ID']
        confID = task['confID']

        # Gather directories
        studyDir = StudyParameters['Files']['studyDirectory']
        templateDir = os.path.join(StudyParameters['Files']['templatesDirectory'], 'reference/flow')
        runDir = os.path.join(studyDir, 'reference/flow', taskID)

        message = "{0} Starting reference flow case {1} ({2} job(s) left) {3}".format(
            datetime.datetime.now(), taskID, taskQueue.qsize(), runDir)
        printer.put(PrintBufferElement(message))

        if steps['prep']:
            # Compute diffusivity and so on
            AdditionalParameters = configuration.process(StudyParameters)
            
            # Gather multiprocessing parameters
            nProc = int(StudyParameters['Reference']['Parallel']['nProc'])
            domDecomposition = map(int, StudyParameters['Reference']['Parallel'].as_list('decomposition'))

            # Copy template files
            fs.copydir(templateDir, runDir)

            # Prepare CaseParameters.ini
            CaseParameters = configobj.ConfigObj()
            
            CaseParameters['Lengths']  = StudyParameters['Lengths'].dict()
            CaseParameters['GridSize'] = StudyParameters['Reference']['Grid'].dict()

            U0 =   float(StudyParameters['Reference']['Flow'][confID]['BoundaryConditions']['U0Plus']) \
                 * AdditionalParameters['URef']
            Uw =   float(StudyParameters['Reference']['Flow'][confID]['BoundaryConditions']['UwPlus']) \
                 * AdditionalParameters['URef']

            CaseParameters['Dictionaries'] = {
                '0/templates/U' : { 'U0' : U0, 'Uw' : Uw },
                'constant/transportProperties' : { 'nu' : AdditionalParameters['nu'] },
                'system/decomposeParDict' : { 
                    'numberOfSubdomains' : nProc, 
                    'nX' : domDecomposition[0],
                    'nY' : domDecomposition[1],
                    'nZ' : domDecomposition[2] 
                    },
                'system/fvSolution' : {
                    'relaxp' : float(StudyParameters['Reference']['Flow'][confID]['Relaxation']['p']),
                    'relaxU' : float(StudyParameters['Reference']['Flow'][confID]['Relaxation']['U'])
                    },
                'system/controlDict' : StudyParameters['Reference']['Flow'][confID]['TimeMarching'].dict()
            }

            CaseParameters.filename = os.path.join(runDir, 'CaseParameters.ini')
            CaseParameters.write()

        # Execute step
        startDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} Executing case {1}".format(startDate, taskID)))

        if steps['prep']:
            reference.flowPrepareMutables(runDir, verbose=args.verbose)
        if steps['run']:
            reference.decompose(runDir, verbose=args.verbose)
            reference.flowCompute(runDir, verbose=args.verbose)
            reference.reconstruct(runDir, verbose=args.verbose)

        cs.touchFoam(runDir)

        endDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} Finishing case {1} (exec time = {2})".format(endDate, taskID, endDate-startDate)))

    # count workers still running
    activeThreadsList = threading.enumerate()
    nActive = 0
    for t in activeThreadsList:
        if "runner" in t.name: nActive += 1
    printer.put(PrintBufferElement("{0} Work finished ({1} worker thread(s) still running)".format(
        datetime.datetime.now(), nActive-1)))

def main():
    # Handle keyboard interruption
    try:
        # Process arguments
        parser = argparse.ArgumentParser()
        parser.add_argument('studyfile', action="store", help="path to study file")
        parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
        # The following two are mutually exclusive
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--onlyprep', action="store_true", help="only prepare case")
        group.add_argument('--onlyrun', action="store_true", help="only run OpenFOAM decomposition, solver and reconstruction")
        args = parser.parse_args()

        # Spawn printer
        printer = Printer()
        
        # Process configuration file
        configFilePath = args.studyfile
        StudyParameters = configobj.ConfigObj(configFilePath)

        # Create and fill task queue
        confIDList = StudyParameters['Parametric']['Flow'].keys()
        taskQueue = Queue.Queue()

        for confID in confIDList:
            task = {}
            task['ID'] = confID
            task['confID'] = confID
            taskQueue.put(task)

        # Spawn runner threads
        param = {
            'args' : args,
            'StudyParameters' : StudyParameters,
            'taskQueue' : taskQueue,
            'printer' : printer
            }

        threadPool = []
        nThread = int(StudyParameters['Reference']['Parallel']['nThread'])
        for i in range(nThread):
            t = threading.Thread(
                target=runner, 
                args=(param,), 
                name="runner{0}".format(i)
                )
            t.daemon = True
            threadPool.append(t)

        # Start threads
        printer.start()
        for t in threadPool: t.start()

        # Main thread only waits until the others have finished their jobs
        while len(threading.enumerate()) > 2:
            time.sleep(1) 

    # This handles keyboard interruption
    except KeyboardInterrupt:
        print '\n! Received keyboard interrupt, quitting threads.\n'

if __name__ == '__main__':
    main()