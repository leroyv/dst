# Runs the reference case

import os, argparse, configobj, itertools, Queue, threading, time, datetime
import downscalingtools.multiscale.reference as reference
import downscalingtools.multiscale.configuration as configuration
import downscalingtools.tools.filesystem as fs
import downscalingtools.tools.case as cs
from downscalingtools.tools.thread import *
from downscalingtools.tools.case import detectLatestTime

def runner(param):
    """
    Worker thread function.
    """

    args = param['args']
    StudyParameters = param['StudyParameters']
    taskQueue = param['taskQueue']
    printer = param['printer']

    # Process arguments
    steps = {
            'prep' : True,
            'map' : True,
            'run' : True
        }
    if args.onlyprep:
        steps['map'], steps['run'] = False, False
    elif args.nomap:
        steps['map'] = False
    elif args.onlymap:
        steps['prep'], steps['run'] = False, False
    elif args.onlyrun:
        steps['prep'], steps['map'] = False, False

    while not(taskQueue.empty()):

        # Get task parameters
        task = taskQueue.get()
        taskQueue.task_done()
        taskID = task['ID']
        confID = task['confID']

        # Gather directories
        studyDir = StudyParameters['Files']['studyDirectory']
        templateDir = os.path.join(StudyParameters['Files']['templatesDirectory'], 'reference/thermo')
        runDir = os.path.join(studyDir, 'reference/thermo', taskID)
        flowDir = os.path.join(studyDir, 'reference/flow', StudyParameters['Parametric']['Thermo'][confID]['flowID'])

        message = "{0} Starting reference flow case {1} ({2} job(s) left) {3}".format(
            datetime.datetime.now(), taskID, taskQueue.qsize(), runDir)
        printer.put(PrintBufferElement(message))

        if steps['prep']:
            # Compute diffusivity and so on
            AdditionalParameters = configuration.process(StudyParameters)
            
            # Gather multiprocessing parameters
            nProc = int(StudyParameters['Reference']['Parallel']['nProc'])
            domDecomposition = map(int, StudyParameters['Reference']['Parallel'].as_list('decomposition'))

            # Copy template files
            fs.copydir(templateDir, runDir)

            # Copy flow files
            ### Generate list of files
            copyList = [
                [ 'system/controlDict'           , 'system/controlDict' ],
                [ 'system/decomposeParDict'      , 'system/decomposeParDict' ],
                [ 'constant/transportProperties' , 'constant/transportProperties' ],
                [ 'constant/polyMesh/boundary'   , 'constant/polyMesh/boundary' ],
                [ 'constant/polyMesh/faces'      , 'constant/polyMesh/faces' ],
                [ 'constant/polyMesh/neighbour'  , 'constant/polyMesh/neighbour' ],
                [ 'constant/polyMesh/owner'      , 'constant/polyMesh/owner' ],
                [ 'constant/polyMesh/points'     , 'constant/polyMesh/points' ]
                ]
            if steps['map']: 
                copyList.append([ os.path.join(str(detectLatestTime(flowDir)),'U'), '0/U' ])
            elif (verbose > 1): 
                message = "{0} Mapping step disabled: 0/U not copied during preparation step".format(
                    datetime.datetime.now(), taskQueue.qsize(), runDir)
                printer.put(PrintBufferElement(message))
            ### Copy files
            fs.copyfilelist(flowDir, os.path.join(runDir, 'input/flow'), copyList)
            
            # Prepare CaseParameters.ini
            CaseParameters = configobj.ConfigObj()
            
            CaseParameters['Lengths']  = StudyParameters['Lengths'].dict()
            CaseParameters['GridSize'] = StudyParameters['Reference']['Grid'].dict()
            CaseParameters['Scaling'] = { 'velocity' : StudyParameters['Parametric']['Thermo'][confID]['flowMultiplier'] }

            ### Read BCs
            bcDict = {}
            for bcName in StudyParameters['Reference']['Thermo'][confID]['BoundaryConditions'].dict().keys():
                bcDict[bcName] = StudyParameters['Reference']['Thermo'][confID]['BoundaryConditions'][bcName]

            temperatureDict = bcDict
            temperatureDict['k'] = AdditionalParameters['k']

            timeDict = StudyParameters['Reference']['Thermo'][confID]['TimeMarching']

            ### Build the actual CaseParameters.ini
            CaseParameters['Dictionaries'] = {
                '0/templates/T' : temperatureDict,
                'constant/transportProperties' : { 
                    'alpha' : AdditionalParameters['alpha'],
                    'k'     : AdditionalParameters['k']
                    },
                'system/decomposeParDict' : { 
                    'numberOfSubdomains' : nProc, 
                    'nX' : domDecomposition[0],
                    'nY' : domDecomposition[1],
                    'nZ' : domDecomposition[2]
                    },
                'system/fvSchemes' : { 'ddtScheme' : timeDict['ddtScheme'] },
                'system/controlDict' : {
                        'endTime' : timeDict['endTime'],
                        'deltaT' : timeDict['deltaT'],
                        'writeInterval' : timeDict['writeInterval']
                    }
            }

            CaseParameters.filename = os.path.join(runDir, 'CaseParameters.ini')
            CaseParameters.write()

        # Execute step
        startDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} Executing case {1}".format(startDate, taskID)))

        if steps['prep']:
            reference.thermoPrepareMutables(runDir, verbose=args.verbose)
        if steps['map']:
            reference.thermoMapSources(runDir, verbose=args.verbose)
        if steps['run']:
            reference.decompose(runDir, verbose=args.verbose)
            reference.thermoCompute(runDir, verbose=args.verbose)
            reference.reconstruct(runDir, verbose=args.verbose)

        cs.touchFoam(runDir)

        endDate = datetime.datetime.now()
        printer.put(PrintBufferElement("{0} Finishing case {1} (exec time = {2})".format(endDate, taskID, endDate-startDate)))

    # count workers still running
    activeThreadsList = threading.enumerate()
    nActive = 0
    for t in activeThreadsList:
        if "runner" in t.name: nActive += 1
    printer.put(PrintBufferElement("{0} Work finished ({1} worker threads still running)".format(
        datetime.datetime.now(), nActive-1)))

def main():
    # Handle keyboard interruption
    try:
        # Process arguments
        parser = argparse.ArgumentParser()
        parser.add_argument('studyfile', action="store", help="path to study file")
        parser.add_argument('-v', '--verbose', action="count", help="increase verbosity")
        # The following three are mutually exclusive
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--onlyprep', action="store_true", help="only prepare case")
        group.add_argument('--nomap', action="store_true", help="skip velocity mapping step")
        group.add_argument('--onlymap', action="store_true", help="only map velocity case")
        group.add_argument('--onlyrun', action="store_true", help="only run OpenFOAM decomposition, solver and reconstruction")
        args = parser.parse_args()

        # Spawn printer
        printer = Printer()
        
        # Process configuration file
        configFilePath = args.studyfile
        StudyParameters = configobj.ConfigObj(configFilePath)

        # Create and fill task queue
        confIDList = StudyParameters['Parametric']['Thermo'].dict().keys()
        taskQueue = Queue.Queue()

        for confID in confIDList:
            task = {}
            task['ID'] = confID
            task['confID'] = confID
            taskQueue.put(task)

        # Spawn runner threads
        param = {
            'args' : args,
            'StudyParameters' : StudyParameters,
            'taskQueue' : taskQueue,
            'printer' : printer
            }

        threadPool = []
        nThread = int(StudyParameters['Reference']['Parallel']['nThread'])
        for i in range(nThread):
            t = threading.Thread(
                target=runner, 
                args=(param,), 
                name="runner{0}".format(i)
                )
            t.daemon = True
            threadPool.append(t)

        # Start threads
        printer.start()
        for t in threadPool: t.start()

        # Main thread only waits until the others have finished their jobs
        while len(threading.enumerate()) > 2:
            time.sleep(1) 

    # This handles keyboard interruption
    except KeyboardInterrupt:
        print '\n! Received keyboard interrupt, quitting threads.\n'

if __name__ == '__main__':
    main()